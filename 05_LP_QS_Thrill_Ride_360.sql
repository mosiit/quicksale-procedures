USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_Thrill_Ride_360]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_thrill_ride_360]
GO

/*
    Local Stored Procedure: LP_QS_thrill_ride_360

*/

CREATE PROCEDURE [dbo].[LP_QS_thrill_ride_360]
        @performance_dt datetime = Null,
        @sort_type varchar(10) = 'Time',  --Time or Production
        @remove_past_performances char(1) = 'Y',
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    /* Procedure Variables */

    DECLARE @bkColorDefault int, @foColorToday int, @foColorFuture int, @foColor int
    DECLARE @bkColor int, @bTypePerf int, @bTypeNav int, @perf_no int, @perf_zone_no int, @season_no int, @bkColorThrill int
    DECLARE @title_no int, @layout_no_a int, @layout_no_b int, @nav_layout_no int
    DECLARE @xMaxA int, @yMaxA int, @xMaxB int, @yMaxB int, @curX int,  @curY int, @is_today char(1)
    DECLARE @perf_date char(10), @perf_time char(8), @perf_code varchar(10)
    DECLARE @layout_name_a varchar(30), @layout_name_b varchar(30), @venue_name varchar(20), @perf_title varchar(30)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @cutoff_time char(8)
   
    /*  If Null is passed to the Performance Date parameter, default to today's date
        If Null is passed to the Title number parameter, default to 0 (All titles)
        IF anything other then 'Title' is passed to the Sort Type parameter, default to 'Time'      */
    
    SELECT @performance_dt = IsNull(@performance_dt,getdate())
    SELECT @sort_type = IsNull(@sort_type, '') 
    IF @sort_type <> 'Production' SELECT @sort_type = 'Time'
    SELECT @remove_past_performances = IsNull(@remove_past_performances, 'N')
    IF @remove_past_performances <> 'Y' SELECT @remove_past_performances = 'N'

    /*  @ valid title no is needed and that title must exist with a quick sale venue value in order for this procedure to work  */
    SELECT @title_no = title_no FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_name] = 'Thrill Ride 360'
    SELECT @title_no = IsNull(@title_no, 0)
    IF @title_no <= 0 or not exists (SELECT * FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_no] = @title_no and [quick_sale_venue] <> '') BEGIN
        SELECT @return_message = 'Thrill Ride 360 title cannot be found.'
        GOTO DONE
    END

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorThrill = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_no] = @title_no
    SELECT @bkColorThrill = IsNull(@bkColorThrill, @bkColorDefault)
    
    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)

    SELECT @foColorFuture = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Future'
    SELECT @foColorFuture = isnull(@foColorFuture, 0)
   
    IF convert(char(10),@performance_dt,111) = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    SELECT @cutoff_time = convert(char(8),dateadd(minute,-10,getdate()),108)
    IF @is_today = 'N' SELECT @foColor = @foColorFuture ELSE SELECT @foColor = @foColorToday

    SELECT @layout_name_a = 'Thrill Ride A Launch', @layout_name_b = 'Thrill Ride B Launch'
    
    /*  Set the performance date to a string (yyyy/mm/dd) and determine if the procedure is working with the current date.
        There are different text colors for today vs future day      */

    SELECT @perf_date = convert(char(10),@performance_dt,111)
    IF @perf_date > convert(char(10),getdate(),111) SELECT @foColor = @foColorFuture
    
    /*  The dimensions of the layout are needed to determine the number of buttons on the screen. If the dimensions cannot be determines, the procedure cannot go on  */

    SELECT @layout_no_a = [id],  @xMaxA = [columns], @yMaxA = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_a
    SELECT @layout_no_a = IsNull(@layout_no_a, 0), @xMaxA = IsNull(@xMaxA, 0), @yMaxA = IsNull(@yMaxA,0)
    SELECT @layout_no_b = [id],  @xMaxB = [columns], @yMaxB = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_b
    SELECT @layout_no_b = IsNull(@layout_no_b, 0), @xMaxB = IsNull(@xMaxB, 0), @yMaxB = IsNull(@yMaxB,0)
    IF @layout_no_a <= 0 or @layout_no_b <= 0 BEGIN
        SELECT @return_message = 'Unable to locate one or both of the Thrill Ride layouts.'
        GOTO DONE
    END
    IF @XMaxA <= 0 or @yMaxA <= 0 or @xMaxB <= 0 or @xMaxB <= 0 BEGIN
        SELECT @return_message = 'Unable to determine the dimensions of one or both of the Thrill Ride layouts.'
        GOTO DONE
    END

    /*  The button type is a necessary value.  This procedure deals with performance buttons, so the id number for that type is needed.
        If it cannot be determines, the procedure cannot go on.   */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = IsNull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = IsNull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    /*  Clear the layout of all performance buttons before continuing.  
        If there are navigation or other types of buttons, Those will be left alone.    */

    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] in (@layout_no_a, @layout_no_b)

    
    /*  Top Row Navigation Buttons - CAPSULE A  */
    
    /* Move To Capsule B Button */
    
        SELECT @Caption1 = 'MOVE TO', @Caption2 = 'CAPSULE B', @Caption3 = '', @caption4 = ''
                   
        SELECT @curY = 1, @curX = 1

        BEGIN TRY

            IF @layout_no_b > 0 and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_a and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_a, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDefault, @foColor, null, @layout_no_b, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Capsule A to B navigation button.'
            GOTO DONE

        END CATCH

    SELECT @Caption1 = 'TODAY', @caption4 = ''
    SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) + ' ' 
                     + CASE WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                       ELSE convert(char(2),datePart(day,@performance_dt)) END
  
    /* Omni Button */

    
        SELECT @bkColor = null
        SELECT @bkColor = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Mugar Omni Theater'
        SELECT @bkColor = Isnull(@bkColor,-1)
        IF @bkColor < 0 SELECT @bkColor = @bkColorDefault
    
        SELECT @nav_layout_no = null
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'today_omni'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
    
        SELECT @Caption2 = 'Omni Theater'
           
        SELECT @curY = 1, @curX = 2

        BEGIN TRY

            IF @nav_layout_no > 0 and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_a and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_a, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Omni button for today.'
            GOTO DONE

        END CATCH
        
    /* Planetarium Button */
        
        SELECT @bkColor = null
        SELECT @bkColor = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Hayden Planetarium'
        SELECT @bkColor = Isnull(@bkColor,-1)
        IF @bkColor < 0 SELECT @bkColor = @bkColorDefault
    
        SELECT @nav_layout_no = null
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'today_planet'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
    
        SELECT @Caption2 = 'Planetarium'
           
        SELECT @curY = 1, @curX = 3

        BEGIN TRY

            IF @nav_layout_no > 0 and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_a and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_a, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Planetarium button for today.'
            GOTO DONE

        END CATCH


    /* 4D Theater Button */
        
        SELECT @bkColor = null
        SELECT @bkColor = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = '4D-Theater'
        SELECT @bkColor = Isnull(@bkColor,-1)
        IF @bkColor < 0 SELECT @bkColor = @bkColorDefault
    
        SELECT @nav_layout_no = null
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'today_4d'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
    
        SELECT @Caption2 = '4D-Theater'
           
        SELECT @curY = 1, @curX = 4

        BEGIN TRY

            IF @nav_layout_no > 0 and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_a and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_a, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the 4D-Theater button for today.'
            GOTO DONE

        END CATCH

    /* Butterfly Garden Button */
        
        SELECT @bkColor = null
        SELECT @bkColor = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Butterfly Garden'
        SELECT @bkColor = Isnull(@bkColor,-1)
        IF @bkColor < 0 SELECT @bkColor = @bkColorDefault
    
        SELECT @nav_layout_no = null
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'today_btrfly'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
    
        SELECT @Caption2 = 'Butterfly Garden'
           
        SELECT @curY = 1, @curX = 5

        BEGIN TRY

            IF @nav_layout_no > 0 and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_a and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_a, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Butterfly Garden button for today.'
            GOTO DONE

        END CATCH


    /*  Thrill Ride Buttons - CAPSULE A  */

        SELECT @Caption1 = '', @Caption2 = '', @Caption3 = '', @Caption4 = '[Avail]'
        SELECT @bkColor = @bkColorDefault, @foColor = @foColorToday
        SELECT @return_message = ''
        SELECT @curX = 1, @curY = 2
  
        DECLARE ButtonCursorA INSENSITIVE CURSOR FOR
        SELECT [performance_no], [performance_zone], [performance_time_display], [Production_name], [quick_sale_venue], [season_no], [quick_sale_button_color], [performance_code]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [title_no] = @title_no and [performance_date] = @perf_date and right([performance_code], 1) = 'A'
        ORDER BY CASE WHEN @sort_type = 'Production' THEN [production_name_short] ELSE [performance_time] END, CASE WHEN @sort_type = 'Production' THEN [performance_time] ELSE [production_name_short]END
        OPEN ButtonCursorA
        BEGIN_BUTTON_A_LOOP:

            /*  Records are processed one at a time by retrieving them from the cursor  */

            FETCH NEXT FROM ButtonCursorA INTO @perf_no, @perf_zone_no, @perf_time, @perf_title, @venue_name, @season_no, @bkcolor, @perf_code
            IF @@FETCH_STATUS = -1 GOTO END_BUTTON_A_LOOP

            IF @is_today = 'Y' and @remove_past_performances = 'Y' and convert(char(8),convert(datetime,@perf_time),108) < @cutoff_time GOTO BEGIN_BUTTON_A_LOOP

            /*  If no button color is provided, use the default button color   */
            SELECT @bkColor = IsNull(@bkColor, 0)
            IF @bkcolor <= 0 SELECT @bkcolor = @bkColorDefault

            /*  Caption 1 = CAPSULE A
                Caption 2 = the short title of the production
                caption 3 = the time of the performance     */

            SELECT @Caption1 = right(@perf_date,5) + ' CAPSULE A'
            SELECT @Caption2 = @perf_title
            SELECT @Caption3 =  @perf_time

            /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
                If a button already exists at a position, that position is skipped.
                If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

            WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_a and [ypos] = @curY and [xpos] = @curX) BEGIN
                     IF @cury > @yMaxA GOTO END_BUTTON_A_LOOP
                ELSE IF @curX = @xMaxA SELECT @curY = (@curY + 1), @curX = 1
                ELSE SELECT @curX = (@curX + 1)
            END

            /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

            IF @curY > @yMaxA GOTO END_BUTTON_A_LOOP

            /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

            BEGIN TRY

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_a, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @Perf_no, null, @perf_zone_no, 
                        null, null, 0, null, null, null, null, 4, null, @season_no)

            END TRY
            BEGIN CATCH

                SELECT @return_message = left(Error_message(),100)
                IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert a Thrill Ride Button For Capsule A.'
                GOTO DONE

            END CATCH

            GOTO BEGIN_BUTTON_A_LOOP

        END_BUTTON_A_LOOP:
        CLOSE ButtonCursorA
        DEALLOCATE ButtonCursorA


    /*  Top Row Navigation Buttons - CAPSULE B  */
    
    /* Move To Capsule A Button */
    
        SELECT @Caption1 = 'MOVE TO', @Caption2 = 'CAPSULE A', @Caption3 = '', @caption4 = ''
                   
        SELECT @curY = 1, @curX = 1

        BEGIN TRY

            IF @layout_no_a > 0 and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_b and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_b, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDefault, @foColor, null, @layout_no_a, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Capsule B to A navigation button.'
            GOTO DONE

        END CATCH

    /*  The Rest - Copied from Capsule A Page  */

        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ( [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount], [itemid], 
                                                    [itemotherid], [itemsubid], [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], 
                                                    [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
        SELECT @layout_no_b, [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], 
               [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid]
        FROM [T_SALES_LAYOUT_BUTTON] WHERE layout_id = @layout_no_a and ypos = 1 and xpos > 1


    /*  Thrill Ride Buttons - CAPSULE B  */

        SELECT @Caption1 = '', @Caption2 = '', @Caption3 = '', @Caption4 = '[Avail]'
        SELECT @bkColor = @bkColorDefault, @foColor = @foColorToday
        SELECT @return_message = ''
        SELECT @curX = 1, @curY = 2
  
        DECLARE ButtonCursorB INSENSITIVE CURSOR FOR
        SELECT [performance_no], [performance_zone], [performance_time_display], [Production_name], [quick_sale_venue], [season_no], [quick_sale_button_color], [performance_code]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [title_no] = @title_no and [performance_date] = @perf_date and right([performance_code], 1) = 'B'
        ORDER BY CASE WHEN @sort_type = 'Production' THEN [production_name_short] ELSE [performance_time] END, CASE WHEN @sort_type = 'Production' THEN [performance_time] ELSE [production_name_short]END
        OPEN ButtonCursorB
        BEGIN_BUTTON_B_LOOP:

            /*  Records are processed one at a time by retrieving them from the cursor  */

            FETCH NEXT FROM ButtonCursorB INTO @perf_no, @perf_zone_no, @perf_time, @perf_title, @venue_name, @season_no, @bkcolor, @perf_code
            IF @@FETCH_STATUS = -1 GOTO END_BUTTON_B_LOOP

            IF @is_today = 'Y' and @remove_past_performances = 'Y' and convert(char(8),convert(datetime,@perf_time),108) < @cutoff_time GOTO BEGIN_BUTTON_B_LOOP

            /*  If no button color is provided, use the default button color   */
            SELECT @bkColor = IsNull(@bkColor, 0)
            IF @bkcolor <= 0 SELECT @bkcolor = @bkColorDefault

            /*  Caption 1 = CAPSULE A
                Caption 2 = the short title of the production
                caption 3 = the time of the performance     */

            SELECT @Caption1 = right(@perf_date,5) + ' CAPSULE B'
            SELECT @Caption2 = @perf_title
            SELECT @Caption3 =  @perf_time

            /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
                If a button already exists at a position, that position is skipped.
                If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

            WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no_b and [ypos] = @curY and [xpos] = @curX) BEGIN
                     IF @cury > @yMaxB GOTO END_BUTTON_B_LOOP
                ELSE IF @curX = @xMaxB SELECT @curY = (@curY + 1), @curX = 1
                ELSE SELECT @curX = (@curX + 1)
            END

            /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

            IF @curY > @yMaxB GOTO END_BUTTON_B_LOOP

            /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

            BEGIN TRY

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@layout_no_b, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @Perf_no, null, @perf_zone_no, 
                        null, null, 0, null, null, null, null, 4, null, @season_no)

            END TRY
            BEGIN CATCH

                SELECT @return_message = left(Error_message(),100)
                IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert a Thrill Ride Button For Capsule B.'
                GOTO DONE

            END CATCH

            GOTO BEGIN_BUTTON_B_LOOP

        END_BUTTON_B_LOOP:
        CLOSE ButtonCursorB
        DEALLOCATE ButtonCursorB

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_thrill_ride_360] TO impusers
GO




 