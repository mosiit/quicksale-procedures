USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_current_school_year_programs]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_current_school_year_programs]
GO

/*
    Local Stored Procedure: [LP_QS_current_school_year_programs]


*/
    
CREATE PROCEDURE [dbo].[LP_QS_current_school_year_programs]
        @return_message varchar(255) = Null OUTPUT
AS BEGIN

    DECLARE @launch_id_today int, @max_x_today int, @max_y_today int, @launch_id_tomorrow int, @max_x_tomorrow int, @max_y_tomorrow int
    DECLARE @layout_id int, @month_no int, @year_no int, @month_name varchar(25)
    DECLARE @launchY int, @LaunchX int, @curY int, @curX int, @maxY int, @maxX int, @bTypeNav int, @bTypePerf int, @bkColor int, @bkColorDefault int
    DECLARE @caption1 varchar(30), @caption2 varchar(30), @caption3 varchar(20), @caption4 varchar(20)
    DECLARE @layout_name varchar(30), @start_date char(10), @end_date char(10)
    DECLARE @perf_date char(10), @last_perf_date char(10), @perf_time varchar(10), @venue varchar(30), @production varchar(30), @perf_no int, @perf_zone_no int, @season_no int
        
    SELECT @return_message = ''

    SELECT @month_no = 10
    SELECT @year_no = datepart(year,getdate())
    IF datepart(month,getdate()) <= 6 SELECT @year_no = (@year_no - 1)

    /*  Procedure Constants  */
    
    DECLARE @ZoneModeBestAvail int                      SELECT @ZoneModeBestAvail = 0
    DECLARE @ZoneModePrompt int                         SELECT @ZoneModePrompt = 3
    DECLARE @ZoneModeSpecific int                       SELECT @ZoneModeSpecific = 4

    

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColor = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Exhibit Halls'
    SELECT @bkColor = isnull(@bkColor, @bkColorDefault)

    /*  Button Types: Performance and Navigation buttons are created by this procedure.  The ID numbers for both are needed  */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = isnull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = isnull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    /*  Launcher Pages (today and tomorrow)  */

    SELECT @launch_id_today = [id], @max_x_today = [columns], @max_y_today = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'today_school'
    SELECT @launch_id_today = IsNull(@launch_id_today, 0), @max_x_today = IsNull(@max_x_today, 0), @max_y_today = IsNull(@max_y_today, 0)
    
    SELECT @launch_id_tomorrow = [id], @max_x_tomorrow = [columns], @max_y_tomorrow = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'tomorrow_school'
    SELECT @launch_id_tomorrow = IsNull(@launch_id_tomorrow, 0), @max_x_tomorrow = IsNull(@max_x_tomorrow, 0), @max_y_tomorrow = Isnull(@max_y_tomorrow, 0)

   /*  Procedure will work if it can't find the tomorrow_school layout, but it must find the today_school layout to continue  */

    IF @launch_id_today = 0 or @max_x_today = 0 or @max_y_today = 0 BEGIN
        SELECT @return_message = 'unable to retrieve information about the today_school layout.'
        GOTO DONE
    END

    IF @max_x_tomorrow <> 0 and @max_x_tomorrow < @max_x_today SELECT @max_x_today = @max_x_tomorrow
    IF @max_y_tomorrow <> 0 and @max_y_tomorrow < @max_y_today SELECT @max_y_today = @max_y_tomorrow

    /*  Launcher buttons for the school programs start on row 5  */

    SELECT @LaunchY = 4, @LaunchX = 1

    /*  *****HIGH SCHOOL SCIENCE SERIES*****
        Has its own page and is processed separtely from the other school programs
    */
    
    SELECT @start_date = convert(varchar(4),@year_no) + '/10/01'
    SELECT @end_date = convert(char(4),(@year_no + 1)) + '/06/30'
    SELECT @Caption1 = 'School Program', @Caption2 = 'HS Science Series'
    SELECT @caption3 = '', @caption4 = ''
    SELECT @layout_name = 'school_hsss'
    SELECT @layout_id = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
    SELECT @layout_id = IsNull(@layout_id, 0)
    IF @layout_id > 0 BEGIN

        BEGIN TRY

            DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_id_today and ypos = @launchY and xpos = @LaunchX

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                       [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid])
            VALUES (@launch_id_today, @bTypeNav, @launchY, @LaunchX, @caption1, @caption2, @caption3, @caption4, @bkColor, 0, null, @layout_id, null, null)

            DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_id_tomorrow and ypos = @launchY and xpos = @LaunchX
                
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                       [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid])
            VALUES (@launch_id_tomorrow, @bTypeNav, @launchY, @LaunchX, @caption1, @caption2, @caption3, @caption4, @bkColor, 0, null, @layout_id, null, null)

            SELECT @LaunchX = (@LaunchX + 1)
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),255)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the HSSS launch buttons.'
            GOTO DONE

        END CATCH

        SELECT @maxY = [rows], @maxX = [columns] FROM [dbo].[T_SALES_LAYOUT] WHERE [id] = @layout_id
        SELECT @maxY = IsNull(@maxY, 0), @maxX = IsNull(@maxX, 0)

        IF @maxY > 0 and @maxX > 0 BEGIN

            /* Delete All High School Science Series buttons First, then re-add them  */

            DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_id
            
            SELECT @curY = 1, @curX = 1
            
            /*  High School Science series is identified by the HSSS prefix in the production name.  */
                   
            DECLARE HSSS_cursor INSENSITIVE CURSOR FOR
            SELECT performance_date, performance_time_display, title_name, production_name_short, performance_no, season_no, performance_zone
            FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE 
            WHERE Performance_date between @start_date and @end_date
              and title_name in ('Cahners Theater', 'Current Science & Technology', 'Science Live! Stage')
              and production_name like 'HSSS%'
            ORDER BY performance_date, performance_time, title_name, production_name_short
            OPEN HSSS_cursor
            BEGIN_HSSS_LOOP:

                FETCH NEXT FROM HSSS_cursor INTO @perf_date, @perf_time, @venue, @production, @perf_no, @season_no, @perf_zone_no
                IF @@FETCH_STATUS = -1 GOTO END_HSSS_LOOP

                SELECT @Caption1 = @production, @Caption2 = right(@perf_date,5) + ' @ ' + @perf_time
                SELECT @caption3 = 'CAHNERS', @caption4 = '[Avail]'
               
                IF @curY <= @maxY and @curX <= @maxX BEGIN

                    BEGIN TRY

                        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                                   [zone_sequence], [itemparentid])
                        VALUES (@layout_id, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, 0, null, @Perf_no, null, @perf_zone_no, 
                                null, null, 0, null, null, null, null, @ZoneModeSpecific, null, @season_no)

                        SELECT @curX = (@curX + 1)
                        IF @curX > @maxX SELECT @curY = (@curY + 1), @curX = 1

                    END TRY
                    BEGIN CATCH

                        SELECT @return_message = left(Error_message(),255)
                        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the HSSS performance buttons.'
                        GOTO END_HSSS_LOOP

                    END CATCH

                END

                    GOTO BEGIN_HSSS_LOOP

                END_HSSS_LOOP:
                CLOSE HSSS_cursor
                DEALLOCATE HSSS_cursor

            END
                   

    END
    
    WHILE @month_no <> 7 and @launchY <= @max_y_today BEGIN

        SELECT @start_date = convert(char(10),convert(datetime,convert(varchar(2),@month_no) + '-1-' + convert(varchar(4),@year_no)),111)
        SELECT @end_date = convert(char(10),EOMONTH(@start_date),111)
        SELECT @month_name = datename(month,@start_date)
                  
        SELECT @Caption1 = 'School Program'
        SELECT @Caption2 = @month_name + ', ' + convert(varchar(4), @year_no)
        SELECT @caption3 = '', @caption4 = ''
        SELECT @layout_name = 'school_' + lower(left(@month_name,3))

        SELECT @layout_id = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
        SELECT @layout_id = IsNull(@layout_id, 0)

        IF @layout_id > 0 BEGIN

            BEGIN TRY

                DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_id_today and ypos = @launchY and xpos = @LaunchX

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                           [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid])
                VALUES (@launch_id_today, @bTypeNav, @launchY, @LaunchX, @caption1, @caption2, @caption3, @caption4, @bkColor, 0, null, @layout_id, null, null)

                DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_id_tomorrow and ypos = @launchY and xpos = @LaunchX
                
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                           [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid])
                VALUES (@launch_id_tomorrow, @bTypeNav, @launchY, @LaunchX, @caption1, @caption2, @caption3, @caption4, @bkColor, 0, null, @layout_id, null, null)

            END TRY
            BEGIN CATCH

                SELECT @return_message = left(Error_message(),255)
                IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the School program launch buttons (' + @month_name + ').'
                GOTO DONE

            END CATCH

            SELECT @maxY = [rows], @maxX = [columns] FROM [dbo].[T_SALES_LAYOUT] WHERE [id] = @layout_id
            SELECT @maxY = IsNull(@maxY, 0), @maxX = IsNull(@maxX, 0)

            IF @maxY > 0 and @maxX > 0 BEGIN

                DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_id

                SELECT @curY = 0, @curX = 1, @last_perf_date = ''

                DECLARE school_program_cursor INSENSITIVE CURSOR FOR
                SELECT performance_date, performance_time_display, title_name, production_name_short, performance_no, season_no, performance_zone
                FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE 
                WHERE Performance_date between @start_date and @end_date
                  and title_name in ('Cahners Theater', 'Current Science & Technology', 'Science Live! Stage')
                  and production_name not like 'HSSS%'
                ORDER BY performance_date, performance_time, title_name, production_name_short
                OPEN school_program_cursor
                BEGIN_SCHOOL_PROGRAM_LOOP:

                    FETCH NEXT FROM school_program_cursor INTO @perf_date, @perf_time, @venue, @production, @perf_no, @season_no, @perf_zone_no 
                    IF @@FETCH_STATUS = -1 GOTO END_SCHOOL_PROGRAM_LOOP

                    IF @perf_date <> @last_perf_date BEGIN
                        SELECT @last_perf_date = @perf_date
                        SELECT @curY = (@curY + 1)
                        SELECT @curX = 1
                    END

                    SELECT @Caption1 = @production, @Caption2 = right(@perf_date,5) + ' @ ' + @perf_time
                    SELECT @caption3 = CASE WHEN @venue = 'Cahners Theater' THEN 'CAHNERS'
                                            WHEN @venue = 'Current Science & Technology' THEN 'CS&T'
                                            WHEN @venue = 'Science Live! Stage' THEN 'LiVE!' ELSE '' END
                    SELECT @caption4 = '[Avail]'

                    IF @curY <= @maxY and @curX <= @maxX BEGIN

                        BEGIN TRY

                                  INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
                                  VALUES (@layout_id, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, 0, null, @Perf_no, null, @perf_zone_no, 
                                          null, null, 0, null, null, null, null, @ZoneModeSpecific, null, @season_no)

                            SELECT @curX = (@curX + 1)

                        END TRY
                        BEGIN CATCH

                            SELECT @return_message = left(Error_message(),255)
                            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the school program performance buttons (' + @month_name + ').'
                            GOTO END_SCHOOL_PROGRAM_LOOP

                        END CATCH

                    END

                    GOTO BEGIN_SCHOOL_PROGRAM_LOOP

                END_SCHOOL_PROGRAM_LOOP:
                CLOSE school_program_cursor
                DEALLOCATE school_program_cursor

            END

            SELECT @LaunchX = (@LaunchX + 1)
            IF @LaunchX > @max_x_today SELECT @launchY = (@launchY + 1), @LaunchX = 1

            SELECT @month_no = (@month_no + 1)
            IF @month_no > 12 SELECT @year_no = (@year_no + 1), @month_no = 1
            
        END

    END

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_current_school_year_programs] to impusers
GO

--DECLARE @rtn varchar(255) EXECUTE [dbo].[LP_QS_current_school_year_programs] @return_message = @rtn OUTPUT  PRINT @rtn

