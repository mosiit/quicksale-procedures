USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_summer_courses]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_summer_courses]
GO

CREATE PROCEDURE [dbo].[LP_QS_summer_courses]
        @return_message varchar(100) = null OUTPUT
AS BEGIN
    
    DECLARE @bkColorCourses int, @bkColorExtras int, @bkColorDefault int, @bkColor int
    DECLARE @foColorCoursesMorn int, @foColorCoursesAftr int, @foColorCoursesFull int, @foColor int
    DECLARE @courses_layout_no int, @launch_layout_no int, @perf_no int, @prod_season_no int, @season_no INT, @prod_no int
    DECLARE @perf_zone_no int, @bTypePerf int, @bTypeNav int, @prev_perf_date char(10), @prev_title_name varchar(30)
    DECLARE @prod_name varchar(30), @prod_season_name varchar(30), @perf_date char(10), @grade_level VARCHAR(30)
    DECLARE @caption1 varchar(30), @caption2 varchar(30), @caption3 varchar(30), @caption4 varchar(30), @title_name varchar(30)
    DECLARE @perf_dt datetime, @perf_time varchar(20), @curY int, @curX int, @todayY int, @todayX int, @xMax int, @yMax int

    SELECT @return_message = '', @launch_layout_no = 0

     /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorCourses = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Courses'
    SELECT @bkColorCourses = isnull(@bkColorCourses, @bkColorDefault)

    SELECT @bkColorExtras = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Course Extras'
    SELECT @bkColorExtras = isnull(@bkColorExtras, @bkColorDefault)

    SELECT @foColorCoursesMorn = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Courses Morn'
    SELECT @foColorCoursesMorn = isnull(@foColorCoursesMorn, 0)

    SELECT @foColorCoursesAftr = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Courses Aftr'
    SELECT @foColorCoursesAftr = isnull(@foColorCoursesAftr, 0)

    SELECT @foColorCoursesFull = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Courses Full'
    SELECT @foColorCoursesFull = isnull(@foColorCoursesFull, 0)
    
    /*  The button type is a necessary value.  This procedure deals with performance buttons, so the id number for that type is needed.  If it cannot be determined, the procedure cannot go on.   */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = IsNull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = IsNull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END
   
    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */
            
    SELECT @courses_layout_no = [id], @yMax = [rows], @xMax = [columns]  FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'summer_courses_1'
    SELECT @courses_layout_no = isnull(@courses_layout_no, 0)
    SELECT @yMax = isnull(@yMax, 0), @xMax = isnull(@xMax, 0)
    IF @courses_layout_no <= 0 or @yMax <= 0 or @xMax <= 0 BEGIN
        SELECT @return_message = 'unable to find the summer courses layout information'
        GOTO DONE
    END

    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @courses_layout_no
    
    SELECT @curY = 0, @curX = 0
    SELECT @prev_perf_date = '', @prev_title_name = ''
   
    DECLARE SummerCoursesCursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_dt], [performance_time_display], [production_season_no], [production_no], [production_name], [production_season_name], [performance_zone], [title_name]
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
    WHERE [performance_date] between datename(year,getdate()) + '/07/01' and datename(year,getdate()) + '/08/31' and [title_name] in ('Summer Courses', 'Courses Extras') and [production_name] <> 'Summer Courses'
    ORDER BY [performance_date], [title_name], [performance_time_display], [production_name]
    OPEN SummerCoursesCursor
    BEGIN_SUMMER_COURSES_LOOP:

        SELECT @grade_level = ''
    
        FETCH NEXT FROM SummerCoursesCursor INTO @perf_no, @perf_dt, @perf_time, @prod_season_no, @prod_no, @prod_name, @prod_season_name, @perf_zone_no, @title_name
        IF @@FETCH_STATUS = -1 GOTO END_SUMMER_COURSES_LOOP

        SELECT @grade_level = [value] FROM [dbo].[TX_INV_CONTENT] (NOLOCK) WHERE inv_no = @prod_no AND content_type = (SELECT [id] FROM [dbo].[TR_INV_CONTENT] WHERE [description] = 'Grade Level')
        SELECT @grade_level = ISNULL(@grade_level,'')
        IF @grade_level <> '' SELECT @grade_level = 'Grades ' + @grade_level
        
        SELECT @perf_date = convert(char(10),@perf_dt,111)
        IF @title_name like '%Extras%' SELECT @bkColor = @bkColorExtras ELSE SELECT @bkColor = @bkColorCourses

        /*  Need the Season number that corresponds to the Exhibit Hall Production Season  */

        SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @prod_season_no
        SELECT @season_no = IsNull(@season_no, 0)
        IF @season_no <= 0 BEGIN
            SELECT @return_message = 'unable to determine the season for ' + @prod_season_name
            GOTO END_SUMMER_COURSES_LOOP
        END

        SELECT @caption4 = '[Avail]'
        SELECT @caption3 = @grade_level
        SELECT @Caption2 = @prod_name
        SELECT @Caption1 = left(datename(month,@perf_dt),3) + ' ' + convert(varchar(2),datePart(day,@perf_dt))

             IF @perf_time = '1/2 Day AM' SELECT @caption1 = @caption1 + ' - Morning', @foColor = @foColorCoursesMorn
        ELSE IF @perf_time = '1/2 Day PM' SELECT @caption1 = @caption1 + ' - Afternoon', @foColor = @foColorCoursesAftr
        ELSE IF @perf_time = 'Full Day'   SELECT @caption1 = @caption1 + ' - Full Day', @foColor = @foColorCoursesFull
        ELSE                              SELECT @foColor = CASE WHEN @prod_name = 'Early Drop-Off' THEN @foColorCoursesMorn
                                                                 WHEN @prod_name = 'Late Stay' THEN @foColorCoursesAftr
                                                                 ELSE 0 END
                        
        
        --     IF @perf_time = '1/2 Day AM' SELECT @caption3 = 'MORNING', @foColor = @foColorCoursesMorn
        --ELSE IF @perf_time = '1/2 Day PM' SELECT @caption3 = 'AFTERNOON', @foColor = @foColorCoursesAftr
        --ELSE IF @perf_time = 'Full Day'   SELECT @caption3 = 'FULL DAY', @foColor = @foColorCoursesFull
        --ELSE                              SELECT @Caption3 = '', @foColor = CASE WHEN @prod_name = 'Early Drop-Off' THEN @foColorCoursesMorn
        --                                                                         WHEN @prod_name = 'Late Stay' THEN @foColorCoursesAftr
        --                                                                         ELSE 0 END

        --SELECT @Caption1 = CASE WHEN @title_name like '%Extras%' THEN '' ELSE 'COU ' END + left(datename(weekday,@perf_dt),3) + ' ' + left(datename(month,@perf_dt),3) + ' ' 
        --                 + case WHEN datepart(day,@perf_dt) < 10 THEN '0' + convert(char(1),datepart(day,@perf_dt)) ELSE convert(char(2),datePart(day,@perf_dt)) END
        

        
        

        /*  Insert Exhibit Hall Button into the layout  */

        BEGIN TRY

            IF @perf_date <> @prev_perf_date SELECT @curX = (@curX + 1), @curY = 1, @prev_title_name = ''
            ELSE SELECT @curY = (@curY + 1)

            --If @title_name like '%Extras%' and @prev_title_name not like '%Extras%' SELECT @curY = 11


            SELECT @prev_perf_date = @perf_date, @prev_title_name = @title_name
            
            IF @curY <= @yMax and not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @courses_layout_no and ypos = @curY and xPos = @curx)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
                VALUES (@courses_layout_no, @bTypeperf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @Perf_no, null, @perf_zone_no, 
                        null, null, 0, null, null, null, null, 4, null, @season_no)

            IF @curY = @yMax and @curX = @xMax GOTO END_SUMMER_COURSES_LOOP
            --ELSE IF @curX = @xMax SELECT @curY = (@curY + 1), @curX = 1
            --ELSE SELECT @curX = (@curX + 1)

        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the course button.'
            GOTO END_SUMMER_COURSES_LOOP

        END CATCH

        GOTO BEGIN_SUMMER_COURSES_LOOP

    END_SUMMER_COURSES_LOOP:
    CLOSE SummerCoursesCursor
    DEALLOCATE SummerCoursesCursor
      
    /*  *********************************************************************************************************************************** */  
    
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @@TRANCOUNT > 0 BEGIN
            IF @return_message = '' SELECT @return_message = 'Transaction Error: Non-Committed transaction existed at the end of the procedure.'
            WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        END
      
        IF @return_message = '' SELECT @return_message = 'unknown error'
    

END
GO 

GRANT EXECUTE ON [dbo].[LP_QS_summer_courses] TO impusers
GO



