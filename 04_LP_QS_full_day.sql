USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_full_day]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_full_day]
GO

/*
    Local Stored Procedure: LP_QS_full_day

*/

CREATE PROCEDURE [dbo].[LP_QS_full_day]
        @performance_dt datetime = Null,
        @sort_type varchar(10) = 'Time',     --Time or Production
        @remove_past_performances char(1) = 'N',
        @is_today char(1) = Null,
        @is_tomorrow char(1) = Null,
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

     /* Procedure Variables */

    DECLARE @bkColorDefault int, @foColorToday int  
    DECLARE @bkColor int, @bType int, @perf_no int, @perf_zone_no int, @season_no int
    DECLARE @xMax int, @yMax int, @curX int, @curY int, @full_day_layout_num int, @layout_no int
    
    DECLARE @performance_date char(10), @perf_time char(8), @cutoff_time char(8)
    DECLARE @layout_name varchar(30), @venue_name varchar(20), @perf_title varchar(30)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @full_day_format_name varchar(30)
   
    /*  If Null is passed to the Performance Date parameter, default to today's date
        If Null is passed to the Title number parameter, default to 0 (All titles)
        IF anything other then 'Title' is passed to the Sort Type parameter, default to 'Time'      */
    
    SELECT @performance_dt = IsNull(@performance_dt, getdate())
    SELECT @sort_type = IsNull(@sort_type, 'Time')
    IF @sort_type <> 'Production' SELECT @sort_type = 'Time'
    SELECT @remove_past_performances = IsNull(@remove_past_performances, 'N')
    IF @remove_past_performances <> 'Y' SELECT @remove_past_performances = 'N'

    IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END
    
    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)
             
    /*  Caption 1, 2, and 3 are created by the procedure, but Caption 4 is always the [Avail] token to display number of tickets left */

    SELECT @Caption1 = '', @Caption2 = '', @Caption3 = '', @Caption4 = '[Avail]'
    SELECT @return_message = ''

    /*  Set the performance date to a string (yyyy/mm/dd) and determine if the procedure is working with the current date.
        There are different text colors for today vs future day      */

    SELECT @performance_date = convert(char(10),@performance_dt,111)
    
    /*  The button type is a necessary value.  This procedure deals with performance buttons, so the id number for that type is needed.
        If it cannot be determines, the procedure cannot go on.   */

    SELECT @bType = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bType = IsNull(@bType, 0)
    IF @bType <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    /*  Clear the layout of all performance buttons before continuing.  
        If there are navigation or other types of buttons, Those will be left alone.    */

    IF @Is_Today = 'Y' BEGIN
        SELECT @full_day_format_name = 'today_all_' 
        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] in (SELECT [id] from [dbo].[T_SALES_LAYOUT] WHERE [description] like 'today_all%') and [button_type_id] = @bType    
    END ELSE BEGIN
         SELECT @full_day_format_name = 'tomorrow_all_'
         DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] in (SELECT [id] from [dbo].[T_SALES_LAYOUT] WHERE [description] like 'tomorrow_all%') and [button_type_id] = @bType
    END

    SELECT @curX = 0, @curY = 0, @xMax = 0, @yMax = 0

    SELECT @full_day_layout_num = 1, @layout_no = 0
    
    /*  The cursor is generated in one of two different ways depening on whats passed to the procedure through the parameters.
            1. All Venues sorted by title, then time            2. All Venues sorted by time, then title        */

    DECLARE ButtonCursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_zone], [performance_time_display], [Production_name_short], [quick_sale_venue], [season_no], [quick_sale_button_color] 
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] 
    WHERE performance_date = @performance_date and [performance_type_name] <> 'School Only'
          and [title_name] in ('4-D Theater', 'Adult Offerings', 'Butterfly Garden', 'Hayden Planetarium', 'Mugar Omni Theater', 'Events')
    ORDER BY CASE WHEN @sort_type = 'Production' THEN [title_name] ELSE [performance_time] END,
             CASE WHEN @sort_type = 'Production' THEN [production_name_short] ELSE [title_name] END,
             CASE WHEN @sort_type = 'Production' THEN [performance_time] ELSE [production_name_short] END
    OPEN ButtonCursor
    BEGIN_BUTTON_LOOP:

        /*  Records are processed one at a time by retrieving them from the cursor  */

        FETCH NEXT FROM ButtonCursor INTO @perf_no, @perf_zone_no, @perf_time, @perf_title, @venue_name, @season_no, @bkcolor
        IF @@FETCH_STATUS = -1 GOTO END_BUTTON_LOOP

        IF @is_today = 'Y' and @remove_past_performances = 'Y' and convert(char(8),convert(datetime,@perf_time),108) < @cutoff_time GOTO BEGIN_BUTTON_LOOP
        
        IF @layout_no = 0 or (@curY = @yMax and @curX = (@xMax - 2)) BEGIN

            SELECT @layout_no = null
            
            SELECT @layout_name = ltrim(rtrim(@full_day_format_name)) + convert(char(1),@full_day_layout_num)
            SELECT @full_day_layout_num = (@full_day_layout_num + 1)

            SELECT @layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
            SELECT @layout_No = isnull(@layout_no, 0)
            IF @layout_no <= 0 GOTO END_BUTTON_LOOP

            SELECT @xMax = [columns],  @yMax = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [id] = @layout_no
            SELECT @xMax = isNull(@xMax, 0), @yMax = IsNull(@yMax, 0)
            IF @XMax <= 0 or @ymax <= 0 BEGIN
                SELECT @return_message = 'Unable to determine the dimensions of the layout (' + @layout_name + ').'
                GOTO END_BUTTON_LOOP
            END

            SELECT @curX = 1, @curY = 1

        END 
        
        WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [ypos] = @curY and [xpos] = @curX) BEGIN
            IF @curX = @xMax SELECT @curY = (@curY + 1), @curX = 1
            ELSE SELECT @curX = (@curX + 1)
        END

        /*  If no button color is provided, use the default button color   */

        IF @bkcolor <= 0 SELECT @bkcolor = @bkColorDefault

        /*  Caption 1 = The Quick Sale Venue Name (from content on the title record) plus the date of the performance (mm/dd/yy)
            Caption 2 = the short title of the production
            caption 3 = the time of the performance     */

        SELECT @Caption1 = upper(@venue_name) + ' ' + upper(left(datename(weekday,@performance_date),2)) + ' '+ right(@performance_date,5)
        SELECT @Caption2 = @perf_title
        SELECT @Caption3 =  @perf_time

        /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
            If a button already exists at a position, that position is skipped.
            If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

        /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

        IF @curY > @yMax GOTO END_BUTTON_LOOP

        /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

        BEGIN TRY

   
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bType, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColortoday, null, @Perf_no, null, @perf_zone_no, 
                    null, null, 0, null, null, null, null, 4, null, @season_no)

          END TRY
          BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to create a show button.'
            GOTO END_BUTTON_LOOP

          END CATCH

        GOTO BEGIN_BUTTON_LOOP

    END_BUTTON_LOOP:
    CLOSE ButtonCursor
    DEALLOCATE ButtonCursor

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_full_day] to impusers
GO


