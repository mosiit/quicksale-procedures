USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_vouchers]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_vouchers]
GO

/*
    Local Stored Procedure: LP_QS_vouchers

    Written By: Mark Sherwood - Mar, 2016
      
*/

CREATE PROCEDURE [dbo].[LP_QS_vouchers]
        @performance_dt datetime = Null,
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    DECLARE @title_no int, @layout_no int, @curY int, @curX int, @maxX int, @maxY int, @bTypePerf int
    DECLARE @bkColorDefault int, @bkColorError int, @foColorError int, @bkColorVoucher int, @foColorToday int, @foColorFuture int, @foColor int
    DECLARE @performance_no int, @performance_zone int, @season_no int, @quick_sale_button_color int
    DECLARE @production_name_long varchar(100), @Production_name_short varchar(20), @quick_sale_venue varchar(20), @performance_zone_text varchar(30)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @performance_date char(10)

    SELECT @return_message = ''

     IF @performance_dt is null SELECT @performance_dt = getdate()
     WHILE datepart(month,dateadd(day,1,@performance_dt)) = datepart(month,@performance_dt) 
        SELECT @performance_dt = dateadd(day,1,@performance_dt)

    SELECT @performance_date = convert(char(10),@performance_dt,111)

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorVoucher = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Vouchers'
    SELECT @bkColorVoucher = isnull(@bkColorVoucher, 14085615)
        
    SELECT @bkColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Error'
    SELECT @bkColorError = isnull(@bkColorError, @bkColorDefault)
        
    SELECT @foColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Error'
    SELECT @foColorError = isnull(@foColorError, 0)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)
 
    /*  @ valid title number for vouchers is needed  */
    
    SELECT @title_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_name] = 'Vouchers'
    SELECT @title_no = IsNull(@title_no, 0)
    IF @title_no <= 0  BEGIN
        SELECT @return_message = 'Cannot locate the Vouches title in the database.'
        GOTO DONE
    END

    /*  @ valid layout number for voucher_sales is needed */

    SELECT @layout_no = [id], @maxY = [rows], @maxX = [columns] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'voucher_sales'
    SELECT @layout_no = IsNull(@layout_no, 0), @maxY = IsNull(@maxY, 0), @maxX = IsNull(@maxX, 0)
    IF @layout_no <= 0 BEGIN
        SELECT @return_message = 'Cannot locate the voucher_sales button layout.'
        GOTO DONE
    END ELSE IF @maxY <= 0 or @maxX <= 0 BEGIN
        SELECT @return_message = 'Cannot determine dimensions of the voucher_sales button layout.'
        GOTO DONE
    END

     /*  The button type is a necessary value.  This procedure deals with performance buttons, so the id number for that type is needed.
        If it cannot be determines, the procedure cannot go on.   */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = IsNull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    /**********************************************************************************************************************************/

    /*  VOUCHERS  */

    /*  Default Caption Values */
    
    SELECT @Caption1 = 'VOUCHER', @Caption2 = '', @Caption3 = '', @Caption4 = ''
    SELECT @foColor = @foColorToday
    
    BEGIN TRY

        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while deleting buttons on voucher_sales layout.'
        GOTO DONE

    END CATCH

    DECLARE ButtonCursor INSENSITIVE CURSOR FOR
    SELECT prf.[performance_no], prf.[production_name_long], prf.[performance_zone_text], prf.[Production_name_short], prf.[quick_sale_venue], prf.[season_no], ttl.[quick_sale_button_color], prf.[performance_zone]  
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf 
         LEFT OUTER JOIN [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] as ttl ON ttl.[title_name] = prf.[production_name_long]
    WHERE prf.[title_no] = @title_no and prf.[performance_date] = @performance_date  
    ORDER BY prf.[production_name_long]
    OPEN ButtonCursor
    BEGIN_BUTTON_LOOP:

        /*  Records are processed one at a time by retrieving them from the cursor  */

        FETCH NEXT FROM ButtonCursor INTO @performance_no, @production_name_long, @performance_zone_text, @Production_name_short, @quick_sale_venue, @season_no, @quick_sale_button_color, @performance_zone
         IF @@FETCH_STATUS = -1 GOTO END_BUTTON_LOOP
         
         SELECT @quick_sale_button_color = IsNull(@quick_sale_button_color, @bkColorVoucher)

         IF @production_name_long like '%CAFE%' SELECT @curY = 5 ELSE SELECT @curY = 2
         SELECT @curX = 1

         /*  If no button color is provided, use the default button color   */
        SELECT @quick_sale_button_color = IsNull(@quick_sale_button_color, 0)
        IF @quick_sale_button_color <= 0 SELECT @quick_sale_button_color = @bkColorDefault

        SELECT @Caption2 = @production_name_long
        
        /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
            If a button already exists at a position, that position is skipped.
            If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

        WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [ypos] = @curY and [xpos] = @curX) BEGIN
                 IF @cury > @maxY GOTO END_BUTTON_LOOP
            ELSE IF @curX = @maxX SELECT @curY = (@curY + 1), @curX = 1
            ELSE SELECT @curX = (@curX + 1)
        END

        /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

        IF @curY > @MaxY GOTO END_BUTTON_LOOP

        /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

        BEGIN TRY

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @quick_sale_button_color, @foColor, null, @performance_no, null, @performance_zone, 
                    null, null, 0, null, null, null, null, 4, null, @season_no)


        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while creating button on voucher_sales layout.'
            GOTO DONE

        END CATCH
        GOTO BEGIN_BUTTON_LOOP

    END_BUTTON_LOOP:
    CLOSE ButtonCursor
    DEALLOCATE ButtonCursor

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO 

GRANT EXECUTE ON [dbo].[LP_QS_vouchers] to impusers
GO


