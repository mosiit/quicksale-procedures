USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_city_pass]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_city_pass]
GO

/*
    Local Stored Procedure: [LP_QS_city_pass]


*/
    
CREATE PROCEDURE [dbo].[LP_QS_city_pass]
        @performance_dt datetime = null,
        @is_today char(1) = null,
        @is_tomorrow char(1) = null,
        @display_results char(1) = 'N',
        @return_Message varchar(100) = null OUTPUT
AS BEGIN

    /*  Procedure Constants  */
    
    DECLARE @ZoneModeBestAvail int                      SELECT @ZoneModeBestAvail = 0
    DECLARE @ZoneModePrompt int                         SELECT @ZoneModePrompt = 3
        
    /*  Procedure Variables  */

    DECLARE @fYear int, @season_no int, @errCount int, @curX int, @curY int, @cty_layout_no int
    DECLARE @bkColorCityPass int, @bkColorDefault int, @bkColorError int, @foColor int, @foColorError int
    DECLARE @bTypePerf int, @bTypeNav int
    DECLARE @performance_date char(10)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @cty_season_name varchar(30), @cty_season_no int, @cty_perf_no int, @special_event_count int

    /*  Reset Variables to Defaults  */

    SELECT @return_message = ''
    SELECT @errCount = 0 

    SELECT @performance_dt = IsNull(@performance_dt, getdate())
    SELECT @performance_date = convert(char(10),@performance_dt,111)
    IF datepart(month,@performance_dt) > 6 SELECT @fYear = datepart(year,@performance_dt) + 1 ELSE SELECT @fYear = datepart(year,@performance_dt)

    IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END

    /*  Get Colors from T_DEFAULTS table  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)
    
    SELECT @bkColorCityPass = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color City Pass'
    SELECT @bkColorCityPass = isnull(@bkColorCityPass, @bkColorDefault)

    SELECT @bkColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Error'
    SELECT @bkColorError = isnull(@bkColorError, @bkColorDefault)
        
    SELECT @foColor = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColor = isnull(@foColor, 0)

    SELECT @foColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Error'
    SELECT @foColorError = isnull(@foColorError, @foColor)

    /*  Button Types: Performance and Navigation buttons are created by this procedure.  The ID numbers for both are needed  */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = isnull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = isnull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */

    SELECT @cty_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'city_pass_sales'
    SELECT @cty_layout_no = isnull(@cty_layout_no, 0)
    IF @cty_layout_no <= 0 BEGIN
        SELECT @return_message = 'unable to find the City Pass layout'
        GOTO DONE
    END

    IF @display_results = 'Y' PRINT 'city_pass_sales (' + convert(varchar(10),@cty_layout_no) + ')'

    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @cty_layout_no
        
    /* CityPASS Button */

    /* Need the City Pass Production Season  */

    SELECT @cty_season_name = 'CityPASS FY' + convert(varchar(4),@fYear)
    SELECT @cty_season_no = [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'S' and [description] = @cty_season_name
    SELECT @cty_season_no = isnull(@cty_season_no, 0)
    IF @cty_season_no > 0 BEGIN

        /*  Need the Season number that corresponds to the Exhibit Hall Production Season  */

        SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @cty_season_no
        SELECT @season_no = IsNull(@season_no, 0)
        IF @season_no > 0 BEGIN
        
            /*  There should only be one performance for Public Exhibit Halls on any given day.  The performance number is retrieved by looking for a record in the
                Exhibit Halls Production Season for the date being processed.  If there does happen to be more than one, it will take the last record created (highest performance number)  */

            SELECT @cty_perf_no = max([perf_no]) FROM [dbo].[T_PERF] WHERE convert(char(10),[perf_dt],111) = @performance_date and [prod_season_no] = @cty_season_no
            SELECT @cty_perf_no = isnull(@cty_perf_no, 0)

       END
    END
        
    /*  Set values specific to CityPASS Button  */

    SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) 
                     + ' ' + CASE WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                                  ELSE convert(char(2),datePart(day,@performance_dt)) END
    SELECT @curY = 2, @curX = 2

    /*  Insert CityPASS Button into the layout  */

    BEGIN TRY
    
        IF @cty_perf_no <= 0 BEGIN
    
            SELECT @Caption1 = 'CityPASS', @caption4 = ''
            SELECT @Caption2 = 'NOT FOUND'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@cty_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorError, @foColorError, null, @cty_layout_no, 
                    null, null, null, null, 0, null, null, null, null, null, null, null)

        END ELSE BEGIN

            SELECT @caption4 = '[Avail]'
            SELECT @Caption1 = 'CityPASS'
            SELECT @Caption2 = ''

            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @cty_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@cty_layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorCityPass, @foColor, null, @cty_perf_no, 
                        null, null, null, null, 0, null, null, null, null, @ZoneModeBestAvail, null, @season_no)
       END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the CityPASS button.'
        GOTO DONE
    
    END CATCH

    IF @return_message = '' and @errCount > 0 SELECT @return_message = 'the procedure has finished but with errors'
    ELSE IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

        IF @display_results = 'Y' PRINT @performance_date +  ' - City Pass Button Done (' + @return_message + ')'

END
GO


GRANT EXECUTE ON [dbo].[LP_QS_city_pass] TO impusers
GO

