USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_launch_Screen2_C19]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_launch_Screen2_C19] AS' 
END
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_launch_Screen2_C19] TO impusers, tessitura_app' 
GO

/*
    Local Stored Procedure: [LP_QS_launch_Screen2_C19]

*/
ALTER PROCEDURE [dbo].[LP_QS_launch_Screen2_C19]
        @special_exh_name VARCHAR(50) = '',
        @include_faded_buttons CHAR(1) = 'Y',
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    /*  Procedure Variables  */

        DECLARE @performance_dt DATETIME = CAST(GETDATE() AS DATE);
        DECLARE @last_performance_dt DATETIME = DATEADD(DAY,4,@performance_dt);
                
        DECLARE @exh_title_no INT = 27, @spe_title_no INT = 37, @lvp_title_no INT = 1398, @pla_title_no INT = 1132;
        DECLARE @omn_title_no INT = 161, @4dt_title_no INT = 173, @btr_title_no INT = 157, @arc_title_no INT = 84181;
        DECLARE @mem_season_no INT = 0, @mem_perf_no INT = 0, @fYear INT = 0, @season_no INT = 0;

        DECLARE @ZoneModeBestAvail INT = 0, @ZoneModePrompt INT = 3;

        DECLARE @errCount INT = 0, @bTypePerf INT = 0, @bTypeNav INT = 0;
        DECLARE @launch_layout_box INT = 0, @launch_layout_sci INT = 0, @nav_layout_no INT = 0;
        DECLARE @nav_layout_box INT = 0, @nav_layout_sci INT = 0
        DECLARE @cur_col INT = 1, @btn_count INT = 0, @btn_color INT = 0, @txt_color INT = 0;
        DECLARE @ext_row INT = 1, @exh_row INT = 2, 
                @spe_row INT = CASE WHEN @special_exh_name = '' THEN 0 ELSE 3 END, 
                @arc_row INT = CASE WHEN @special_exh_name = '' THEN 3 ELSE 4 END, 
                @lvp_row INT = CASE WHEN @special_exh_name = '' THEN 4 ELSE 5 END, 
                @pla_row INT = CASE WHEN @special_exh_name = '' THEN 5 ELSE 6 END, 
                @omn_row INT = CASE WHEN @special_exh_name = '' THEN 6 ELSE 7 END, 
                @4dt_row INT = CASE WHEN @special_exh_name = '' THEN 7 ELSE 8 END, 
                @btr_row INT = CASE WHEN @special_exh_name = '' THEN 8 ELSE 9 END;
                
        DECLARE @is_today CHAR(1) = 'N', @is_tomorrow CHAR(1) = 'N';
        DECLARE @performance_date char(10) = '';
        
        DECLARE @Caption1 VARCHAR(30) = '', @Caption2 VARCHAR(30) = '', @Caption3 VARCHAR(30) = '', @Caption4 VARCHAR(30) = '';
        DECLARE @nav_layout_name VARCHAR(30) = '', @rtn VARCHAR(100) = '';

        DECLARE @mem_season_name varchar(30) = '', @mem_abbreviation VARCHAR(30) = '';

        DECLARE @focolor INT = 0;

        DECLARE @bkColorDefault INT = ISNULL((SELECT [default_value] 
                                              FROM [dbo].[T_DEFAULTS] 
                                              WHERE parent_table = 'Museum of Science' 
                                                AND [field_name] =  'QS Button Color Default'), 14085615);

        DECLARE @bkColorExhibitHalls INT = ISNULL((SELECT [default_value]
                                                   FROM [dbo].[T_DEFAULTS] 
                                                   WHERE [parent_table] = 'Museum of Science' 
                                                     AND [field_name] =  'QS Button Color Exhibit Halls'),@bkColorDefault);

        DECLARE @bkColorSpecialExh INT = ISNULL((SELECT [default_value]
                                                 FROM [dbo].[T_DEFAULTS] 
                                                 WHERE [parent_table] = 'Museum of Science' 
                                                   AND [field_name] =  'QS Button Color Special Exh'),@bkColorDefault);

        DECLARE @bkColorLivePresentations INT = ISNULL((SELECT [default_value]
                                                        FROM [dbo].[T_DEFAULTS] 
                                                        WHERE [parent_table] = 'Museum of Science' 
                                                          AND [field_name] =  'QS Button Color Live Present'),@bkColorDefault);

        DECLARE @bkColorPlanetarium INT = ISNULL((SELECT [value]
                                                  FROM [dbo].[TX_INV_CONTENT] 
                                                  WHERE [inv_no] = 1132 
                                                    AND [content_type] = 21),@bkColorDefault);

        DECLARE @bkColorOmni INT = ISNULL((SELECT [value]
                                           FROM [dbo].[TX_INV_CONTENT] 
                                           WHERE [inv_no] = @omn_title_no
                                             AND [content_type] = 21),@bkColorDefault);

        DECLARE @bkColor4D INT = ISNULL((SELECT [value]
                                         FROM [dbo].[TX_INV_CONTENT] 
                                         WHERE [inv_no] = @4dt_title_no
                                           AND [content_type] = 21), @bkColorDefault);

        DECLARE @bkColorButterfly INT = ISNULL((SELECT [value]
                                                 FROM [dbo].[TX_INV_CONTENT] 
                                                 WHERE [inv_no] = @btr_title_no
                                                  AND [content_type] = 21), @bkColorDefault);

        DECLARE @bkColorArcticAdv INT = ISNULL((SELECT [value]
                                                FROM [dbo].[TX_INV_CONTENT] 
                                                WHERE [inv_no] = @arc_title_no
                                                  AND [content_type] = 21), @bkColorDefault);

        DECLARE @bkColorMembership INT = ISNULL((SELECT [default_value]
                                                 FROM [dbo].[T_DEFAULTS] 
                                                 WHERE [parent_table] = 'Museum of Science' 
                                                   AND [field_name] =  'QS Button Color Membership'), @bkColorDefault);

        DECLARE @bkColorDonation INT = ISNULL((SELECT [default_value]
                                               FROM [dbo].[T_DEFAULTS] 
                                               WHERE [parent_table] = 'Museum of Science' 
                                                 AND [field_name] =  'QS Button Color Donation'), @bkColorDefault);

        DECLARE @color_white INT = 16777215;
        DECLARE @color_black INT = 0;
        DECLARE @bkColorDisabled INT = 15263999;
        DECLARE @foColorDisabled INT = 12566527;
        DECLARE @bkColorDrop INT = 11468718;
        DECLARE @drop_season_name VARCHAR(30) = '', @drop_season_no INT = 0;
        DECLARE @perf_no INT = 0, @perf_zone_no INT = 0;
      
    /*  Check Parameters  */
        
        SELECT @special_exh_name = ISNULL(@special_exh_name,'');
        SELECT @include_faded_buttons = ISNULL(@include_faded_buttons,'Y');
        SELECT @return_message = '';
  
    /*  Button Types: Performance and Navigation buttons are created by this procedure.  The ID numbers for both are needed  */

        SELECT @bTypePerf = ISNULL([id],0) FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance';
        SELECT @bTypeNav = ISNULL([id], 0) FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator';

        IF @bTypePerf <= 0 OR @bTypeNav <= 0 BEGIN
            SELECT @return_message = 'unable to determine id number for button type';
            GOTO DONE;
        END;
                    
    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */

        SELECT @launch_layout_box = ISNULL([id], 0) 
               FROM [dbo].[T_SALES_LAYOUT] 
               WHERE [description] = 'Box Office Launch 2';

        SELECT @launch_layout_sci = ISNULL([id], 0) 
               FROM [dbo].[T_SALES_LAYOUT] 
               WHERE [description] = 'Science Central Launch 2';
        
        IF @launch_layout_box <= 0 OR @launch_layout_sci <= 0 BEGIN
            SELECT @return_message = 'unable to find the launch layout id number';
            GOTO DONE;
        END;

        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] 
        WHERE [layout_id] IN (@launch_layout_box, @launch_layout_sci);
       
    /* Daily Buttons */

        --First Performance Date is today unless today is before July 21    
        SELECT @performance_dt = CAST(GETDATE() AS DATE)
        IF @performance_dt < '7-21-2020' SELECT @performance_dt = '7-21-2020';

        SELECT @performance_dt = DATEADD(DAY, 5, @performance_dt)
        
        SELECT @last_performance_dt = DATEADD(DAY, 4, @performance_dt);
        
    /*  Process for each date in the range  */

        PRODUCTS:

        WHILE @performance_dt <= @last_performance_dt BEGIN
        
            SELECT @performance_date = CONVERT(CHAR(10),@performance_date,111);

            IF @performance_dt = CAST(GETDATE() AS DATE) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N';
            IF @performance_dt = CAST(DATEADD(DAY,1,GETDATE()) AS DATE) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N';
    
            IF @is_today = 'Y' SELECT @Caption1 = 'TODAY'
            ELSE IF @is_tomorrow = 'Y' SELECT @Caption1 = 'TOMORROW'
            ELSE SELECT @Caption1 = FORMAT(@performance_dt,'ddd MMM dd yyyy');
            
            SELECT @Caption3 = '';

            SELECT @Caption2 = 'Exhibit Halls';
            SELECT @nav_layout_name = 'future_exh_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;
     
            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @exh_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColorExhibitHalls,           @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColorExhibitHalls, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;
                
                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @exh_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @exh_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);

            END

            IF @special_exh_name <> '' BEGIN

                SELECT @Caption2 = @special_exh_name;
                SELECT @nav_layout_name = 'special_exhibit_add_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
                SELECT @nav_layout_no = ISNULL([id], 0) 
                                        FROM [dbo].[T_SALES_LAYOUT] 
                                        WHERE [description] = @nav_layout_name;

                IF @nav_layout_no > 0 BEGIN

                    EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                            @performance_dt = @performance_dt,              @title_no = @spe_title_no,                  @layout_no = @nav_layout_no,
                            @button_color = @bkColorSpecialExh,             @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                    IF @rtn <> 'success' SELECT @errCount += 1;

                    IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                    ELSE SELECT @btn_color = @bkColorSpecialExh, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;
                    
                    IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                                   [zone_sequence], [itemparentid])
                        VALUES (@launch_layout_box, @bTypeNav, @spe_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                                null, null, null, null, 0, null, null, null, null, null, null, NULL),
                               (@launch_layout_sci, @bTypeNav, @spe_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                                null, null, null, null, 0, null, null, null, null, null, null, NULL);

                END;

            END;
            
            SELECT @Caption2 = 'Live Presentations';
            SELECT @nav_layout_name = 'future_live_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;
     
            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @lvp_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColorLivePresentations,      @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColorLivePresentations, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;

                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @lvp_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @lvp_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);

            END;

            SELECT @Caption2 = 'Hayden Planetarium';
            SELECT @nav_layout_name = 'future_planet_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;
     
            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @pla_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColorPlanetarium,            @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColorPlanetarium, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;

                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @pla_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @pla_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);

            END;

            SELECT @Caption2 = 'Mugar Omni Theater';
            SELECT @nav_layout_name = 'future_omni_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;
     
     
            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @omn_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColorOmni,                   @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColorOmni, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;

                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @omn_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @omn_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);

            END;
            
            SELECT @Caption2 = '4-D Theater';
            SELECT @nav_layout_name = 'future_4d_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;
     
            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @4dt_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColor4d,                     @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColor4D, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;

                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @4dt_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @4dt_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);

            END;


            SELECT @Caption2 = 'Butterfly Garden';
            SELECT @nav_layout_name = 'future_btrfly_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;
     
            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @btr_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColorButterfly,              @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColorButterfly, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;

                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @btr_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @btr_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);
            END;

            SELECT @Caption2 = 'Arctic Adventure';
            SELECT @nav_layout_name = 'future_arctic_' + CASE WHEN (@cur_col + 5) < 10 THEN '0' ELSE '' END + CAST((@cur_col + 5) AS VARCHAR(2));
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @nav_layout_name;

            IF @nav_layout_no > 0 BEGIN

                EXECUTE [dbo].[LP_QS_timed_admissions_C19]
                        @performance_dt = @performance_dt,              @title_no = @arc_title_no,                  @layout_no = @nav_layout_no,
                        @button_color = @bkColorArcticAdv,              @buttons_created = @btn_count OUTPUT,       @return_message = @rtn OUTPUT;

                IF @rtn <> 'success' SELECT @errCount += 1;

                IF @btn_count = 0 SELECT @btn_color = @bkColorDisabled, @txt_color = @foColorDisabled, @nav_layout_box = @launch_layout_box, @nav_layout_sci = @launch_layout_sci
                ELSE SELECT @btn_color = @bkColorArcticAdv, @txt_color = @color_black, @nav_layout_box = @nav_layout_no, @nav_layout_sci = @nav_layout_no;

                IF @btn_count > 0 OR @include_faded_buttons = 'Y'
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES(@launch_layout_box, @bTypeNav, @arc_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_box, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL),
                          (@launch_layout_sci, @bTypeNav, @arc_row, @cur_col, @Caption1, @Caption2, @Caption3, @Caption4, @btn_color, @txt_color, null, @nav_layout_sci, 
                           null, null, null, null, 0, null, null, null, null, null, null, NULL);
            END;
                        
            SELECT @cur_col += 1;
            SELECT @performance_dt = DATEADD(DAY, 1, @performance_dt);

        END
                
        PREV5:

            SELECT @Caption1 = 'Previous Five Days',
                   @caption2 = '< < < < < < < < < <',
                   @caption3 = '',
                   @Caption4 = '';
                
            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = 'Box Office Launch';
        
            IF @nav_layout_no > 0
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES(@launch_layout_box, @bTypeNav, @ext_row, 5, @Caption1, @Caption2, @Caption3, @Caption4, @color_black, @color_white, null, @nav_layout_no, 
                       null, null, null, null, 0, null, null, null, null, null, null, NULL);

            SELECT @nav_layout_no = ISNULL([id], 0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = 'Science Central Launch';
            IF @nav_layout_no > 0
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_sci, @bTypeNav, @ext_row, 5, @Caption1, @Caption2, @Caption3, @Caption4, @color_black, @color_white, null, @nav_layout_no, 
                        NULL, null, null, null, 0, null, null, null, null, null, null, NULL);

             IF @return_message = '' AND @errCount > 0 SELECT @return_message = 'the procedure has finished but with errors'
        ELSE IF @return_message = '' SELECT @return_message = 'success';

        DONE:

            IF @return_message = '' SELECT @return_message = 'unknown error';

END;
GO

--DECLARE @rtn VARCHAR(100) EXECUTE [dbo].[LP_QS_launch_Screen2_C19] @special_exh_name = 'Science Behind Pixar', @include_faded_buttons = 'Y', @return_message = @rtn OUTPUT    PRINT @rtn







        --DECLARE @exh_row INT = 1, 
        --        @spe_row INT = CASE WHEN @special_exh_name = '' THEN 0 ELSE 2 END, 
        --        @lvp_row INT = CASE WHEN @special_exh_name = '' THEN 2 ELSE 3 END, 
        --        @pla_row INT = CASE WHEN @special_exh_name = '' THEN 3 ELSE 4 END, 
        --        @omn_row INT = CASE WHEN @special_exh_name = '' THEN 4 ELSE 5 END, 
        --        @4dt_row INT = CASE WHEN @special_exh_name = '' THEN 5 ELSE 6 END, 
        --        @btr_row INT = CASE WHEN @special_exh_name = '' THEN 6 ELSE 7 END, 
        --        @ext_row INT = CASE WHEN @special_exh_name = '' THEN 8 ELSE 9 END;