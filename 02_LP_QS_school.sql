USE [impresario]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_school]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_school]
GO

/*
    Local Stored Procedure: LP_QS_today_school

    Written By: Mark Sherwood - Jan, 2016
    Updated: Mar, 2016
      
*/

CREATE PROCEDURE [dbo].[LP_QS_school]
        @performance_dt datetime = Null,
        @is_today char(1) = Null,
        @is_tomorrow char(1) = Null,
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    /*  Check Parameters:  If Null is passed to the Performance Date parameter, default to today's date
                           IF anything other then 'Title' is passed to the Sort Type parameter, default to 'Time'      */
    
    SELECT @performance_dt = IsNull(@performance_dt, getdate())
    SELECT @return_message = ''

    /*  Procedure Constants  */
    
    DECLARE @ZoneModeBestAvail int                      SELECT @ZoneModeBestAvail = 0
    DECLARE @ZoneModePrompt int                         SELECT @ZoneModePrompt = 3
    DECLARE @ZoneModeSpecific int                       SELECT @ZoneModeSpecific = 4
    
    /*  Procedure Variables  */

    DECLARE @bkColorDefault int, @bkColorExhibitHalls int, @bkColorSchoolLunch int, @bkColorShow int, @bkColorError int, @foColorError int, @foColorToday int                           
    DECLARE @title_no int, @quick_sale_venue varchar(6), @layout_no int, @nav_layout_no int, @errCount int, @Perf_no int, @Perf_zone_no int
    DECLARE @fYear int, @exh_season_no int, @exh_perf_no int, @exh_zone_no int, @mem_season_no int, @mem_perf_no int, @season_no int, @bTypePerf int, @bTypeNav int
    DECLARE @xMax int, @yMax int, @curY int, @curX int, @DayOfWeek int, @lun_season_no int, @lun_perf_no int, @lun_zone_no int, @format_days int
    DECLARE @performance_date char(10), @venue_name varchar(30), @performance_time char(8), @production_title varchar(30)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @exh_season_name varchar(30), @mem_season_name varchar(30), @rtn varchar(100)
    DECLARE @lun_season_name varchar(30), @lun_perf_time varchar(20), @format_name varchar(30)

    /*  Initialize variables that need it.  Convert date to the yyyy/mm/dd format and determine the current fiscal year  */

    SELECT @errCount = 0 
    SELECT @performance_date = convert(char(10),@performance_dt,111)
    IF datepart(month,@performance_dt) > 6 SELECT @fYear = datepart(year,@performance_dt) + 1 ELSE SELECT @fYear = datepart(year,@performance_dt)

    IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorExhibitHalls = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Exhibit Halls'
    SELECT @bkColorExhibitHalls = isnull(@bkColorExhibitHalls, @bkColorDefault)

    SELECT @bkColorSchoolLunch = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color School Lunch'
    SELECT @bkColorSchoolLunch = isnull(@bkColorSchoolLunch, @bkColorDefault)
        
    SELECT @bkColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Error'
    SELECT @bkColorError = isnull(@bkColorError, @bkColorDefault)
        
    SELECT @foColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Error'
    SELECT @foColorError = isnull(@foColorError, 0)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)
            
    /*  Button Types: Performance and Navigation buttons are created by this procedure.  The ID numbers for both are needed  */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = isnull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = isnull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    /*  *********************************************************************************************************************************** */  
        
    /* School Screen */

    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */

        SELECT @format_Days = Datediff(day,convert(char(10),getdate(),111),@performance_date) 
        IF @format_days < 0 SELECT @format_days = 0
    
        IF @is_today = 'Y' SELECT @format_name = 'today_school'
        ELSE IF @is_tomorrow = 'Y' SELECT @format_name = 'tomorrow_school'
        ELSE SELECT @format_name = 'future_school_' + CASE WHEN (@format_days - 1) < 10 THEN '0' + convert(char(1),(@format_days - 1)) ELSE convert(char(2),(@format_days - 1)) END
                                   
        SELECT @layout_no = [id], @xMax = [columns], @yMax = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @format_name
        SELECT @layout_no = isnull(@layout_no, 0)
        SELECT @xMax = isnull(@xMax, 0)
        SELECT @yMax = isnull(@yMax, 0)
        IF @layout_no <= 0 or @yMax <= 0 or @xMax <= 0 BEGIN
            SELECT @return_message = 'unable to find the today_school layout information'
            GOTO DONE
        END
        
        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no

    /*  *********************************************************************************************************************************** */  
       
    /* Box Office Launch Screen  */

    /* SCHOOL - Exhibit Hall Buttons  */

    /*  Reset Variables  */

    SELECT @exh_season_no = 0, @season_no = 0, @exh_perf_no = 0, @exh_zone_no = 0

    /* Need the Exhibit Hall Production Season  */
           
    SELECT @exh_season_name = 'Exhibit Halls School FY' + convert(varchar(4),@fYear)
    SELECT @exh_season_no = [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'S' and [description] = @exh_season_name
    SELECT @exh_season_no = isnull(@exh_season_no, 0)
    IF @exh_season_no > 0 BEGIN

        /*  Need the Season number that corresponds to the Exhibit Hall Production Season  */
        SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @exh_season_no
        SELECT @season_no = IsNull(@season_no, 0)
        IF @season_no > 0 BEGIN
        
            /*  There should only be one performance for Public Exhibit Halls on any given day.  The performance number is retrieved by looking for a record in the
            Exhibit Halls Production Season for the date being processed.  If there does happen to be more than one, it will take the last record created (highest performance number)  */
            SELECT @exh_perf_no = max([perf_no]) FROM [dbo].[T_PERF] WHERE convert(char(10),[perf_dt],111) = convert(char(10),@performance_dt,111) and [prod_season_no] = @exh_season_no
            SELECT @exh_perf_no = isnull(@exh_perf_no, 0)

        END

    END

    /*  Set values specific to Today's MORNING Exhibit Hall Button  */

    SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) + ' ' + case WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                                                                                                                                  ELSE convert(char(2),datePart(day,@performance_dt)) END
    SELECT @curY = 1, @curX = 1

    /*  Get Zone for MORNING Exhibit Hall Product  */
    
    IF @exh_perf_no > 0 BEGIN
        SELECT @exh_zone_no = [zone_no] FROM [dbo].[T_ZONE] WHERE [zmap_no] = (SELECT [zmap_no] FROM [dbo].[T_PERF] WHERE [perf_no] = @exh_perf_no) and [description] = '09:00'
        SELECT @exh_zone_no = IsNull(@exh_zone_no, 0)
    END

    /*  Insert Morning School Exhibit Hall Button into the layout  */

    BEGIN TRY

        IF @exh_perf_no <= 0 or @exh_zone_no <= 0 BEGIN

            SELECT @Caption1 = 'MORNING EXH', @caption4 = ''
            SELECT @Caption2 = 'NOT FOUND'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorError, @foColorError, null, @layout_no, 
                    null, null, null, null, 0, null, null, null, null, null, null, null)
        
        END ELSE BEGIN

            SELECT @Caption1 = 'MORNING', @caption4 = '[Avail]'
            SELECT @Caption2 = 'School Exhibit Halls'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorExhibitHalls, @foColortoday, null, @exh_perf_no, 
                    null, @exh_zone_no, null, null, 0, null, null, null, null, @ZoneModeSpecific, null, @season_no)
        END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the morning school exhibit halls button.'
        GOTO DONE

    END CATCH


    /*  Set values specific to Today's AFTERNOON Exhibit Hall Button  */
    SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) + ' ' + case WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                                                                                                                                  ELSE convert(char(2),datePart(day,@performance_dt)) END
    SELECT @curY = 1, @curX = 2

    /*  Get Zone for AFTERNOON Exhibit Hall Product  */

    IF @exh_perf_no > 0 BEGIN
        SELECT @exh_zone_no = null
        SELECT @exh_zone_no = [zone_no] FROM [dbo].[T_ZONE] WHERE [zmap_no] = (SELECT [zmap_no] FROM [dbo].[T_PERF] WHERE [perf_no] = @exh_perf_no) and [description] = '13:00'
        SELECT @exh_zone_no = IsNull(@exh_zone_no, 0)
    END


    /*  Insert Afternoon School Exhibit Hall Button into the layout  */

    BEGIN TRY

        IF @exh_perf_no <= 0 or @exh_zone_no <= 0 BEGIN

            SELECT @Caption1 = 'AFTERNOON EXH', @caption4 = ''
            SELECT @Caption2 = 'NOT FOUND'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorError, @foColorError, null, @layout_no, 
                    null, null, null, null, 0, null, null, null, null, null, null, null)

        END ELSE BEGIN

            SELECT @Caption1 = 'AFTERNOON', @caption4 = '[Avail]'
            SELECT @Caption2 = 'School Exhibit Hall'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorExhibitHalls, @foColortoday, null, @exh_perf_no, 
                    null, @exh_zone_no, null, null, 0, null, null, null, null, @ZoneModeSpecific, null, @season_no)

        END

    END TRY
    BEGIN CATCH
    
        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the afternoon school exhibit halls button.'
        GOTO DONE
    
    END CATCH

    /*  *********************************************************************************************************************************** */  

    SELECT @Caption4 = '[Avail]'

    /* SCHOOL - LUNCH Buttons  */

    SELECT @curY = 2, @curX = 1
        
    /*  Reset Variables  */

    SELECT @lun_season_no = null, @season_no = null, @lun_perf_no = null, @lun_zone_no = null
    
    /* Need the School Lunch Production Season  */
           
    SELECT @lun_season_name = 'School Lunch FY' + convert(varchar(4),@fYear)
    SELECT @lun_season_no = [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'S' and [description] = @lun_season_name
    SELECT @lun_season_no = isnull(@lun_season_no, 0)
    IF @lun_season_no > 0 BEGIN
        
        /*  Need the Season number that corresponds to the School Lunch Production Season  */

        SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @lun_season_no
        SELECT @season_no = IsNull(@season_no, 0)
        IF @season_no > 0 BEGIN
        
            /*  There should only be one performance for School Lunch on any given day.  The performance number is retrieved by looking for a record in the
                School Lunch Production Season for the date being processed.  If there does happen to be more than one, it will take the last record created (highest performance number)  */

            SELECT @lun_perf_no = null

            SELECT @lun_perf_no = max([perf_no]) FROM [dbo].[T_PERF] WHERE convert(char(10),[perf_dt],111) = @performance_date and [prod_season_no] = @lun_season_no
            SELECT @lun_perf_no = isnull(@lun_perf_no, 0)
            IF @lun_perf_no > 0 BEGIN

                DECLARE SchoolLunchCursor INSENSITIVE CURSOR FOR
                SELECT [zone_no], [short_desc] FROM [dbo].[T_ZONE] WHERE [zmap_no] = (SELECT [zmap_no] FROM [dbo].[T_PERF] WHERE [perf_no] = @lun_perf_no) and zone_no > 0 ORDER BY [description]
                OPEN SchoolLunchCursor
                BEGIN_SCHOOL_LUNCH_LOOP:
    
                    FETCH NEXT FROM SchoolLunchCursor INTO @lun_zone_no, @lun_perf_time
                    IF @@FETCH_STATUS = -1 GOTO END_SCHOOL_LUNCH_LOOP

                    /*  Caption 1 = The Quick Sale Venue Name (from content on the title record) plus the date of the performance (mm/dd/yy)
                        Caption 2 = the short title of the production
                        caption 3 = the time of the performance     */

                    SELECT @Caption1 = 'LUNCH ' + upper(left(datename(weekday,@performance_dt),2)) + ' '+ right(@performance_date,5)
                    SELECT @Caption2 = 'School Lunch'
                    SELECT @Caption3 =  @lun_perf_time

                    /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
                        If a button already exists at a position, that position is skipped.
                        If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

                    WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [ypos] = @curY and [xpos] = @curX) BEGIN
                             IF @cury > @yMax GOTO END_SCHOOL_LUNCH_LOOP
                        ELSE IF @curX = @xMax SELECT @curY = (@curY + 1), @curX = 1
                        ELSE SELECT @curX = (@curX + 1)
                    END

                    /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

                    IF @curY > @yMax GOTO END_SCHOOL_LUNCH_LOOP

                    /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

                    BEGIN TRY
        
                        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                                   [zone_sequence], [itemparentid])
                        VALUES (@layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorSchoolLunch, @foColortoday, null, @lun_Perf_no, null, @lun_zone_no, 
                                null, null, 0, null, null, null, null, @ZoneModeSpecific, null, @season_no)
                    END TRY
                    BEGIN CATCH
        
                        SELECT @return_message = left(Error_message(),100)
                        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the school lunch buttons.'
                        GOTO DONE

                    END CATCH

                    GOTO BEGIN_SCHOOL_LUNCH_LOOP

                END_SCHOOL_LUNCH_LOOP:
                CLOSE SchoolLunchCursor
                DEALLOCATE SchoolLunchCursor

            END
        END
    END

    /*  School Only Shows for Omni and Planetarium.
            1. Specific venue sorted by title, then time        2. Specific Venue sorted by time, then title        */

    DECLARE SchoolOnlyShowsCursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_zone], [performance_time_display], [Production_name_short], [quick_sale_venue], [season_no], [quick_sale_button_color] 
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE title_name in ('Hayden Planetarium', 'Mugar Omni Theater') and performance_date = @performance_date  and [performance_type_name] = 'School Only'
    ORDER BY [title_name], [performance_time]
    OPEN SchoolOnlyShowsCursor
    BEGIN_SCHOOL_ONLY_SHOWS_LOOP:

    /*  Records are processed one at a time by retrieving them from the cursor  */

        FETCH NEXT FROM SchoolOnlyShowsCursor INTO @perf_no, @perf_zone_no, @performance_time, @production_title, @venue_name, @season_no, @bkColorShow
        IF @@FETCH_STATUS = -1 GOTO END_SCHOOL_ONLY_SHOWS_LOOP

        /*  If no button color is provided, use the default button color   */
        SELECT @bkColorShow = IsNull(@bkColorShow, @bkColorDefault)

        /*  Caption 1 = The Quick Sale Venue Name (from content on the title record) plus the date of the performance (mm/dd/yy)
            Caption 2 = the short title of the production
            caption 3 = the time of the performance     */

        SELECT @Caption1 = upper(@venue_name) + ' ' + upper(left(datename(weekday,@performance_date),2)) + ' '+ right(@performance_date,5)
        SELECT @Caption2 = @production_title
        SELECT @Caption3 =  @performance_time

        /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
            If a button already exists at a position, that position is skipped.
            If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

        WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [ypos] = @curY and [xpos] = @curX) BEGIN
                 IF @cury > @yMax GOTO END_SCHOOL_ONLY_SHOWS_LOOP
            ELSE IF @curX = @xMax SELECT @curY = (@curY + 1), @curX = 1
            ELSE SELECT @curX = (@curX + 1)
        END

        /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

        IF @curY > @yMax GOTO END_SCHOOL_ONLY_SHOWS_LOOP

        /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

        BEGIN TRY

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorshow, @foColorToday, null, @Perf_no, null, @perf_zone_no, 
                    null, null, 0, null, null, null, null, 4, null, @season_no)

        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the school show buttons.'
            GOTO DONE

        END CATCH

        GOTO BEGIN_SCHOOL_ONLY_SHOWS_LOOP

    END_SCHOOL_ONLY_SHOWS_LOOP:
    CLOSE SchoolOnlyShowsCursor
    DEALLOCATE SchoolOnlyShowsCursor

    BEGIN TRY

        IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [button_type_id] = @bTypePerf)
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], 
                                                      [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], 
                                                      [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypeNav, 2, 2, 'NOTHING FOUND', 'No Performances', '', '', @bkColorDefault, @foColorToday, null, @layout_no, null, null, null, null, 0, null, null, null, null, null, null, null)
            
    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the "No Perfs Found" button.'
        GOTO DONE

    END CATCH
          
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_school] TO impusers
GO



