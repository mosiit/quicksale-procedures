USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_special_exhibit_adjustments]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_special_exhibit_adjustments] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_special_exhibit_adjustments] TO impusers' 
END
GO
/*
    This procedure was written 8/30/2019 to adjust buttons for new pricing policies.  
    This will be run at the end of the Quick Sale button procedures.

    Doing it this way was easier, faster, and had less chance of breaking something esle
    while the adjustments were made.  It also has the added bonus of easy reversion.
    If the museum changes its mind and wants to go back to the way it was, simply comment out
    or delete the line at the very end of the LP_QS_daily_button_build procedure that calls this procedure.
*/
ALTER PROCEDURE [dbo].[LP_QS_special_exhibit_adjustments]
        @start_dt DATETIME = Null,
        @exhibit_name VARCHAR(30) = 'Body Worlds',
        @return_message VARCHAR(1000) = NULL OUTPUT
AS BEGIN
    
    DECLARE @color_white INT = 16777215;
    DECLARE @color_black INT = 0;
    
    DECLARE @special_exhibit_layout_id INT = 0, @first_voucher_row INT = 0
    DECLARE @sci_central_launch_layout_id INT = 0, @today_school_layout_id INT = 0
    DECLARE @special_add_01_layout_id INT = 0

    DECLARE @rtn_msg VARCHAR(1000) = ''

    SELECT @return_message = ''

    SELECT @special_exhibit_layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'special_exhibit'
    SELECT @sci_central_launch_layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Science Central Launch'
    SELECT @today_school_layout_id = ISNULL([id],0) FROM dbo.T_SALES_LAYOUT WHERE [description] = 'today_school'
    SELECT @special_add_01_layout_id = ISNULL([id],0) FROM dbo.T_SALES_LAYOUT WHERE [description] = 'special_exhibit_add_01'
    
    IF ISNULL(@special_exhibit_layout_id,0) > 0 BEGIN

        SELECT @first_voucher_row = ISNULL(MIN(ypos),4)
        FROM [dbo].[T_SALES_LAYOUT_BUTTON]
        WHERE layout_id = @special_exhibit_layout_id AND caption1 = 'Exhibit Voucher'

        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON]
        WHERE layout_id = @special_exhibit_layout_id
          AND ypos = @first_voucher_row
          AND xpos = 1

        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4],
                                                   [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid],
                                                   [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], 
                                                   [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
    
        SELECT @special_exhibit_layout_id, [button_type_id], @first_voucher_row, 1, 'Exhibit Halls', FORMAT(GETDATE(),'MM/dd/yyyy'), 'Public', '',
               @color_white, @color_black, [itemamount], [itemid], [itemotherid], [itemsubid],
               [product_start_mode],  [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], 
               [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid]
        FROM [dbo].[T_SALES_LAYOUT_BUTTON] 
        WHERE [layout_id] = @sci_central_launch_layout_id 
        AND [caption2] = 'Public Exhibit Hall'

                UPDATE [dbo].[T_SALES_LAYOUT_BUTTON]
        SET [caption1] = 'Exhibit Halls',
            [caption3] = 'Public'
        WHERE layout_id = @special_exhibit_layout_id
          AND caption1 = 'Exhibit Voucher'


    END

    UPDATE [dbo].[T_SALES_LAYOUT_BUTTON]
    SET Caption1 = 'Exhibit Halls',
        Caption3 = 'Public'
    WHERE [layout_id] IN (SELECT [id] 
                          FROM [dbo].[T_SALES_LAYOUT] 
                          WHERE [description] LIKE 'special_exhibit_add%')
      AND [caption1] = 'Exhibit Voucher'


    UPDATE [dbo].[T_SALES_LAYOUT_BUTTON]
    SET [caption1] = 'Exhibit Halls',
        [caption3] = 'Public'
    WHERE [layout_id] IN (SELECT [id] 
                          FROM [dbo].[T_SALES_LAYOUT] 
                          WHERE [description] LIKE 'special_exhibit_aud%')
      AND [caption1] = 'Exhibit Voucher'


    IF @return_message = '' SELECT @return_message = 'success'

END
GO
    
