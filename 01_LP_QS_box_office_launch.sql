USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_box_office_launch]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_box_office_launch]
GO

/*
    Local Stored Procedure: [LP_QS_box_office_launch]


*/
    
CREATE PROCEDURE [dbo].[LP_QS_box_office_launch]
        @performance_dt datetime = Null,
        @is_today char(1) = Null,
        @is_tomorrow char(1) = Null,
        @display_results char(1) = 'N',
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    /*  Check Parameters:  If Null is passed to the Performance Date parameter, default to today's date
                           IF anything other then 'Title' is passed to the Sort Type parameter, default to 'Time'      */
    
    SELECT @performance_dt = IsNull(@performance_dt, getdate())
    SELECT @return_message = ''

    /*  Procedure Constants  */
    
    DECLARE @ZoneModeBestAvail int                      SELECT @ZoneModeBestAvail = 0
    DECLARE @ZoneModePrompt int                         SELECT @ZoneModePrompt = 3

    /*  Procedure Variables  */

    DECLARE @bkColorDefault int, @bkColorExhibitHalls int, @bkColorMembership int, @bkColorAllPerf int, @foColorToday int, @foColorFuture int, @foColor int, @bkColorDonation int, @bkColorCityPass int
    DECLARE @title_no int, @quick_sale_venue varchar(6), @layout_no int, @nav_layout_no int, @launch_layout_no int, @tomorrow_layout_no int, @errCount int
    DECLARE @fYear int, @exh_season_no int, @exh_perf_no int, @exh_zone_no int, @mem_season_no int, @mem_perf_no int, @season_no int, @bTypePerf int, @bTypeNav int
    DECLARE @curY int, @curX int, @bkColorOmni int, @bkColorPlanet int, @bkColor4D int, @bkColorButterfly int, @bkColorThrill int, @DayOfWeek int
    DECLARE @special_event_count int
    DECLARE @bkColorError int, @foColorError int, @performance_date char(10)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @layout_name varchar(30), @exh_season_name varchar(30), @mem_season_name varchar(30), @mem_abbreviation varchar(4), @rtn varchar(100)

    /*  New Variables added 1/23/2019 to accomodate the Cash Drop button  */
    
    DECLARE @drop_season_name VARCHAR(30) = '', @drop_season_no INT = 0
    DECLARE @bType INT = 0, @perf_no INT = 0, @perf_zone_no INT = 0
    DECLARE @bkColorDrop INT = 11468718

        
    /*  Initialize variables that need it Convert date to the yyyy/mm/dd format and determine the current fiscal year  */

    SELECT @errCount = 0 
    SELECT @performance_date = convert(char(10),@performance_dt,111)
    IF datepart(month,@performance_dt) > 6 SELECT @fYear = datepart(year,@performance_dt) + 1 ELSE SELECT @fYear = datepart(year,@performance_dt)
    
    IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorExhibitHalls = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Exhibit Halls'
    SELECT @bkColorExhibitHalls = isnull(@bkColorExhibitHalls, @bkColorDefault)

    SELECT @bkColorMembership = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Membership'
    SELECT @bkColorMembership = isnull(@bkColorMembership, @bkColorDefault)

    SELECT @bkColorDonation = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Donation'
    SELECT @bkColorDonation = isnull(@bkColorDonation, @bkColorDefault)

    SELECT @bkColorCityPass = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color City Pass'
    SELECT @bkColorCityPass = isnull(@bkColorCityPass, @bkColorDefault)

    SELECT @bkColorAllPerf = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color All Perf'
    SELECT @bkColorAllPerf = isnull(@bkColorAllPerf, @bkColorDefault)

    SELECT @bkColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Error'
    SELECT @bkColorError = isnull(@bkColorError, @bkColorDefault)
        
    SELECT @foColorFuture = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Future'
    SELECT @foColorFuture = isnull(@foColorFuture, 0)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)

    SELECT @foColorError = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Error'
    SELECT @foColorError = isnull(@foColorError, @foColorToday)

    IF @is_today = 'Y' SELECT @foColor = @foColorToday ELSE SELECT @foColor = @foColorFuture
    IF @foColor is null SELECT @foColor = 0
    
    /*  Button Colors: Each Venue has its own button color, stored in the content under the title records in Tessitura.
                       If a back color cannot be found, the default back color is used  */

    SELECT @bkColorOmni = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Mugar Omni Theater'
    SELECT @bkColorOmni = Isnull(@bkColorOmni,-1)
    IF @bkColorOmni <= 0 SELECT @bkColorOmni = @bkColorDefault
        
    SELECT @bkColorPlanet = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Hayden Planetarium'
    SELECT @bkColorPlanet = Isnull(@bkColorPlanet,-1)
    IF @bkColorPlanet <= 0 SELECT @bkColorPlanet = @bkColorDefault

    SELECT @bkColor4D = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = '4-D Theater'
    SELECT @bkColor4D = Isnull(@bkColor4D,-1)
    IF @bkColor4D <= 0 SELECT @bkColor4D = @bkColorDefault

    SELECT @bkColorButterfly = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Butterfly Garden'
    SELECT @bkColorButterfly = Isnull(@bkColorButterfly,-1)
    IF @bkColorButterfly <= 0 SELECT @bkColorButterfly = @bkColorDefault

    SELECT @bkColorThrill = [quick_sale_button_color] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Thrill Ride 360'
    SELECT @bkColorThrill = Isnull(@bkColorThrill,-1)
    IF @bkColorThrill <= 0 SELECT @bkColorThrill = @bkColorDefault
            
    /*  Button Types: Performance and Navigation buttons are created by this procedure.  The ID numbers for both are needed  */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = isnull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = isnull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    /*  *********************************************************************************************************************************** */  
        
    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */

    IF @is_today = 'Y' SELECT @layout_name = 'Box Office Launch' ELSE SELECT @layout_name = 'tomorrow_launch'

    SELECT @launch_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
    SELECT @launch_layout_no = isnull(@launch_layout_no, 0)
    IF @launch_layout_no <= 0 BEGIN
        SELECT @return_message = 'unable to find the box office launch layout'
        GOTO DONE
    END

    IF @display_results = 'Y' PRINT @layout_name + ' (' + convert(varchar(10),@launch_layout_no) + ')'

    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no

    /*  *********************************************************************************************************************************** */  
       
    /* Exhibit Hall Button */

    /* Need the Exhibit Hall Production Season  */
        
    SELECT @exh_season_name = 'Exhibit Halls FY' + convert(varchar(4),@fYear)
    SELECT @exh_season_no = [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'S' and [description] = @exh_season_name
    SELECT @exh_season_no = isnull(@exh_season_no, 0)
    IF @exh_season_no > 0 BEGIN

        /*  Need the Season number that corresponds to the Exhibit Hall Production Season  */

        SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @exh_season_no
        SELECT @season_no = IsNull(@season_no, 0)
        IF @season_no > 0 BEGIN
        
            /*  There should only be one performance for Public Exhibit Halls on any given day.  The performance number is retrieved by looking for a record in the
                Exhibit Halls Production Season for the date being processed.  If there does happen to be more than one, it will take the last record created (highest performance number)  */

            SELECT @exh_perf_no = max([perf_no]) FROM [dbo].[T_PERF] WHERE convert(char(10),[perf_dt],111) = convert(char(10),@performance_dt,111) and [prod_season_no] = @exh_season_no
            SELECT @exh_perf_no = isnull(@exh_perf_no, 0)

       END
    END
        
    /*  Set values specific to Exhibit Hall Button  */

    SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) 
                     + ' ' + CASE WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                                  ELSE convert(char(2),datePart(day,@performance_dt)) END
    SELECT @curY = 2, @curX = 1

    /*  Insert Exhibit Hall Button into the layout  */

    BEGIN TRY
    
        IF @exh_perf_no <= 0 BEGIN
    
            SELECT @Caption1 = 'EXHIBIT HALLS', @caption4 = ''
            SELECT @Caption2 = 'NOT FOUND'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorError, @foColorError, null, @launch_layout_no, 
                    null, null, null, null, 0, null, null, null, null, null, null, null)

        END ELSE BEGIN

            IF @is_today = 'Y' SELECT @Caption1 = 'TODAY' ELSE SELECT @Caption1 = 'TOMORROW'
            SELECT @caption4 = '[Avail]'
            SELECT @Caption2 = 'Public Exhibit Hall'

            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorExhibitHalls, @foColor, null, @exh_perf_no, 
                        null, null, null, null, 0, null, null, null, null, @ZoneModeBestAvail, null, @season_no)
       END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the public exhibit halls button for today.'
        GOTO DONE
    
    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - Exhibit Hall Button Done'
    
    /*  *********************************************************************************************************************************** */  

    /* School Navigation Button - Only on Weekdays */

    /*  Set values specific to Today's SCHOOL NAVIGATION Button  */

    SELECT @DayOfWeek = datepart(weekday,@performance_dt)
        
    IF @DayOfWeek not in (1, 7) BEGIN

        SELECT @nav_layout_no = null

        IF @is_today = 'Y' SELECT @layout_name = 'today_school' ELSE SELECT @layout_name = 'tomorrow_school'

        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
                  
        IF @is_today = 'Y' SELECT @Caption1 = 'TODAY' ELSE SELECT @Caption1 = 'TOMORROW'
        SELECT @caption4 = ''
        SELECT @Caption2 = 'SCHOOL PRODUCTS'
        SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) + ' ' 
                         + CASE WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                                ELSE convert(char(2),datePart(day,@performance_dt)) END

        SELECT @curY = 2, @curX = 2

        /*  Insert School Navgation into the layout  */

        BEGIN TRY
        
            IF @nav_layout_no > 0 BEGIN

                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorExhibitHalls, @foColor, null, @nav_layout_no, 
                            null, null, null, null, 0, null, null, null, null, null, null, null)
            
            END
        END TRY
        BEGIN CATCH
            
            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the school product button for today.'
            GOTO DONE
            
        END CATCH
        
    END

    IF @display_results = 'Y' PRINT @performance_date +  ' - School Products Button Done'

    /*  *********************************************************************************************************************************** */  
    
    /* All Performances */

    /*  Set values specific to Today's All Performances Button  */    

    SELECT @nav_layout_no = null

    IF @is_today = 'Y' SELECT @layout_name = 'today_all_1' ELSE SELECT @layout_name = 'tomorrow_all_1'

    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = @layout_name
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)

    IF @is_today = 'Y' SELECT @Caption1 = 'TODAY' ELSE SELECT @Caption1 = 'TOMORROW'
    SELECT @Caption2 = 'All Performances'

    SELECT @curY = 2, @curX = 3

    /*  IF navigation layout id is found, Insert Navigation button for All Today's Performances the layout  */
 
    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN
        
            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorAllPerf, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, null, null, Null)
        
        END
    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the All Performances button for today.'
        GOTO DONE

    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - All Performances Button Done'
     
    /*  *********************************************************************************************************************************** */  

    /* Membership Button (today only) */
    
    IF @is_today = 'Y' BEGIN

        /* Need the Membership Production Season  */

        SELECT @mem_season_name = 'Household FY' + convert(varchar(4),@fYear)
        SELECT @mem_season_no = [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'S' and [description] = @mem_season_name
        SELECT @mem_season_no = isnull(@mem_season_no, 0)
        IF @mem_season_no > 0 BEGIN
        
            /*  Need the Season number that corresponds to the Membership Production Season  This should always be the same as the Exhibit Hall Season, but check again anyway  */

            SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @mem_season_no
            SELECT @season_no = IsNull(@season_no, 0)
            IF @season_no > 0 BEGIN

            SELECT @mem_abbreviation = [production_name_abbreviated] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [production_name] = 'household'
            SELECT @mem_abbreviation = IsNull(@mem_abbreviation,'')
                        
            /*  There should only be one membership performance in any given month with a performance date of the last day of the month.  The membership performance number is retrieved by
                looking for the performance where the month of the performance date is the same as the month of the date being processed. If there happenes to be more than one, the 
                most recent record is selected (the highest performance number).        */

                SELECT @mem_perf_no = max([perf_no]) FROM [dbo].[T_PERF] 
                WHERE datepart(month,[perf_dt]) = datepart(month,@performance_dt) and [prod_season_no] = @mem_season_no and [perf_code] like '%' + @mem_abbreviation + '%'
                SELECT @mem_perf_no = isnull(@mem_perf_no, 0)
            
            END
        END

        /*  Set values specific to the Membership Button  */

        SELECT @Caption3 = datename(month,@performance_dt)

        SELECT @curY = 2, @curX = 5

        /*  Insert Membership Button into the layout  */

        BEGIN TRY

    
            IF @mem_perf_no <= 0 BEGIN
    
                SELECT @Caption1 = 'MEMBERSHIPS', @caption4 = ''
                SELECT @Caption2 = 'NOT FOUND'

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorError, @foColorError, null, @launch_layout_no, 
                        null, null, null, null, 0, null, null, null, null, null, null, null)

            END ELSE BEGIN
        
                SELECT @Caption1 = 'MEMBERSHIPS', @Caption2 = '', @Caption4 = '[Avail]'
    
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorMembership, @foColor, null, @mem_perf_no, 
                        null, null, null, null, 0, null, null, null, null, @ZoneModePrompt, null, @season_no)
            END
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the membership sales button.'
            GOTO DONE
    
        END CATCH

        IF @display_results = 'Y' PRINT @performance_date +  ' - Membership Button Done'
        
    END

    /*  *********************************************************************************************************************************** */  
    
    /*  Venue Navigation Buttons */
    
    IF @is_today = 'Y' SELECT @caption1 = 'TODAY' ELSE SELECT @Caption1 = 'TOMORROW'
    SELECT @caption4 = ''
    SELECT @Caption3 = left(datename(weekday,@performance_dt),3) + ' ' + left(datename(month,@performance_dt),3) + ' ' 
                     + CASE WHEN datepart(day,@performance_dt) < 10 THEN '0' + convert(char(1),datepart(day,@performance_dt)) 
                            ELSE convert(char(2),datePart(day,@performance_dt)) END

    /*  TODAY - Omni Theater  */

    /*  Set values specific to Today's Omni Button  */

    SELECT @nav_layout_no = null
        
    IF @is_today = 'Y' SELECT @layout_name = 'today_omni' ELSE SELECT @layout_name = 'tomorrow_omni'
    SELECT @Caption2 = 'Omni Theater'

    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = @layout_name
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
           
    SELECT @curY = 3, @curX = 1

    /*  IF navigation layout id is found, Insert Navigation button for Today's Omni Shows into the layout  */
 
    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN
        
            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorOmni, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Omni button for today.'
        GOTO DONE

    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - Omni Theater Button Done'

    /* Planetarium */
    
    /*  Set values specific to Today's Planetarium Button  */

    SELECT @nav_layout_no = null

    IF @is_today = 'Y' SELECT @layout_name = 'today_planet' ELSE SELECT @layout_name = 'tomorrow_planet'
    SELECT @Caption2 = 'Planetarium'

    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
           
    SELECT @curY = 3, @curX = 2

    /*  IF navigation layout id is found, Insert Navigation button for Today's Planetarium Shows into the layout  */
 
    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN
        
            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorplanet, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
            END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Planetarium button for today.'
        GOTO DONE

    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - Planetarium Button Done'

    /* 4-D Theater */

    /*  Set values specific to Today's 4D Button  */    

    SELECT @nav_layout_no = null

    IF @is_today = 'Y' SELECT @layout_name = 'today_4d' ELSE SELECT @layout_name = 'tomorrow_4d'
    SELECT @Caption2 = '4-D Theater'
            
    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
        
    SELECT @curY = 3, @curX = 3

    /*  IF navigation layout id is found, Insert Navigation button for Today's 4D Shows into the layout  */
 
    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN

            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor4D, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the 4D-Theater button for today.'
        GOTO DONE

    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - 4-D Theater Button Done'

    /* Butterfly Garden */

    /*  Set values specific to Today's Butterfly Garden Button  */    

    SELECT @nav_layout_no = null

    IF @is_today = 'Y' SELECT @layout_name = 'today_btrfly' ELSE SELECT @layout_name = 'tomorrow_btrfly'
    SELECT @Caption2 = 'Butterfly Garden'

    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)
        
    SELECT @curY = 3, @curX = 4
 
    /*  IF navigation layout id is found, Insert Navigation button for Today's Butterfly Garden Shows into the layout  */

    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN
        
            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorButterfly, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        END

    END TRY
    BEGIN CATCH
        
        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Butterfly Garden button for today.'
        GOTO DONE
        
    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - Butterfly Garden Button Done'

    
    IF @is_today = 'Y' BEGIN
        
        /* TODAY - Thrill Ride */

        /*  Set values specific to Today's Thrill Ride Button  */    

        SELECT @nav_layout_no = null
        
        SELECT @Caption2 = 'Thrill Ride 360'
                        
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'Thrill Ride A Launch'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)

        SELECT @curY = 3, @curX = 5

        /*  IF navigation layout id is found, Insert Navigation button for Today's Omni Shows into the layout  */
 
        BEGIN TRY

            IF @nav_layout_no > 0 BEGIN
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorThrill, @foColor, null, @nav_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)
            END

        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Thrill Ride button for today.'
            GOTO DONE
    
        END CATCH

        IF @display_results = 'Y' PRINT @performance_date +  ' - Thrill Ride Button Done'

    END

    /* Navigation Button to Speical Events buttons */
    
    SELECT @special_event_count = count(*) FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_date >= @performance_date and title_name in ('Adult Offerings', 'Events')
    SELECT @special_event_count = IsNull(@special_event_count, 0)

    IF @special_event_count > 0 BEGIN

        SELECT @nav_layout_no = null
       
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'special_events'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)

        SELECT @Caption1 = 'MOVE TO', @Caption2 = 'SPECIAL EVENTS', @Caption3 = '', @Caption4 = ''

        SELECT @curY = 6, @curX = 4

        /*  IF navigation layout id is found, Insert Navigation button for Today's Omni Shows into the layout  */
 
        BEGIN TRY

            IF @nav_layout_no > 0 BEGIN
        
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @caption4, @bkColorDefault, @foColor, null, @nav_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)

            END

        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Thrill Ride button for today.'
            GOTO DONE

        END CATCH

        IF @display_results = 'Y' PRINT @performance_date +  ' - Special Events Button Done'
    
    END

    IF @is_today = 'Y' BEGIN

        /* Navigation Button to Tomorrow's buttons */

        SELECT @nav_layout_no = null
            
        SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'tomorrow_launch'
        SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)

        SELECT @Caption1 = 'MOVE TO', @Caption2 = 'TOMORROW', @caption3 = '', @Caption4 = ''

        SELECT @curY = 6, @curX = 5

        /*  IF navigation layout id is found, Insert Navigation button for Today's Omni Shows into the layout  */
 
        BEGIN TRY

            IF @nav_layout_no > 0 BEGIN

                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDefault, @foColorFuture, null, @nav_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)
                END

        END TRY
        BEGIN CATCH
            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Thrill Ride button for today.'
            GOTO DONE
        END CATCH
    
        IF @display_results = 'Y' PRINT @performance_date +  ' - Go To Tomorrow Button Done'

    END

    /* Navigation Button to Contribution Buttons */

    SELECT @nav_layout_no = null
            
    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'pre_defined_contributions_box'
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)

    SELECT @Caption1 = 'CONTRIBUTIONS', @Caption2 = ''

    SELECT @curY = 2, @curX = 4

    /*  IF navigation layout id is found, Insert Navigation button for Today's Omni Shows into the layout  */
 
    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN

                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDonation, @foColor, null, @nav_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)
            END
    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Thrill Ride button for today.'
        GOTO DONE

    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - Contributions Button Done'

    /* Navigation Button to Voucher Sales */

    SELECT @nav_layout_no = null
            
    SELECT @nav_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description]  = 'voucher_sales'
    SELECT @nav_layout_no = IsNull(@nav_layout_no, 0)

    SELECT @Caption1 = 'MOVE TO', @Caption2 = 'VOUCHER SALES', @caption3 = ''

    SELECT @curY = 6, @curX = 3

    /*  IF navigation layout id is found, Insert Navigation button for Today's Omni Shows into the layout  */
 
    BEGIN TRY

        IF @nav_layout_no > 0 BEGIN
        
            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @curY and xpos = @curX)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@launch_layout_no, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDefault, @foColor, null, @nav_layout_no, 
                        null, null, null, null, 0, null, null, null, null, Null, null, Null)
        END

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Voucher Sales button for today.'
        GOTO DONE

    END CATCH

    IF @display_results = 'Y' PRINT @performance_date +  ' - Voucher Button Done'
    
            /**********************************************************/
            /*  Added 1/29/2019 - Cash Drop Button on the main page  */
            /**********************************************************/
    
            BEGIN TRY
    
                SELECT @bType = ISNULL([id],0) 
                       FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'

                SELECT @perf_no = ISNULL(performance_no,0), 
                                  @perf_zone_no = ISNULL(performance_zone,0)
                       FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE 
                       WHERE performance_date = CONVERT(VARCHAR(10),GETDATE(),111) AND title_name = 'Cash Drop'
    
                SELECT @drop_season_name = 'Cash Drop FY' + convert(varchar(4),@fYear)
                SELECT @drop_season_no = ISNULL([inv_no],0) 
                       FROM [dbo].[T_INVENTORY] 
                       WHERE [type] = 'S' and [description] = @drop_season_name

                IF @drop_season_no = 0
                    SELECT @season_no = 0
                ELSE
                    SELECT @season_no = ISNULL([season],0) 
                           FROM [dbo].[T_PROD_SEASON] 
                           WHERE [prod_season_no] = @drop_season_no
        
                IF @bType > 0 AND @perf_no > 0 AND @perf_zone_no > 0 AND @season_no > 0 BEGIN
            
                    SELECT @Caption1 = 'TODAY'
                    SELECT @Caption2 = 'CASH DROP'
                    SELECT @Caption3 = ''
                    SELECT @Caption4 = ''

                    --Hard Coded to the bottom left corner of the screen
                    SELECT @curX = 2, @curY = 6

                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@launch_layout_no, @bType, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDrop, @foColortoday, null, @Perf_no, null, @perf_zone_no, 
                            null, null, 0, null, null, null, null, 4, null, @season_no)

                END

            END TRY
            BEGIN CATCH

                SELECT @return_message = left(Error_message(),100)
                IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the Cash Drop button for today.'
                GOTO DONE

            END CATCH

            IF @display_results = 'Y' PRINT @performance_date +  ' - Cash Drop Button Done'


    IF @errCount > 0 SELECT @return_message = 'the procedure has finished but with errors'
    ELSE IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_box_office_launch] TO impusers
GO

