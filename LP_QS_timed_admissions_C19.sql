USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_timed_admissions_C19]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_timed_admissions_C19] AS' 
END
GO

EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_timed_admissions_C19] TO impusers, tessitura_app' 
GO

ALTER PROCEDURE [dbo].[LP_QS_timed_admissions_C19]
        @performance_dt DATETIME = NULL,
        @title_no INT = NULL,
        @layout_no INT = NULL,
        @button_color INT = NULL,
        @buttons_created INT = 0 OUTPUT,
        @return_message VARCHAR(100) = '' OUTPUT
AS BEGIN

        DECLARE @bkColorDefault INT = ISNULL((SELECT [default_value] 
                                              FROM [dbo].[T_DEFAULTS] 
                                              WHERE parent_table = 'Museum of Science' 
                                                AND [field_name] =  'QS Button Color Default'), 14085615);

        DECLARE @color_white INT = 16777215;
        DECLARE @color_black INT = 0;

        DECLARE @yMax INT = ISNULL((SELECT [rows] 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                   WHERE [id] = @layout_no), 0);

        DECLARE @xMax INT = ISNULL((SELECT [columns]
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [id] = @layout_no), 0);

        DECLARE @btnTypePerf INT = ISNULL((SELECT [id]
                                          FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE]
                                          WHERE [description] = 'Performance'), 0);

        DECLARE @btnTypeNav INT = ISNULL((SELECT [id]
                                          FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE]
                                          WHERE [description] = 'Navigator'), 0);

        DECLARE @yPOS INT = 1, @xPOS INT = 1, @for_color AS INT = 0;
        DECLARE @perf_no INT = 0, @zone_no INT = 0, @season_no INT = 0;
        DECLARE @production_no INT = 0, @production_color INT = @button_color

        DECLARE @caption1 VARCHAR(30) = '', @caption2 VARCHAR(30) = '', @caption3 VARCHAR(30) = '', @caption4 VARCHAR(30) = '';
        DECLARE @production_name VARCHAR(30) = '', @production_name_short VARCHAR(30) = '';
        
        IF ISNULL(@button_color, 0) = 0 SELECT @button_color = @bkColorDefault;

        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no;

        SELECT @buttons_created = 0
        
        DECLARE button_cursor INSENSITIVE CURSOR FOR
        SELECT p.[production_no],
               UPPER(p.[production_name]),
               UPPER(p.[production_name_short]),
               CASE WHEN CAST(GETDATE() AS DATE) = @performance_dt THEN 'TODAY'
                    WHEN CAST(DATEADD(DAY,1,GETDATE()) AS DATE) = @performance_dt THEN 'TOMORROW'
                    ELSE FORMAT(p.[performance_dt],'ddd MMM dd, yyyy') END,
               p.[performance_time_display],
               '[avail]',
               p.[performance_no],
               p.[performance_zone],
               p.[season_no]
        FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS p
        WHERE CAST(p.[performance_dt] AS DATE) = @performance_dt
          AND p.[title_no] = @title_no
          AND ISDATE(p.[performance_time_display]) = 1
        ORDER BY p.[performance_zone_text]
        OPEN [button_cursor]
        BEGIN_BUTTON_LOOP:

            FETCH NEXT FROM [button_cursor] INTO @production_no, @production_name, @production_name_short, @caption1, @caption3, @caption4, @perf_no, @zone_no, @season_no
            IF @@FETCH_STATUS = -1 GOTO END_BUTTON_LOOP

            IF LEN(@production_name) > 18 SELECT @caption2 = UPPER(@production_name_short)
            ELSE SELECT @caption2 = UPPER(@production_name)

            SELECT @production_color = TRY_CONVERT(INT,[value])
                                       FROM [dbo].[TX_INV_CONTENT] 
                                       WHERE [inv_no] = @production_no 
                                         AND [content_type] = 21      --21 = Quick Sale Button Color
                      
            SELECT @production_color = ISNULL(@production_color, @button_color)
            
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                                       [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                                       [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
            
            VALUES (@layout_no, @btnTypePerf, @yPOS, @xPOS, @caption1, @caption2, @caption3, @caption4, @production_color, @for_color, NULL, 
                    @perf_no, NULL, @zone_no, NULL, NULL, 0, NULL, NULL, NULL, NULL, 4, NULL, @season_no)

            SELECT @xPOS += 1
            
            IF @xPOS > @xMax SELECT @yPOS += 1, @xPOS = 1

            SELECT @buttons_created += 1

            IF @yPOS > @yMax GOTO END_BUTTON_LOOP

            GOTO BEGIN_BUTTON_LOOP

        END_BUTTON_LOOP:
        CLOSE [button_cursor]
        DEALLOCATE [button_cursor]

        IF @return_message = '' SELECT @return_message = 'success'

        DONE:

            IF @return_message = '' SELECT @return_message = 'error'

END
GO


--EXECUTE [dbo].[LP_QS_timed_admissions_C19] @performance_dt = '7-26-2020', @title_no = 1398, @layout_no = 193, @button_color =0, @buttons_created = 0, @return_message = ''
--EXECUTE [dbo].[LP_QS_timed_admissions_C19] @performance_dt = '7-27-2020', @title_no = 1398, @layout_no = 193, @button_color =0, @buttons_created = 0, @return_message = ''

--SELECT * FROM [dbo].[T_INVENTORY] WHERE [description] = 'Live Presentations'
--SELECT * FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'future_live_06'
--SELECT ISNULL([default_value], 0) FROM [dbo].[T_DEFAULTS] WHERE [parent_table] = 'Museum of Science' AND [field_name] =  'QS Button Color Live Present'

--SELECT   @performance_dt, @title_no, @layout_no, @button_color, @production_color, @production_no, @production_name, @production_name_short, @caption1, @caption3, @caption4, @perf_no, @zone_no, @season_no
