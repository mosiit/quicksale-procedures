USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_single_venue]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_single_venue]
GO

/*
    Local Stored Procedure: LP_QS_single_venue

*/

CREATE PROCEDURE [dbo].[LP_QS_single_venue]
        @performance_dt datetime = Null,
        @title_no int,
        @layout_no int = Null,
        @sort_type varchar(10) = 'Time',  --Time or Production
        @remove_past_performances char(1) = 'N',
        @is_today char(1) = Null,
        @is_tomorrow char(1) = Null,
        @return_message varchar(100) OUTPUT
AS BEGIN

    /* Procedure Variables */

    DECLARE @bkColorDefault int, @foColorToday int, @foColorFuture int, @foColor int
    DECLARE @bkColor int, @bTypePerf int, @perf_no int, @perf_zone_no int, @season_no int
    DECLARE @xMax int, @yMax int, @curX int, @curY int, @format_Days int
    DECLARE @perf_date char(10), @perf_time char(8), @performance_date char(10)
    DECLARE @layout_name varchar(30), @venue_name varchar(20), @perf_title varchar(30), @QStitle varchar(20)
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @Caption4 varchar(30)
    DECLARE @cutoff_time char(8)
   
    /*  If Null is passed to the Performance Date parameter, default to today's date
        If Null is passed to the Title number parameter, default to 0 (All titles)
        IF anything other then 'Title' is passed to the Sort Type parameter, default to 'Time'      */
    
    SELECT @performance_dt = IsNull(@performance_dt,getdate())
    SELECT @performance_date = convert(char(10),@performance_dt,111)
    SELECT @layout_no = IsNull(@layout_no, 0)
    SELECT @sort_type = IsNull(@sort_type, '') 
    IF @sort_type <> 'Production' SELECT @sort_type = 'Time'
    SELECT @remove_past_performances = IsNull(@remove_past_performances, 'N')
    IF @remove_past_performances <> 'Y' SELECT @remove_past_performances = 'N'

    IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END

    /*  @ valid title no is needed and that title must exist with a quick sale venue value in order for this procedure to work  */
    
    IF @title_no is null SELECT @title_no = 0
    IF @title_no <= 0 or not exists (SELECT * FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_no] = @title_no and [quick_sale_venue] <> '') BEGIN
        SELECT @return_message = 'Invalid title_no (' + convert(varchar(10),@title_no) + ').'
        GOTO DONE
    END

    /*  A valid layout is needed.  If the layout number is not greater than zero, error out and stop the process.  */
    If @layout_no is null SELECT @layout_no = 0
    IF @layout_no <= 0 BEGIN

        SELECT @QStitle = lower([quick_sale_venue]) FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [title_no] = @title_no
        SELECT @QStitle = IsNull(@QStitle,'')

        SELECT @format_Days = Datediff(day,convert(char(10),getdate(),111),@performance_date) 
        IF @format_days < 0 SELECT @format_days = 0

        IF @is_today = 'Y' SELECT @layout_name = 'today_' + ltrim(@QStitle)
        ELSE IF @is_tomorrow = 'Y' SELECT @layout_name = 'tomorrow_' + ltrim(@QStitle)
        ELSE SELECT @layout_name = 'future_' + ltrim(@QStitle) + '_' + CASE WHEN (@format_days - 1) < 10 THEN '0' + convert(char(1),(@format_days - 1)) ELSE convert(char(2),(@format_days - 1)) END

        SELECT @layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name
        SELECT @layout_no = IsNull(@layout_no, 0)

    END
    
    IF @layout_no <= 0 BEGIN
        SELECT @return_message = 'invalid layout'
        GOTO DONE
    END

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)

    SELECT @foColorFuture = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Future'
    SELECT @foColorFuture = isnull(@foColorFuture, 0)
    
    IF convert(char(10),@performance_dt,111) = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    SELECT @cutoff_time = convert(char(8),dateadd(minute,-10,getdate()),108)
    
    /*  Caption 1, 2, and 3 are created by the procedure, but Caption 4 is always the [Avail] token to display number of tickets left */

    SELECT @Caption1 = '', @Caption2 = '', @Caption3 = '', @Caption4 = '[Avail]'
    SELECT @bkColor = @bkColorDefault, @foColor = @foColorToday
    SELECT @return_message = ''

    /*  Set the performance date to a string (yyyy/mm/dd) and determine if the procedure is working with the current date.
        There are different text colors for today vs future day      */

    SELECT @perf_date = convert(char(10),@performance_dt,111)
    IF @perf_date > convert(char(10),getdate(),111) SELECT @foColor = @foColorFuture
    
    /*  The dimensions of the layout are needed to determine the number of buttons on the screen. If the dimensions cannot be determines, the procedure cannot go on  */

    SELECT @layout_name = [description],  @xMax = [columns], @yMax = [rows] FROM [dbo].[T_SALES_LAYOUT] WHERE [id] = @layout_no
    SELECT @layout_name = IsNull(@layout_name, ''), @xMax = IsNull(@xMax, 0), @yMax = IsNull(@yMax,0)
    IF @XMax <= 0 or @yMax <= 0 BEGIN
        SELECT @return_message = 'Unable to determine the dimensions of the layout (' + @layout_name + ').'
        GOTO DONE
    END

    /*  The button type is a necessary value.  This procedure deals with performance buttons, so the id number for that type is needed.
        If it cannot be determines, the procedure cannot go on.   */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = IsNull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    /*  Clear the layout of all performance buttons before continuing.  
        If there are navigation or other types of buttons, Those will be left alone.    */

    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [button_type_id] = @bTypePerf
    
    SELECT @curX = 1, @curY = 1
    
    /*  The cursor is generated in one of two different ways depening on whats passed to the procedure through the parameters.
            1. Specific venue sorted by title, then time        2. Specific Venue sorted by time, then title        */

    DECLARE ButtonCursor INSENSITIVE CURSOR FOR
    SELECT [performance_no], [performance_zone], [performance_time_display], [Production_name_short], [quick_sale_venue], [season_no], [quick_sale_button_color] 
    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE title_no = @title_no and performance_date = @perf_date  and [performance_type_name] <> 'School Only'
    ORDER BY CASE WHEN @sort_type = 'Production' THEN [production_name_short] ELSE [performance_time] END, CASE WHEN @sort_type = 'Production' THEN [performance_time] ELSE [production_name_short]END
    OPEN ButtonCursor
    BEGIN_BUTTON_LOOP:

        /*  Records are processed one at a time by retrieving them from the cursor  */

        FETCH NEXT FROM ButtonCursor INTO @perf_no, @perf_zone_no, @perf_time, @perf_title, @venue_name, @season_no, @bkcolor
        IF @@FETCH_STATUS = -1 GOTO END_BUTTON_LOOP

        IF @is_today = 'Y' and @remove_past_performances = 'Y' and convert(char(8),convert(datetime,@perf_time),108) < @cutoff_time GOTO BEGIN_BUTTON_LOOP

        /*  If no button color is provided, use the default button color   */
        SELECT @bkColor = IsNull(@bkColor, 0)
        IF @bkcolor <= 0 SELECT @bkcolor = @bkColorDefault

        /*  Caption 1 = The Quick Sale Venue Name (from content on the title record) plus the date of the performance (mm/dd/yy)
            Caption 2 = the short title of the production
            caption 3 = the time of the performance     */

        SELECT @Caption1 = upper(@venue_name) + ' ' + upper(left(datename(weekday,@perf_date),2)) + ' '+ right(@perf_date,5)
        SELECT @Caption2 = @perf_title
        SELECT @Caption3 =  @perf_time

        /*  Determine the next button position based on the last position, moving one at a time, across (X) then down (y)
            If a button already exists at a position, that position is skipped.
            If there are more performances than available buttons on the layout, the procedure adds as many as it can, then stops.      */

        WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and [ypos] = @curY and [xpos] = @curX) BEGIN
                 IF @cury > @yMax GOTO END_BUTTON_LOOP
            ELSE IF @curX = @xMax SELECT @curY = (@curY + 1), @curX = 1
            ELSE SELECT @curX = (@curX + 1)
        END

        /*  If the procedure has moved passed the end of the layout, skip the insert and go to the end of the loop   */

        IF @curY > @yMax GOTO END_BUTTON_LOOP

        BEGIN TRY

            /*  Insert the new record into the T_SALES_LAYOUT_BUTTONS table  */

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_no, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor, @foColor, null, @Perf_no, null, @perf_zone_no, 
                    null, null, 0, null, null, null, null, 4, null, @season_no)

        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while inserting button on ' + @layout_name + '.'
            GOTO DONE


        END CATCH
        GOTO BEGIN_BUTTON_LOOP

    END_BUTTON_LOOP:
    CLOSE ButtonCursor
    DEALLOCATE ButtonCursor

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_single_venue] TO impusers
GO

 

 