USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_special_exhibit_future_performances]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_special_exhibit_future_performances] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_special_exhibit_future_performances] TO impusers' 
END
GO

ALTER PROCEDURE [dbo].[LP_QS_special_exhibit_future_performances]
        @exhibit_name VARCHAR(50) = '',
        @return_message VARCHAR(255) = '' OUTPUT
AS BEGIN

    /*  Procedure Variables and Tables  */

        DECLARE @start_dt DATETIME = GETDATE();
        DECLARE @fiscal_year INT = 0, @voucher_prod_season_name VARCHAR(30) = '', @prf_description VARCHAR(30) = '';
        
        --parent_x_max and parent_y_max are hard coded but are also double-checked later
        DECLARE @parent_layout_name VARCHAR(30) = '', @parent_layout_id INT = 0;
        DECLARE @return_layout_id INT = 0
        --DECLARE @parent_x_pos INT = 1, @parent_y_pos INT = 1, @parent_x_max INT = 5, @parent_y_max INT = 5;

        --parent_x_max and parent_y_max are hard coded and are used when creating new layouts
        DECLARE @layout_name VARCHAR(30) = '', @layout_no INT = 2, @layout_id INT = 0;
        DECLARE @x_pos INT = 1, @y_pos INT = 1, @x_max INT = 5, @y_max INT = 15;

        --Constant values used when inserting new values
        DECLARE @prf_button_type INT = 1;    --FROM TR_SALES_LAYOUT_BUTTON_TYPE
        DECLARE @pkg_button_type INT = 9;    --FROM TR_SALES_LAYOUT_BUTTON_TYPE
        DECLARE @nav_button_type INT = 30;   --FROM TR_SALES_LAYOUT_BUTTON_TYPE
        DECLARE @color_white INT = 16777215;
        DECLARE @color_black INT = 0;
        DECLARE @btn_bg_color INT = 11202814;
        DECLARE @btn_bg_color_alternate INT = 11261438;
        DECLARE @btn_fg_color INT = 64;
        DECLARE @special_exhibit_title_no INT = 37;
        DECLARE @voucher_title_no INT = 5541;
        DECLARE @zone_mode INT = 4;          --4 = Specific
        DECLARE @exh_voucher_zone INT = 359;

        --Variables to hold the cursor values and for creating the actual buttons.
        --DECLARE @pkg_no INT = 0, @pkg_description VARCHAR(30) = '', @pkg_season INT, @pkg_dt DATETIME, @prev_pkg_dt DATETIME;
        DECLARE @prf_no INT = 0, @prf_zone INT = 0, @prf_dt DATETIME, @prf_time VARCHAR(10)= '', @prf_season INT = 0;
        DECLARE @btn_clr INT = 11202814
        DECLARE @end_dt DATETIME = NULL
        DECLARE @day_num INT = 0
        DECLARE @days_out INT = 0
                
        --Putting everything into a temp table then moving it all into impresario at once
        --This is an exact copy of T_SALES_LAYOUT_BUTTON                
        IF OBJECT_ID('tempdb..#fut_buttons') IS NOT NULL DROP TABLE [#fut_buttons]
    
        CREATE TABLE [#fut_buttons] ([id] INT NOT NULL DEFAULT (0),                             [layout_id] INT NOT NULL DEFAULT (0),
	                                 [button_type_id] INT NOT NULL  DEFAULT (0),                [ypos] INT NOT NULL DEFAULT ((0)),
	                                 [xpos] INT NOT NULL DEFAULT ((0)),                         [caption1] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption2] VARCHAR(30) NULL DEFAULT (''),                  [caption3] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption4] VARCHAR(30) NULL DEFAULT (''),                  [background_color] INT NOT NULL DEFAULT (0),
	                                 [foreground_color] INT NOT NULL DEFAULT (0),               [itemamount] MONEY NULL DEFAULT (0.0),
	                                 [itemid] INT NULL DEFAULT (0),                             [itemotherid] INT NULL DEFAULT (0),
	                                 [itemsubid] INT NULL DEFAULT (0),                          [create_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
	                                 [created_by] VARCHAR(8) NOT NULL DEFAULT (''),             [create_loc] VARCHAR(16) NOT NULL DEFAULT (''),
	                                 [last_update_dt] DATETIME NOT NULL DEFAULT (GETDATE()),    [last_updated_by] VARCHAR(8) NOT NULL DEFAULT (''),
	                                 [product_start_mode] INT NULL DEFAULT (0),                 [product_start_dt] DATETIME NULL,
	                                 [product_start_day_add] INT NULL DEFAULT (0),              [product_sequence] INT NULL DEFAULT (0),
	                                 [product_end_mode] INT NULL DEFAULT (0),                   [product_end_day_add] INT NULL DEFAULT (0),
	                                 [product_end_dt] DATETIME NULL,                            [zone_mode] INT NULL DEFAULT (0),
	                                 [zone_sequence] INT NULL DEFAULT (0),                      [itemparentid] INT NULL DEFAULT (0))

    /*  Check Parameters  */

        --Start Date for this procedure is always today
        SELECT @start_dt = GETDATE()
        
        --Remove any time information from the start date
        SELECT @start_dt = CAST(@start_dt AS DATE);

        --End Date is today + 11
        SELECT @end_dt = DATEADD(DAY, 11, GETDATE())

        --Initialize if necessary
        SELECT @Exhibit_name = ISNULL(@Exhibit_name,'');

        SELECT @return_message = '';

      
    /*  Process one day at a time  */  
        
        WHILE @start_dt <= @end_dt BEGIN

            --RESET VARIABLES
            SELECT @layout_id = NULL, @x_pos = 1, @y_pos = 1

            --Determine number of days between today and the date being processed
            SELECT @day_num = DATEDIFF(DAY, GETDATE(), @start_dt)

            BEGIN TRY
            
                --Determine layout name 
                --Because the dynamic names start at 1 but also start with the second day after today, subtract 1 from @day_nu,
                IF @day_num = 0 SELECT @layout_name = 'today_special_add'
                ELSE IF @day_num = 1 SELECT @layout_name = 'tomorrow_special_add'
                ELSE SELECT @layout_name = 'future_special_add_' + CASE WHEN (@day_num - 1) < 10 THEN '0' ELSE '' END + CAST((@day_num - 1) AS VARCHAR(2))

                --Determine the layout id number for the layout being processed
                SELECT @layout_id = ISNULL([id],0) 
                                    FROM [dbo].[T_SALES_LAYOUT] 
                                    WHERE [description] = @layout_name;

                --If it doesn't exist, create it
                IF ISNULL(@layout_id,0) = 0 BEGIN
                
                    INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                    SELECT @layout_name, 
                           'N', 
                           @y_max, 
                           @x_max, 
                           -1, 
                           'N';

                    SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name;

                END;

                --If layout still can't be found, throw an error (jumps to catch block)
                IF @layout_id <= 0 THROW 500001,'Unable to determine sales layout id.',1;            

            END TRY
            BEGIN CATCH

                --include the layout name with the error message                           
                SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                --Throw the error to the user (this will stop the procedure)
                THROW 500001, @return_message, 1;
               
            END CATCH

        /*  Pull All special exhibitions performances for the day being processed and create individual buttons for each one
            on a separate screen for each day  */

            BEGIN TRY
            
                DECLARE performance_cursor_futureperf INSENSITIVE CURSOR FOR
                SELECT performance_no, 
                       performance_zone, 
                       performance_dt, 
                       performance_time_display, 
                       season_no,
                       DATEDIFF(DAY,GETDATE(),performance_dt)
                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                WHERE DATEDIFF(DAY,performance_dt, @start_dt) = 0
                  AND title_no = @special_exhibit_title_no
                ORDER BY TRY_CONVERT(TIME, performance_time_display);

                OPEN performance_cursor_futureperf;

                FETCH NEXT FROM performance_cursor_futureperf INTO @prf_no, @prf_zone, @prf_dt, @prf_time, @prf_season, @days_out;

                WHILE @@FETCH_STATUS <> -1 BEGIN

                    BEGIN TRY

                        IF @days_out BETWEEN 0 AND 11 SELECT @btn_clr = @btn_bg_color_alternate
                        ELSE SELECT @btn_clr = @btn_bg_color

                       INSERT INTO [#fut_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                   [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                   [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                   [zone_mode], [zone_sequence], [itemparentid])
                       VALUES (0, 
                               @layout_id, 
                               @prf_button_type, 
                               @y_pos, 
                               @x_pos, 
                               @exhibit_name, 
                               FORMAT(@prf_dt,'MM/dd/yyyy'), @prf_time, '[avail]', 
                               @btn_clr, 
                               @btn_fg_color, 
                               NULL, 
                               @prf_no, 
                               NULL, 
                               @prf_zone, 
                               GETDATE(), 
                               dbo.FS_USER(), 
                               [dbo].[FS_LOCATION](), 
                               GETDATE(), 
                               dbo.FS_USER(), 
                               NULL, 
                               NULL, 
                               0, 
                               NULL, 
                               NULL, 
                               NULL, 
                               NULL, 
                               4, 
                               NULL, 
                               @prf_season);
                    
                        SELECT @x_pos += 1;

                        IF @x_pos > @x_max
                            SELECT @y_pos += 1,
                                   @x_pos = 1;

                        FETCH NEXT FROM performance_cursor_futureperf INTO @prf_no, @prf_zone, @prf_dt, @prf_time, @prf_season, @days_out;

                    END TRY
                    BEGIN CATCH

                        --include the layout name with the error message                           
                        SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                        IF cursor_status('global','performance_cursor_futureperf') > 0 CLOSE performance_cursor_futureperf;
                        IF cursor_status('global','performance_cursor_futureperf') > -3 DEALLOCATE performance_cursor_futureperf;
    
                        --Throw the error to the user (this will stop the procedure)
                        THROW 500001, @return_message, 1;

                    END CATCH

                END
                CLOSE performance_cursor_futureperf;
                DEALLOCATE performance_cursor_futureperf;
              

            /*  Add voucher button for the same day on the screen */
              
                BEGIN TRY

                    --Determine the Production Season for the package Exhibit Hall Vouchers
                    SELECT @fiscal_year = dbo.LF_GetFiscalYear(@start_dt)
                    SELECT @voucher_prod_season_name = 'EH Vouchers (w/pkg) FY' + CONVERT(VARCHAR(4),@fiscal_year)

                    SELECT @y_pos += 1
                    SELECT @x_pos = (@x_max - 2)

                    --Get performance information for today's performance
                    SELECT @prf_no = MAX([performance_no]),
                           @prf_description = MAX([production_name]),
                           @prf_season = MAX([season_no]),
                           @prf_dt = MAX([performance_dt])
                    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                    WHERE DATEDIFF(DAY,performance_dt, @start_dt) = 0
                      AND [title_no] = @voucher_title_no 
                      AND [production_season_name] = @voucher_prod_season_name;

                  
                       INSERT INTO [#fut_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                   [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                   [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                    [zone_mode], [zone_sequence], [itemparentid])
                        VALUES (0, 
                                @layout_id, 
                                @prf_button_type, 
                                @y_pos, 
                                @x_pos, 
                                'Exhibit Voucher', 
                                FORMAT(@prf_dt,'MM/dd/yyyy'), 
                                '', 
                                '', 
                                @btn_bg_color, 
                                @btn_fg_color, 
                                NULL, 
                                @prf_no, 
                                NULL, 
                                @exh_voucher_zone, 
                                GETDATE(), 
                                [dbo].[FS_USER](), 
                                [dbo].[FS_LOCATION](), 
                                GETDATE(), 
                                dbo.FS_USER(), 
                                NULL, 
                                NULL, 
                                0, 
                                NULL, 
                                NULL, 
                                NULL, 
                                NULL, 
                                @zone_mode, 
                                NULL, 
                                @prf_season);

                END TRY
                BEGIN CATCH

                    --include the layout name with the error message                           
                    SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                    --Throw the error to the user (this will stop the procedure)
                    THROW 500001, @return_message, 1;

                END CATCH

            END TRY
            BEGIN CATCH
    
                SELECT @return_message = ERROR_MESSAGE();

                --Throw the error to the user (this will stop the procedure)
                THROW 500001, @return_message, 1;

            END CATCH

        /*  Add Audio Tour button for the same day on the screen */
            
            BEGIN TRY    

                SELECT @return_layout_id = id
                FROM [dbo].[T_SALES_LAYOUT]
                WHERE [description] = REPLACE(@layout_name,'_add','_aud')

                SELECT @x_pos -= 1

                IF ISNULL(@return_layout_id,0) > 0
                        INSERT INTO [#fut_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                    [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                    [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                    [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                    [zone_mode], [zone_sequence], [itemparentid])
                        VALUES (0,
                                @layout_id,
                                @nav_button_type,
                                @y_pos,
                                @x_pos,
                                'Audio Tour', 
                                FORMAT(@start_dt,'MM/dd/yyyy'), 
                                '', 
                                '', 
                                @btn_bg_color, 
                                @btn_fg_color,  
                                NULL,
                                @return_layout_id,
                                NULL, 
                                NULL, 
                                GETDATE(), 
                                dbo.FS_USER(),
                                [dbo].[FS_LOCATION](), 
                                GETDATE(), 
                                dbo.FS_USER(), 
                                NULL, 
                                NULL, 
                                0, 
                                NULL, 
                                NULL, 
                                NULL, 
                                NULL, 
                                NULL, 
                                NULL, 
                                NULL);           
            
            END TRY
            BEGIN CATCH

                SELECT @return_message = ERROR_MESSAGE();

                --Throw the error to the user (this will stop the procedure)
                THROW 500001, @return_message, 1;

            END CATCH

        /*  Add go back button for the same day on the screen */

            BEGIN TRY

                SELECT @return_layout_id = MAX(btn.layout_id)
                FROM [dbo].[T_SALES_LAYOUT_BUTTON] AS btn
                     INNER JOIN [dbo].[T_SALES_LAYOUT] AS lay ON lay.[id] = btn.[layout_id]
                WHERE btn.[caption2] = FORMAT(GETDATE(),'MM/dd/yyyy')
                  AND lay.[description] LIKE 'special_exhibit_add%'

                  --SELECT  @layout_id, @nav_button_type, @y_pos, @x_max


                IF ISNULL(@return_layout_id,0) > 0
                    INSERT INTO [#fut_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                [zone_mode], [zone_sequence], [itemparentid])
                    VALUES (0,
                            @layout_id,
                            @nav_button_type,
                            @y_pos,
                            @x_max,
                            '', 
                            '<<< GO BACK <<<', 
                            '', 
                            '', 
                            @color_black, 
                            @color_white, 
                            NULL,
                            @return_layout_id,
                            NULL, 
                            NULL, 
                            GETDATE(), 
                            dbo.FS_USER(),
                            [dbo].[FS_LOCATION](), 
                            GETDATE(), 
                            dbo.FS_USER(), 
                            NULL, 
                            NULL, 
                            0, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL);

            END TRY
            BEGIN CATCH

                SELECT @return_message = ERROR_MESSAGE();

                --Throw the error to the user (this will stop the procedure)
                THROW 500001, @return_message, 1;

            END CATCH

            --Move to the next date
            SELECT @start_dt = DATEADD(DAY,1,@start_dt)
            
        END;
        
        BEGIN TRY

            --Delete existing buttons from the table
            DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON]
            WHERE [layout_id] IN (SELECT DISTINCT [layout_id] FROM [#fut_buttons] WHERE layout_id <> @parent_layout_id);

            
            --SELECT id, description FROM dbo.T_SALES_LAYOUT WHERE id IN (SELECT DISTINCT [layout_id] FROM [#fut_buttons] WHERE layout_id <> @parent_layout_id)

            --Insert new buttons into the table
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                       [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], 
                                                       [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], 
                                                       [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
            SELECT  [layout_id],
                    [button_type_id],
                    [ypos],
                    [xpos],
                    [caption1],
                    [caption2],
                    [caption3],
                    [caption4],
                    [background_color],
                    [foreground_color],
                    [itemamount],
                    [itemid],
                    [itemotherid],
                    [itemsubid],
                    [product_start_mode],
                    [product_start_dt],
                    [product_start_day_add],
                    [product_sequence],
                    [product_end_mode],
                    [product_end_day_add],
                    [product_end_dt],
                    [zone_mode],
                    [zone_sequence],
                    [itemparentid]
            FROM [#fut_buttons];

        END TRY
        BEGIN CATCH

            --include the layout name with the error message                           
            SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();
        
            --Throw the error to the user (this will stop the procedure)
            THROW 500001, @return_message, 1;

        END CATCH                  
        
        IF @return_message = '' SELECT @return_message = 'success'

    FINISHED:

        IF cursor_status('global','performance_cursor_futureperf') > 0 CLOSE performance_cursor_futureperf
        IF cursor_status('global','performance_cursor_futureperf') > -3 DEALLOCATE performance_cursor_futureperf

        IF OBJECT_ID('tempdb..#fut_buttons') IS NOT NULL DROP TABLE [#fut_buttons]

END
GO
        
    --DECLARE @rtn VARCHAR(255)
    --EXECUTE [dbo].[LP_QS_special_exhibit_future_performances]
    --        @exhibit_name = 'Body Worlds', @return_message = @rtn OUTPUT
    --PRINT @rtn



    --DECLARE @start_dt DATETIME = GETDATE()
--DECLARE @x_pos INT = 2, @y_pos INT = 9
--DECLARE @x_max INT = 5, @y_max INT = 15
--DECLARE @prf_no INT = 0, @prf_season INT = 0, @prf_dt DATETIME
--DECLARE @layout_id INT = 113
 ----Constant values used when inserting new values
 --       DECLARE @prf_button_type INT = 1;    --FROM TR_SALES_LAYOUT_BUTTON_TYPE
 --       DECLARE @pkg_button_type INT = 9;    --FROM TR_SALES_LAYOUT_BUTTON_TYPE
 --       DECLARE @nav_button_type INT = 30;   --FROM TR_SALES_LAYOUT_BUTTON_TYPE
 --       DECLARE @color_white INT = 16777215;
 --       DECLARE @color_black INT = 0;
 --       DECLARE @btn_bg_color INT = 11202814;
 --       DECLARE @btn_bg_color_alternate INT = 11261438;
 --       DECLARE @btn_fg_color INT = 64;
 --       DECLARE @special_exhibit_title_no INT = 37;





        --SELECT DISTINCT layout_id FROM [#fut_buttons]


        --SELECT * FROM #fut_buttons ORDER BY layout_id, ypos, xpos



         --SELECT performance_no, performance_zone, performance_dt, performance_time_display, season_no FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE performance_no = 52982 AND performance_zone = 592


         --SELECT * FROM dbo.T_SALES_LAYOUT WHERE description LIKE '%today%'
         --SELECT * FROM dbo.T_SALES_LAYOUT WHERE id = 117
         --SELECT * FROM dbo.T_SALES_LAYOUT_BUTTON WHERE layout_id = 53

         --DELETE FROM dbo.T_SALES_LAYOUT_BUTTON WHERE  layout_id BETWEEN 140 AND 153
         --SELECT * FROM dbo.T_SALES_LAYOUT_BUTTON WHERE  layout_id BETWEEN 140 AND 153
         
         --SELECT * FROM dbo.T_SALES_LAYOUT WHERE id BETWEEN 140 AND 153