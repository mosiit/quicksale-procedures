USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_Messages]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_Messages]
GO

/*
    Local Stored Procedure: LP_QS_Messages


*/

CREATE PROCEDURE [dbo].[LP_QS_Messages]
        @force_overwrite char(1) = 'N',
        @return_message varchar(100) = null OUTPUT
AS BEGIN

    DECLARE @bkColorDefault int, @bkColorMessages int, @foColorMessages int
    DECLARE @layout_no int, @xPos int, @yPos int, @bTypeNav int
    DECLARE @caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(20)

    SELECT @return_message = ''

    SELECT @force_overwrite = IsNull(@force_overwrite,'')
    IF @force_overwrite <> 'Y' SELECT @force_overwrite = 'N'

     /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorMessages = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Messages'
    SELECT @bkColorMessages = isnull(@bkColorMessages, @bkColorDefault)

    SELECT @foColorMessages = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Messages'
    SELECT @foColorMessages = isnull(@foColorMessages, 0)

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = isnull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END
        
    DECLARE MessageCursor INSENSITIVE CURSOR FOR
    SELECT [message_layout], [message_row], [message_column], [message_001], [message_002], [message_003] 
    FROM [dbo].[LTR_QS_Messages] WHERE [inactive] = 'N' ORDER BY [last_update_dt] DESC
    OPEN MessageCursor
    BEGIN_MESSAGE_LOOP:

        FETCH NEXT FROM MessageCursor INTO @layout_no, @yPos, @xPos, @Caption1, @Caption2, @Caption3
        IF @@FETCH_STATUS = -1 GOTO END_MESSAGE_LOOP


        BEGIN TRY

            BEGIN TRANSACTION

                IF @force_overwrite = 'Y' or exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and ypos = @yPos and xpos = @xPos and button_type_id = @bTypeNav and background_color = @bkColorMessages)
                    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_no and ypos = @yPos and xpos = @xPos

                IF not exists (SELECT * FROM T_SALES_LAYOUT_BUTTON WHERE [layout_id] = @layout_no and ypos = @yPos and xpos = @xPos)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                               [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                               [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                               [zone_sequence], [itemparentid])
                    VALUES (@layout_no, @bTypeNav, @Ypos, @xPos, @Caption1, @Caption2, @Caption3, '', @bkColorMessages, @foColorMessages, null, @layout_no, 
                            null, null, null, null, 0, null, null, null, null, null, null, null)

            COMMIT TRANSACTION

        END TRY
        BEGIN CATCH
        
            WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert a message.'
            GOTO DONE

        END CATCH


        GOTO BEGIN_MESSAGE_LOOP

    END_MESSAGE_LOOP:
    CLOSE MessageCursor
    DEALLOCATE MessageCursor
    
    IF @return_message = '' SELECT @return_message = 'success'
    
    DONE:

        IF @@TRANCOUNT > 0 BEGIN
            IF @return_message = '' SELECT @return_message = 'Transaction Error: Non-Committed transaction existed at the end of the procedure.'
            WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        END
      
        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_Messages] TO impusers
GO

