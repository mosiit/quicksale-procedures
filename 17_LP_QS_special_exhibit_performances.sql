USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_special_exhibit_performances]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_special_exhibit_performances] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_special_exhibit_performances] TO impusers' 
END
GO
/*
    LP_QS_special_exhibit_performances:  This procedure creates buttons for the actual special exhibit performances.  If packages are being used,
                                         this would be the add on performances of the special exhibit (Body Worlds when originally written).
                                         
                                         The procedure does all the work in a temp table and then moves all the buttons at once into the proper
                                         tables in impresario.  
*/
ALTER PROCEDURE [dbo].[LP_QS_special_exhibit_performances]
        @start_dt DATETIME = Null,
        @exhibit_name VARCHAR(30) = NULL,
        @return_message VARCHAR(1000) = NULL OUTPUT
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables and Tables  */

        --parent_x_max and parent_y_max are hard coded but are also double-checked later
        DECLARE @parent_layout_name VARCHAR(30) = 'special_exhibit', @parent_layout_id INT = 0;
        DECLARE @parent_x_pos INT = 1, @parent_y_pos INT = 1, @parent_x_max INT = 5, @parent_y_max INT = 5;

        --
        DECLARE @layout_name VARCHAR(30) = 'special_exhibit_add_01', @layout_no INT = 2, @layout_id INT = 0;
        DECLARE @layout_name_aud VARCHAR(30) = 'special_exhibit_aud_01', @layout_id_aud INT = 0
        DECLARE @layout_name_vou VARCHAR(30) = 'special_exhibit_vou_01', @layout_id_vou INT = 0
        DECLARE @x_pos INT = 1, @y_pos INT = 1, @x_max INT = 5, @y_max INT = 15;

        --Destination layout for first 11 days into the future
        DECLARE @destination_layout_name VARCHAR(30) = '', @destination_layout_id INT = 0;;

        --Constant values used when inserting new values
        DECLARE @prf_button_type INT = 1;    --FROM TR_SALES_LAYOUT_BUTTON_TYPE
        DECLARE @nav_button_type INT = 30;   --FROM TR_SALES_LAYOUT_BUTTON_TYPE
        DECLARE @zone_mode INT = 3;          --3 = Prompt
        DECLARE @color_white INT = 16777215;
        DECLARE @color_black INT = 0;
        DECLARE @btn_bg_color INT = 1102588;
        DECLARE @btn_bg_color_alternate INT = 11261438;
        DECLARE @btn_bg_color_aud INT = 16306648;
        DECLARE @btn_bg_color_vou INT = 11202814;


        DECLARE @btn_fg_color INT = 64;
        DECLARE @special_exhibit_title_no INT = 37;

        --Variables to hold the cursor values and for creating the actual buttons.
        DECLARE @prf_no INT = 0, @prf_description VARCHAR(30) = '', @prf_season INT, @prf_dt DATETIME, @prev_prf_dt DATETIME;
        DECLARE @mn_name VARCHAR(30), @days_out INT = 0, @within_11 CHAR(1) = 'N';

        --Putting everything into a temp table then moving it all into impresario at once
        --This is an exact copy of T_SALES_LAYOUT_BUTTON                
        IF OBJECT_ID('tempdb..#prf_buttons') IS NOT NULL DROP TABLE [#prf_buttons];
            
        CREATE TABLE [#prf_buttons] ([id] INT NOT NULL DEFAULT (0),                             [layout_id] INT NOT NULL DEFAULT (0),
	                                 [button_type_id] INT NOT NULL  DEFAULT (0),                [ypos] INT NOT NULL DEFAULT ((0)),
	                                 [xpos] INT NOT NULL DEFAULT ((0)),                         [caption1] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption2] VARCHAR(30) NULL DEFAULT (''),                  [caption3] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption4] VARCHAR(30) NULL DEFAULT (''),                  [background_color] INT NOT NULL DEFAULT (0),
	                                 [foreground_color] INT NOT NULL DEFAULT (0),               [itemamount] MONEY NULL DEFAULT (0.0),
	                                 [itemid] INT NULL DEFAULT (0),                             [itemotherid] INT NULL DEFAULT (0),
	                                 [itemsubid] INT NULL DEFAULT (0),                          [create_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
	                                 [created_by] VARCHAR(8) NOT NULL DEFAULT (''),             [create_loc] VARCHAR(16) NOT NULL DEFAULT (''),
	                                 [last_update_dt] DATETIME NOT NULL DEFAULT (GETDATE()),    [last_updated_by] VARCHAR(8) NOT NULL DEFAULT (''),
	                                 [product_start_mode] INT NULL DEFAULT (0),                 [product_start_dt] DATETIME NULL,
	                                 [product_start_day_add] INT NULL DEFAULT (0),              [product_sequence] INT NULL DEFAULT (0),
	                                 [product_end_mode] INT NULL DEFAULT (0),                   [product_end_day_add] INT NULL DEFAULT (0),
	                                 [product_end_dt] DATETIME NULL,                            [zone_mode] INT NULL DEFAULT (0),
	                                 [zone_sequence] INT NULL DEFAULT (0),                      [itemparentid] INT NULL DEFAULT (0));
 
    /* Check Parameters  */

        --If no date passed, start with today
        IF @start_dt IS NULL SELECT @start_dt = GETDATE();

        --Remove any time information from the start date
        SELECT @start_dt = CAST(@start_dt AS DATE);

        --Always start at the first of the month
        WHILE DATEPART(DAY,@start_dt) > 1 SELECT @start_dt = DATEADD(DAY,-1,@start_dt);
               
        --Initialize if necessary
        SELECT @Exhibit_name = ISNULL(@Exhibit_name,'');

        SELECT @return_message = '';
        

    /*  Determine parent layout id number  --  This is the special_exhibit layout
        The procedure cannot proceed without this id number  */

        BEGIN TRY
        
            --Determine the layout ID of the main Special Exhibit screen simply caused special_exhibit
            --This layout will not be created if it's not there.  It needs to be created manually.
            SELECT @parent_layout_name = 'special_exhibit';

            SELECT @parent_layout_id = ISNULL([id],0) 
                                       FROM [dbo].[T_SALES_LAYOUT]
                                       WHERE [description] = @parent_layout_name;

            --If layout does not exist, create iot and try again to retrieve the id number
            IF ISNULL(@parent_layout_id,0) = 0 BEGIN
                
                INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                SELECT @parent_layout_name, 
                       'N', 
                       10, 
                       5, 
                       -1, 
                       'N';

                SELECT @parent_layout_id = ISNULL([id],0) 
                                           FROM [dbo].[T_SALES_LAYOUT] 
                                           WHERE [description] = @parent_layout_name;

            END;

            --Throw and error if id number still not found (This will jump to catch block)
            IF ISNULL(@parent_layout_id,0) <= 0 
                THROW 500001,'Unable to determine sales layout id.',1;

            --Retrieve number of rows and columns in the parent layout (default to 5 if not found)
            SELECT @parent_x_max = ISNULL([columns],5),
                   @parent_y_max = ISNULL([rows],5)  FROM [dbo].[T_SALES_LAYOUT] 
                                                     WHERE [id] = @parent_layout_id;

            --Pull last row used in T_SALES_LAYOUT_BUTTON (populated by other procedures)
            --It nothing found, set row to 1 otherwise add 2 to row number to skip line between last section and this section
            SELECT @parent_y_pos = MAX([ypos]) FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @parent_layout_id;
            
            IF ISNULL(@parent_y_pos,0) = 0 SELECT @parent_y_pos = 1 
            ELSE SELECT @parent_y_pos = ISNULL(@parent_y_pos,0) + 2;

        END TRY
        BEGIN CATCH

            --include the layout name with the error message                           
            SELECT @return_message = @parent_layout_name + ' - ' + ERROR_MESSAGE();

            --Throw the error to the user (this will stop the procedure)
            THROW 500001, @return_message, 1;

        END CATCH;

    /******************************************************************************************************************/

        /*  The first Button in each section of the special exhibit layout is always for TODAY  */

            BEGIN TRY

                --Get performance information for today's performance (if there is one)
                SELECT @prf_no = MAX([performance_no]),
                       @prf_description = MAX([production_name]),
                       @prf_season = MAX([season_no]),
                       @prf_dt = MAX([performance_dt])
                FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                WHERE DATEDIFF(DAY,performance_dt, GETDATE()) = 0
                  AND title_no = @special_exhibit_title_no;

                --If performance number found with a valid season and performance date, add button in the first slot of row 1 
                --(where the add on buttons start) -- Since this is within ten days, it is a navigation button to today's time.

                SELECT @destination_layout_name = 'today_special_add'
                SELECT @destination_layout_id = ISNULL([id],0) 
                                                FROM [dbo].[T_SALES_LAYOUT] 
                                                WHERE [description] = @destination_layout_name;

                --If the destiantion layout does not exist, create it now.
                IF ISNULL(@destination_layout_id,0) = 0 BEGIN
                
                    INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                    SELECT @destination_layout_name, 
                           'N', 
                           @y_max, 
                           @x_max, 
                           -1, 
                           'N';

                    SELECT @destination_layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @destination_layout_name;

                END;

                --If destination layout still can't be found, throw an error (jumps to catch block)
                IF @destination_layout_id <= 0 
                    THROW 500001,'Unable to determine destination sales layout id.',1;  

                IF ISNULL(@prf_no,0) > 0 AND ISNULL(@prf_season,0) > 0 AND @prf_dt IS NOT NULL
                    INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                [zone_mode], [zone_sequence], [itemparentid])
                    VALUES (0, 
                            @parent_layout_id, 
                            @nav_button_type, 
                            1, 
                            1, 
                            CASE WHEN @Exhibit_name = '' THEN @prf_description 
                                 ELSE @Exhibit_name END, 
                            FORMAT(@prf_dt,'MM/dd/yyyy'), 
                            'Add On', 
                            'Today', 
                            @color_white, 
                            @btn_fg_color, 
                            NULL, 
                            @destination_layout_id, 
                            NULL, 
                            NULL, 
                            GETDATE(), 
                            dbo.FS_USER(), 
                            [dbo].[FS_LOCATION](), 
                            GETDATE(), 
                            dbo.FS_USER(), 
                            NULL, 
                            NULL, 
                            0, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL, 
                            NULL);

                --Set new button position to the second button on the row
                SELECT @parent_x_pos = 2; 

            END TRY
            BEGIN CATCH

                --include the layout name with the error message                           
                SELECT @return_message = CASE WHEN ERROR_MESSAGE() LIKE '%destination%' THEN @destination_layout_name ELSE @layout_name END + ' - ' + ERROR_MESSAGE();

                --Throw the error to the user (this will stop the procedure)
                THROW 500001, @return_message, 1;

            END CATCH;

    /******************************************************************************************************************/

    /*  Special Exhibits will be divided by month.  Each month will have its own layout.
        The first layout name for special exhibits is special_exhibit_01, as indicated in the variable declaration 
        above Need to determine the id number of that layout (if layout does not exist, it will automatically be 
        created later).  */

        BEGIN TRY

            SELECT @layout_id = NULL;

            SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name;

            --Package buttons are created one at a time.  The package information is pulled into a cursor.
            DECLARE performance_cursor_perf CURSOR FORWARD_ONLY FOR
            SELECT DISTINCT [performance_no],
                            [production_name],
                            [season_no],
                            [performance_dt]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
            WHERE [performance_dt] >= @start_dt
              AND title_no = @special_exhibit_title_no
            ORDER BY [performance_dt];

            OPEN performance_cursor_perf;
        
            --Pull first record from the cursor
            FETCH NEXT FROM performance_cursor_perf INTO @prf_no, @prf_description, @prf_season, @prf_dt;

            --Set to same as pkg date for first record -- After first record, this will contain date of previous record
            --This is used to determine if the procedure has crossed over into a new month
            SELECT @prev_prf_dt = @prf_dt;

            WHILE @@FETCH_STATUS <> -1 BEGIN

                --Nested Try/Catch
                BEGIN TRY

                    --Determine how many days out from today we are
                    --How things are handled is different for first 11 days from today than for dates after that
                    SELECT @days_out = DATEDIFF(DAY, GETDATE(), @prf_dt);

                    IF @days_out BETWEEN 0 AND 11 SELECT @within_11 = 'Y'
                    ELSE SELECT @within_11 = 'N';
                        
                    --If the date is within the 11 days of today, the button is going to navigate to a separate screen rather than 
                    --select a specific day's performances.  On the destination screen will be individual buttons for each time
                    IF @within_11 = 'Y' BEGIN

                        --Reset variables first
                        SELECT @destination_layout_id = NULL, @destination_layout_name = '';

                        --Layout name is different depending on how may days out 
                        IF @days_out = 0 SELECT @destination_layout_name = 'today_special_add'
                        ELSE IF @days_out = 1 SELECT @destination_layout_name = 'tomorrow_special_add'
                        ELSE SELECT @destination_layout_name = 'future_special_add_' + CASE WHEN (@days_out - 1) < 10 THEN '0'
                                                                                       ELSE '' END + CAST((@days_out - 1) AS VARCHAR(2));

                        SELECT @destination_layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @destination_layout_name;

                        IF ISNULL(@destination_layout_id,0) = 0 BEGIN
                
                            INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                            SELECT @destination_layout_name, 'N', @y_max, @x_max, -1, 'N';

                            SELECT @destination_layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @destination_layout_name;
                       END;

                        --If layout still can't be found, throw an error (jumps to catch block)
                        IF @destination_layout_id <= 0 THROW 500001,'Unable to determine sales layout id.',1;  

                    END;

                END TRY
                BEGIN CATCH

                    --include the layout name with the error message                           
                    SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();
                    
                    --Make sure the cursor is closed and deallocated
                    IF cursor_status('global','performance_cursor_perf') > 0 CLOSE performance_cursor_perf;
                    IF cursor_status('global','performance_cursor_perf') > -3 DEALLOCATE performance_cursor_perf;
                    
                    --Throw the error to the user (this will stop the procedure)
                    THROW 500001, @return_message, 1;
                
                END CATCH;

                ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
            
                BEGIN TRY

                    --If month of current record is different from month of previous record, layout needs to change
                    --Set the name to the new layout then attempt to get an id number for that layout
                    IF DATEDIFF(MONTH,@prev_prf_dt, @prf_dt) > 0 BEGIN

                        IF @x_pos <> 1 SELECT @y_pos += 1

                        SELECT @layout_id_aud = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_aud;

                        SELECT @layout_id_vou = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_vou;

                        IF ISNULL(@layout_ID_vou,0)> 0
                            INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                        [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                        [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                        [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                        [zone_mode], [zone_sequence], [itemparentid])
                            VALUES (0, @layout_id, @nav_button_type, @y_pos, 2, 'Exhibit Voucher', @mn_name, '', '',
                                    @btn_bg_color_vou, @btn_fg_color, NULL, @layout_id_vou, NULL, NULL, GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), 
                                    GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
                                        
                        IF ISNULL(@layout_ID_aud,0)> 0
                            INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                        [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                        [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                        [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                        [zone_mode], [zone_sequence], [itemparentid])
                            VALUES (0, @layout_id, @nav_button_type, @y_pos, 3, 'Audio Tour', @mn_name, '', '',
                                    @btn_bg_color_aud, @btn_fg_color, NULL, @layout_id_aud, NULL, NULL, GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), 
                                    GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

                        INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                    [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                    [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                    [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                    [zone_mode], [zone_sequence], [itemparentid])
                        VALUES (0, @layout_id, @nav_button_type, @y_pos, 5, '', '<<< GO BACK <<<', '', '', @color_black, @color_white, NULL, @parent_layout_id, NULL, NULL,
                                GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

                        SELECT @layout_id = NULL,
                               @layout_id_aud = NULL,
                               @layout_id_vou = null;

                        SELECT @layout_name = 'special_exhibit_add_' 
                                            + CASE WHEN @layout_no < 10 THEN '0' ELSE '' END 
                                            + CONVERT(VARCHAR(2),@layout_no);

                        SELECT @layout_name_aud = 'special_exhibit_aud_' 
                                                + CASE WHEN @layout_no < 10 THEN '0' ELSE '' END 
                                                + CONVERT(VARCHAR(2),@layout_no);

                        SELECT @layout_name_vou = 'special_exhibit_vou_' 
                                                + CASE WHEN @layout_no < 10 THEN '0' ELSE '' END 
                                                + CONVERT(VARCHAR(2),@layout_no);

                        SELECT @layout_no += 1;

                        SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name;

                        SELECT @layout_id_aud = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_aud;

                        SELECT @layout_id_vou = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_vou;

                        SELECT @x_pos = 1,
                               @y_pos = 1;

                    END;

                    --If layout does not exist (layout_id = 0) then create a new layout and try again to get the id number
                    IF ISNULL(@layout_id,0) = 0 BEGIN
                
                        INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                        VALUES (@layout_name, 'N', @y_max, @x_max, -1, 'N');

                        SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name;

                    END;

                    --If layout still can't be found, throw an error (jumps to catch block)
                    IF @layout_id <= 0 THROW 500001,'Unable to determine sales layout id.',1;            

                END TRY
                BEGIN CATCH

                    --include the layout name with the error message                           
                    SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                    --Make sure the cursor is closed and deallocated
                    IF cursor_status('global','performance_cursor_perf') > 0 CLOSE performance_cursor_perf;
                    IF cursor_status('global','performance_cursor_perf') > -3 DEALLOCATE performance_cursor_perf;
        
                    --Throw the error to the user (this will stop the procedure)
                    THROW 500001, @return_message, 1;
                      
                END CATCH;

                ---------------------------------------------------------------------------------------------------------------------------------------------------------------------

                BEGIN TRY

                    SELECT @mn_name = DATENAME(MONTH,@prf_dt) + ', ' + DATENAME(YEAR,@prf_dt);

                    --Add the navigation button to this month's buttons to the parent layout
                    IF NOT EXISTS (SELECT * FROM [#prf_buttons] WHERE [layout_id] = @parent_layout_id AND itemid = @layout_id) BEGIN

                        INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                    [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                    [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                    [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                    [zone_mode], [zone_sequence], [itemparentid])
                        VALUES (0, @parent_layout_id, @nav_button_type, @parent_y_pos, @parent_x_pos, @Exhibit_name, @mn_name, 'Add On', '',
                                @btn_bg_color, @btn_fg_color, NULL, @layout_id, NULL, NULL, GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), 
                                GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

                        SELECT @parent_x_pos += 1

                        IF @parent_x_pos > @parent_x_max
                            SELECT @parent_y_pos += 1,
                                   @parent_x_pos = 1;

                    END;

                    --If it's within 11 days from today, it will be a navigation button to a separate screen with all the times for that day
                    --If it's not within 11 days from today, it will be a performance button that will prompt for a zone.
                    IF @within_11 = 'Y'

                        INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                    [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                    [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                    [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                    [zone_mode], [zone_sequence], [itemparentid])
                        VALUES (0, @layout_id, @nav_button_type, @y_pos, @x_pos, CASE WHEN @Exhibit_name = '' THEN @prf_description ELSE @Exhibit_name END, 
                                FORMAT(@prf_dt,'MM/dd/yyyy'), 'Add On', '', @btn_bg_color_alternate, @btn_fg_color, NULL, @destination_layout_id, NULL, NULL, GETDATE(), 
                                dbo.FS_USER(), [dbo].[FS_LOCATION](), GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

                    ELSE

                        INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                    [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                    [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                    [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                    [zone_mode], [zone_sequence], [itemparentid])
                        VALUES (0, @layout_id, @prf_button_type, @y_pos, @x_pos, CASE WHEN @Exhibit_name = '' THEN @prf_description ELSE @Exhibit_name END, 
                                FORMAT(@prf_dt,'MM/dd/yyyy'), 'Add On', '', @btn_bg_color, @btn_fg_color, NULL, @prf_no, NULL, NULL, GETDATE(), dbo.FS_USER(), 
                                [dbo].[FS_LOCATION](), GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, @zone_mode, NULL, @prf_season);

                   SELECT @x_pos += 1;

                   IF @x_pos > @x_max
                        SELECT @y_pos += 1,
                               @x_pos = 1;

                END TRY
                BEGIN CATCH

                    --include the layout name with the error message                           
                    SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                    --Make sure the cursor is closed and deallocated
                    IF cursor_status('global','performance_cursor_perf') > 0 CLOSE performance_cursor_perf;
                    IF cursor_status('global','performance_cursor_perf') > -3 DEALLOCATE performance_cursor_perf;

                    --Throw the error to the user (this will stop the procedure)
                    THROW 500001, @return_message, 1;

                END CATCH;

                SELECT @prev_prf_dt = @prf_dt
                FETCH NEXT FROM performance_cursor_perf INTO @prf_no, @prf_description, @prf_season, @prf_dt;

            END
            CLOSE performance_cursor_perf
            DEALLOCATE performance_cursor_perf

            IF @x_pos <> 1 SELECT @y_pos += 1
                
                IF ISNULL(@layout_ID_vou,0)> 0
                    INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                [zone_mode], [zone_sequence], [itemparentid])
                    VALUES (0, @layout_id, @nav_button_type, @y_pos, 2, 'Exhibit Voucher', @mn_name, '', '',
                            @btn_bg_color_vou, @btn_fg_color, NULL, @layout_id_vou, NULL, NULL, GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), 
                            GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
                                                    
                IF ISNULL(@layout_ID_aud,0)> 0
                    INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                                [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                                [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                                [zone_mode], [zone_sequence], [itemparentid])
                    VALUES (0, @layout_id, @nav_button_type, @y_pos, 3, 'Audio Tour', @mn_name, '', '',
                            @btn_bg_color_aud, @btn_fg_color, NULL, @layout_id_aud, NULL, NULL, GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), 
                            GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

                INSERT INTO [#prf_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                            [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [create_dt], 
                                            [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                            [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], 
                                            [zone_mode], [zone_sequence], [itemparentid])
                VALUES (0, @layout_id, @nav_button_type, @y_pos, 5, '', '<<< GO BACK <<<', '', '', @color_black, @color_white, NULL, @parent_layout_id, NULL, NULL,
                        GETDATE(), dbo.FS_USER(), [dbo].[FS_LOCATION](), GETDATE(), dbo.FS_USER(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

                      

        
        END TRY
        BEGIN CATCH

            --include the layout name with the error message                           
            SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();
        
            --Make sure the cursor is closed and deallocated
            IF cursor_status('global','performance_cursor_perf') > 0 CLOSE performance_cursor_perf;
            IF cursor_status('global','performance_cursor_perf') > -3 DEALLOCATE performance_cursor_perf;

            --Throw the error to the user (this will stop the procedure)
            THROW 500001, @return_message, 1;

        END CATCH

    /******************************************************************************************************************/
        
        BEGIN TRY

            --Delete existing buttons from the table
            DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON]
            WHERE [layout_id] IN (SELECT DISTINCT [layout_id] 
                                  FROM [#prf_buttons] 
                                  WHERE layout_id <> @parent_layout_id);

            --Insert new buttons into the table
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                       [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], 
                                                       [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], 
                                                       [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
            SELECT  [layout_id],
                    [button_type_id],
                    [ypos],
                    [xpos],
                    [caption1],
                    [caption2],
                    [caption3],
                    [caption4],
                    [background_color],
                    [foreground_color],
                    [itemamount],
                    [itemid],
                    [itemotherid],
                    [itemsubid],
                    [product_start_mode],
                    [product_start_dt],
                    [product_start_day_add],
                    [product_sequence],
                    [product_end_mode],
                    [product_end_day_add],
                    [product_end_dt],
                    [zone_mode],
                    [zone_sequence],
                    [itemparentid]
            FROM [#prf_buttons];

        END TRY
        BEGIN CATCH

            --include the layout name with the error message                           
            SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

            --Make sure the cursor is closed and deallocated
            IF cursor_status('global','performance_cursor_perf') > 0 CLOSE performance_cursor_perf;
            IF cursor_status('global','performance_cursor_perf') > -3 DEALLOCATE performance_cursor_perf;
        
            --Throw the error to the user (this will stop the procedure)
            THROW 500001, @return_message, 1;

        END CATCH

    /******************************************************************************************************************/

        IF @return_message = '' SELECT @return_message = 'success';

    FINISHED:

        IF cursor_status('global','performance_cursor_perf') > 0 CLOSE performance_cursor_perf;
        IF cursor_status('global','performance_cursor_perf') > -3 DEALLOCATE performance_cursor_perf;

        --Destroy the temp table
        IF OBJECT_ID('tempdb..#prf_buttons') IS NOT NULL DROP TABLE [#prf_buttons];

END
GO
  
    --DECLARE @rtn VARCHAR(1000) = ''
    --EXECUTE [dbo].[LP_QS_special_exhibit_performances] @start_dt = NULL, @exhibit_name = 'Body Worlds', @return_message = @rtn OUTPUT
    --PRINT @rtn


    