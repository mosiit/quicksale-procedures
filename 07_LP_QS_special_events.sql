USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_special_events]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_special_events]
GO

CREATE PROCEDURE [dbo].[LP_QS_special_events]
        @performance_dt datetime = null,
        @is_today char(1) = null,
        @is_tomorrow char(1) = null,
        @return_message varchar(100) = null OUTPUT
AS BEGIN
    
    DECLARE @bkColorEvents int, @bkColorDefault int, @foColorToday int, @foColorFuture int, @foColor int
    DECLARE @special_event_layout_no int, @launch_layout_no int, @perf_no int, @prod_season_no int, @season_no int
    DECLARE @perf_zone_no int, @bTypePerf int, @bTypeNav int
    DECLARE @prod_name varchar(30), @prod_season_name varchar(30), @perf_date char(10), @performance_date char(10), @perf_name VARCHAR(30)
    DECLARE @caption1 varchar(30), @caption2 varchar(30), @caption3 varchar(30), @caption4 varchar(30)
    DECLARE @perf_dt datetime, @perf_time char(8), @curY int, @curX int, @todayY int, @todayX int, @xMax int, @yMax int

    SELECT @return_message = '', @launch_layout_no = 0
    IF @performance_dt is null SELECT @performance_dt = getdate()

    SELECT @performance_date = convert(char(10),@performance_dt,111)

    IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @bkColorEvents = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Special Events'
    SELECT @bkColorEvents = isnull(@bkColorEvents, @bkColorDefault)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)

    SELECT @foColorFuture = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Future'
    SELECT @foColorFuture = isnull(@foColorFuture, 0)
    
    /*  The button type is a necessary value.  This procedure deals with performance buttons, so the id number for that type is needed.  If it cannot be determined, the procedure cannot go on.   */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = IsNull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = IsNull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END
   
    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */
            
    SELECT @special_event_layout_no = [id], @yMax = [rows], @xMax = [columns]  FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'special_events'
    SELECT @special_event_layout_no = isnull(@special_event_layout_no, 0)
    SELECT @yMax = isnull(@yMax, 0), @xMax = isnull(@xMax, 0)
    IF @special_event_layout_no <= 0 or @yMax <= 0 or @xMax <= 0 BEGIN
        SELECT @return_message = 'unable to find the adult offering layout information'
        GOTO DONE
    END

    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @special_event_layout_no
    
    SELECT @curY = 2, @curX = 1
   
    DECLARE AdultOfferingCursor INSENSITIVE CURSOR FOR
    SELECT performance_no, performance_dt, performance_time_display, production_season_no, production_name, production_season_name, performance_zone, performance_name 
    FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_date >= convert(char(10),@performance_dt,111) and title_name in ('Adult Offerings', 'Events', 'Member Only Events')
    ORDER BY performance_date, performance_time
    OPEN AdultOfferingCursor
    BEGIN_ADULT_OFFERING_LOOP:
    
        FETCH NEXT FROM AdultOfferingCursor INTO @perf_no, @perf_dt, @perf_time, @prod_season_no, @prod_name, @prod_season_name, @perf_zone_no, @perf_name
        IF @@FETCH_STATUS = -1 GOTO END_ADULT_OFFERING_LOOP
        
        SELECT @perf_date = convert(char(10),@perf_dt,111)
        IF @perf_date = convert(char(10),getdate(),111) SELECT @foColor = @foColorToday ELSE SELECT @foColor = @foColorFuture

        /*  Need the Season number that corresponds to the Exhibit Hall Production Season  */

        SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @prod_season_no
        SELECT @season_no = IsNull(@season_no, 0)
        IF @season_no <= 0 BEGIN
            SELECT @return_message = 'unable to determine the season for ' + @prod_season_name
            GOTO END_ADULT_OFFERING_LOOP
        END
     
        SELECT @Caption1 = left(datename(weekday,@perf_dt),3) + ' ' + left(datename(month,@perf_dt),3) + ' ' 
                         + case WHEN datepart(day,@perf_dt) < 10 THEN '0' + convert(char(1),datepart(day,@perf_dt)) ELSE convert(char(2),datePart(day,@perf_dt)) END
        SELECT @Caption2 = @prod_name
        SELECT @caption3 = @perf_time
        SELECT @caption4 = '[Avail]'
        
                /*  One-Off Hard coded exception for the Summer Thursday series.
                    Use performance name instead of production name  */

                    IF @prod_name = 'Summer Thursdays' SELECT @caption2 = @perf_name

        /*  Insert Button into the layout  */

        BEGIN TRY

            IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @special_event_layout_no and ypos = @curY and xPos = @curx)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
                VALUES (@special_event_layout_no, @bTypeperf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorevents, @foColor, null, @Perf_no, null, @perf_zone_no, 
                        null, null, 0, null, null, null, null, 4, null, @season_no)

            IF @curY = @yMax and @curX = @xMax GOTO END_ADULT_OFFERING_LOOP
            ELSE IF @curX = @xMax SELECT @curY = (@curY + 1), @curX = 1
            ELSE SELECT @curX = (@curX + 1)

            IF @perf_date = @performance_date BEGIN

                SELECT @todayY = 4, @todayX = 1
                SELECT @launch_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Box Office Launch'
                SELECT @launch_layout_no = isnull(@launch_layout_no, 0)
                
                IF @Launch_layout_no <> 0 BEGIN

                    WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @todayY and xPos = @todayX) SELECT @todayX = @todayX + 1

                    IF @todayX <= 5 BEGIN

                        SELECT @foColor = @foColorToday

                        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                                   [zone_sequence], [itemparentid])
                        VALUES (@launch_layout_no, @bTypePerf, @todayY, @todayX, 'EVENT TODAY', @Caption2, @Caption3, @Caption4, @bkColorEvents, @foColor, null, @Perf_no, null, @perf_zone_no, 
                                null, null, 0, null, null, null, null, 4, null, @season_no)
    
                    END

                END
            
            END ELSE IF @perf_date = convert(char(10),dateadd(day,1,@performance_dt),111) BEGIN

                SELECT @todayY = 4, @todayX = 1
                SELECT @launch_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'tomorrow_Launch'
                SELECT @launch_layout_no = isnull(@launch_layout_no, 0)
                
                
                IF @Launch_layout_no <> 0 BEGIN

                    WHILE exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @launch_layout_no and ypos = @todayY and xPos = @todayX) SELECT @todayX = @todayX + 1

                    IF @todayX <= 5 BEGIN
                        
                        SELECT @foColor = @foColorFuture

                        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                                   [zone_sequence], [itemparentid])
                        VALUES (@launch_layout_no, @bTypePerf, @todayY, @todayX, 'EVENT TOMORROW', @Caption2, @Caption3, @Caption4, @bkColorEvents, @foColor, null, @Perf_no, null, @perf_zone_no, 
                                null, null, 0, null, null, null, null, 4, null, @season_no)
    
                    END

                END
            
            END
            
        
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'an error occurred while attempting to insert the adult offering button.'
            GOTO END_ADULT_OFFERING_LOOP

        END CATCH

        GOTO BEGIN_ADULT_OFFERING_LOOP

    END_ADULT_OFFERING_LOOP:
    CLOSE AdultOfferingCursor
    DEALLOCATE AdultOfferingCursor
      
    /*  *********************************************************************************************************************************** */  
    
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @@TRANCOUNT > 0 BEGIN
            IF @return_message = '' SELECT @return_message = 'Transaction Error: Non-Committed transaction existed at the end of the procedure.'
            WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        END
      
        IF @return_message = '' SELECT @return_message = 'unknown error'
    

END
GO 

GRANT EXECUTE ON [dbo].[LP_QS_special_events] TO impusers
GO



