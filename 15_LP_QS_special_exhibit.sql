USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_special_exhibit]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_special_exhibit] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_special_exhibit] TO impusers' 
END
GO


ALTER PROCEDURE [dbo].[LP_QS_special_exhibit]
        @start_dt DATETIME = NULL,
        @exhibit_name VARCHAR(30) = NULL,
        @return_message VARCHAR(100) = NULL OUTPUT
AS BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables  */

        DECLARE @layout1_name VARCHAR(30) = 'Science Central Launch', @layout1_id INT = 0;

        DECLARE @layout2_name VARCHAR(30) = 'Box Office Launch', @layout2_id INT = 0;

        DECLARE @layoutD_name VARCHAR(30) = 'special_exhibit', @layoutD_id INT = 0;

        DECLARE @x_pos INT = 5, @y_pos INT = 4 

        DECLARE @nav_button_type INT = 30   --FROM TR_SALES_LAYOUT_BUTTON_TYPE

        DECLARE @btn_bg_color INT = 11202814
        DECLARE @btn_fg_color INT = 64

        DECLARE @rtn VARCHAR(1000) = ''

    /*  Check Parameters  */

        IF @start_dt IS NULL SELECT @start_dt = GETDATE()

        --Always start at the first of the month
        WHILE DATEPART(DAY,@start_dt) > 1 SELECT @start_dt = DATEADD(DAY,-1,@start_dt)
       
        SELECT @start_dt = CAST(@start_dt AS DATE)

        SELECT @Exhibit_name = ISNULL(@Exhibit_name,'')

        SELECT @return_message = ''


    /*  Delete all buttons from the special exhibit layouts  */

        DELETE FROM dbo.T_SALES_LAYOUT_BUTTON
        WHERE layout_id IN (SELECT [id] 
                            FROM [dbo].[T_SALES_LAYOUT] 
                            WHERE [description] LIKE 'special_exhibit%')
                            
        --IF @rtn <> 'success' SELECT @return_message = @return_message + @rtn + ' / '

    /*  The first procedure creates the add on performance buttons  */

        EXECUTE [dbo].[LP_QS_special_exhibit_performances]
                @start_dt = @start_dt,
                @exhibit_name = @exhibit_name,
                @return_message = @rtn OUTPUT

        IF @rtn <> 'success' SELECT @return_message = @return_message + @rtn + ' / '

    /*  The second procedure creates the add on performance buttons  */

        EXECUTE [dbo].[LP_QS_special_exhibit_vouchers]
                @start_dt = @start_dt,
                @exhibit_name = @exhibit_name,
                @return_message = @rtn OUTPUT


        IF @rtn <> 'success' SELECT @return_message = @return_message + @rtn + ' / '
        
    /*  The third procedure creates individual zone buttons for add on perfs for today and next 11 days
        --Only run if starting from a date that requires it  */
        
        IF DATEDIFF(DAY, GETDATE(), @start_dt) < 11
            EXECUTE [dbo].[LP_QS_special_exhibit_future_performances]
                     @exhibit_name = @exhibit_name, 
                     @return_message = @rtn OUTPUT

        IF @rtn <> 'success' SELECT @return_message = @return_message + @rtn + ' / '

    /*  The fourth procedure creates the audio tour buttons  */

        EXECUTE [dbo].[LP_QS_special_exhibit_audio_tour]
                @start_dt = @start_dt,
                @exhibit_name = @exhibit_name,
                @return_message = @rtn OUTPUT

        IF @rtn <> 'success' SELECT @return_message = @return_message + @rtn + ' / '

    /*  The fifth procedure creates individual zone buttons for audio tour perfs for today and next 11 days
        --Only run if starting from a date that requires it  */
        
        IF DATEDIFF(DAY, GETDATE(), @start_dt) < 11
            EXECUTE [dbo].[LP_QS_special_exhibit_future_audio_tour]
                     @exhibit_name = @exhibit_name, 
                    @return_message = @rtn OUTPUT
    
        IF @rtn <> 'success' SELECT @return_message = @return_message + @rtn + ' / '
       
    /*  Create the button navigation buttons on the launch pages  */
    
        SELECT @layoutD_id =  ISNULL([id],0)
                              FROM [dbo].[T_SALES_LAYOUT]
                              WHERE [description] = @layoutD_name

        IF @layoutD_id > 0 BEGIN
       
            --Science Central Launch

            SELECT @layout1_id =  ISNULL([id],0)
                                  FROM [dbo].[T_SALES_LAYOUT]
                                  WHERE [description] = @layout1_name

            IF @layout1_id > 0 AND NOT EXISTS (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout1_id AND ypos = @y_pos AND xpos = @x_pos)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                           [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], 
                                                           [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], 
                                                           [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
                SELECT @layout1_id, 
                       @nav_button_type, 
                       @y_pos, 
                       @x_pos, 
                       @Exhibit_name,
                       'EXHIBIT',
                       '',
                       '',
                       @btn_bg_color,
                       @btn_fg_color,
                       NULL,
                       @layoutD_id, 
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       0,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL


            --Box Office Launch

            SELECT @layout2_id =  ISNULL([id],0)
                            FROM [dbo].[T_SALES_LAYOUT]
                            WHERE [description] = @layout2_name

            IF @layout2_id > 0 AND NOT EXISTS (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout2_id AND ypos = @y_pos AND xpos = @x_pos)
                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], 
                                                           [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], 
                                                           [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], 
                                                           [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
                SELECT @layout2_id, 
                       @nav_button_type, 
                       @y_pos, 
                       @x_pos, 
                       @Exhibit_name,
                       'EXHIBIT',
                       '',
                       '',
                       @btn_bg_color,
                       @btn_fg_color,
                       NULL,
                       @layoutD_id, 
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       0,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL
        
        END


       IF @return_message = '' SELECT @return_message = 'success'

END
GO
