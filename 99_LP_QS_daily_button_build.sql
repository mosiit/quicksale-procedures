USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_daily_button_build]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_daily_button_build] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_daily_button_build] TO impusers' 
END
GO
/*
    Local Stored Procedure: [LP_QS_daily_button_build]

*/

ALTER PROCEDURE [dbo].[LP_QS_daily_button_build]
    @performance_dt datetime = null,
    @display_results char(1) = 'N',
    @return_message varchar(100) = NULL OUTPUT
AS BEGIN

    DECLARE @layout_no_today int, @layout_no_tomorrow int, @err_count int, @title_no int
    DECLARE @layout_name_today varchar(30), @layout_name_tomorrow varchar(30)
    DECLARE @quick_sale_venue varchar(30), @rtn varchar(100), @tomorrow_dt datetime

    IF @performance_dt is null SELECT @performance_dt = getdate()
    SELECT @tomorrow_dt = dateadd(day,1,@performance_dt)

    SELECT @return_message = '', @err_count = 0

    /*  *********************************************************************************************************************************** */  

    /*  BOX OFFICE LAUNCH  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_box_office_launch] @performance_dt = @performance_dt, @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_box_office_launch (today)'

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_box_office_launch] @performance_dt = @tomorrow_dt, @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_box_office_launch (tomorrow)'

    /*  *********************************************************************************************************************************** */  

    /*  SCHOOL SCREEN  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_school] @performance_dt = @performance_dt, @return_message = @rtn OUTPUT
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_school (today)'

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_school] @performance_dt = @tomorrow_dt, @return_message = @rtn OUTPUT
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_school (tomorrow)'

    /*  *********************************************************************************************************************************** */  

    /*  INDIVIDIUAL SHOWS */
    
    DECLARE ShowButtonCursor INSENSITIVE CURSOR FOR
    SELECT [title_no], [quick_sale_venue]  FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [quick_sale_venue] <> ''
    OPEN ShowButtonCursor
    BEGIN_SHOW_BUTTON_LOOP:
    
        FETCH NEXT FROM ShowButtonCursor INTO @title_no, @quick_sale_venue
        IF @@FETCH_STATUS = -1 GOTO END_SHOW_BUTTON_LOOP

        IF @quick_sale_venue = 'THRILL' GOTO BEGIN_SHOW_BUTTON_LOOP

        SELECT @layout_name_today = 'today_' + lower(ltrim(@quick_sale_venue))
        SELECT @layout_no_today = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_today
        SELECT @layout_no_today = IsNull(@layout_no_today,0)

        IF @layout_no_today > 0 BEGIN
        
            BEGIN TRY

                SELECT @rtn = ''
                EXECUTE [dbo].[LP_QS_single_venue] 
                        @performance_dt = @performance_dt, @title_no = @title_no, @layout_no = @layout_no_today, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT
                IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
                IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_single_venue (' + @quick_sale_venue + ' - today)'

            END TRY
            BEGIN CATCH

                SELECT @return_message = left(Error_message(),100)
                IF @return_message is null SELECT @return_message = 'an error occurred while attempting to create the performance screen (' + convert(varchar(10),@title_no) + ') button for today.'
                GOTO END_SHOW_BUTTON_LOOP
                
            END CATCH

        END

        SELECT @layout_name_tomorrow = 'tomorrow_' + lower(ltrim(@quick_sale_venue))
        SELECT @layout_no_tomorrow = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name_tomorrow
        SELECT @layout_no_tomorrow = IsNull(@layout_no_tomorrow,0)

        IF @layout_no_tomorrow > 0 BEGIN
                
            BEGIN TRY

                SELECT @rtn = ''
                EXECUTE [dbo].[LP_QS_single_venue] 
                        @performance_dt = @tomorrow_dt, @title_no = @title_no, @layout_no = @layout_no_tomorrow, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT
                IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
                IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_single_venue (' + @quick_sale_venue + ' - tomorrow)'

            END TRY
            BEGIN CATCH

                SELECT @return_message = left(Error_message(),100)
                IF @return_message is null SELECT @return_message = 'an error occurred while attempting to create the performance screen (' + convert(varchar(10),@title_no) + ') button for today.'
                GOTO END_SHOW_BUTTON_LOOP
                
            END CATCH

        END

        GOTO BEGIN_SHOW_BUTTON_LOOP

    END_SHOW_BUTTON_LOOP:
    CLOSE ShowButtonCursor
    DEALLOCATE ShowButtonCursor

    /*  *********************************************************************************************************************************** */  

    /*  Full Day  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_full_day] @performance_dt = @performance_dt, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_full_day (today)'

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_full_day] @performance_dt = @tomorrow_dt, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_full_day (tomorrow)'

    /*  *********************************************************************************************************************************** */  

    /* Thrill Ride 360 */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_Thrill_Ride_360] @performance_dt = @performance_dt, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_Thrill_Ride_360'

    /*  *********************************************************************************************************************************** */  

    /*  Contributions  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_Contributions] @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_Contributions'

    /*  *********************************************************************************************************************************** */  
       
    /*  Special Events  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_special_events] @performance_dt = @performance_dt, @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_special_events'

    /*  *********************************************************************************************************************************** */  

    /*  Science Central Launch */

    EXECUTE [dbo].[LP_QS_science_central_launch] @performance_dt = @performance_dt, @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_science_central_launch'

    /*  *********************************************************************************************************************************** */  
    
    /* Summer Courses - Only Runs January through August  */

    IF datepart(month,@performance_dt) <= 8 BEGIN

        EXECUTE [dbo].[LP_QS_summer_courses] @return_message = @rtn OUTPUT
        IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
        IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_summer_courses'

    END

    /*  *********************************************************************************************************************************** */  
    
    /* Special Exhibits  */

                    --Body Worlds Running through Jan 5, 2020
                    --Let run through Jan 31 in case it gets extended

                    IF CAST(GETDATE() AS DATE) <= '1-31-2020' BEGIN

                        EXECUTE [dbo].[LP_QS_special_exhibit]
                                @start_dt = NULL,
                                @exhibit_name = 'Body Worlds',
                                @return_message = @rtn OUTPUT

                        IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
                        IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_special_exhibit'

                        /*  8/30/2019 - Two new procedures added to adjust buttons for new Body Worlds Pricing policies  */
                        EXECUTE [dbo].[LP_QS_special_exhibit_adjustments]
                                @start_dt = NULL,
                                @exhibit_name = 'Body Worlds',
                                @return_message = @rtn OUTPUT

                        IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
                        IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_special_exhibit_adjustments'

                        EXECUTE [dbo].[LP_QS_special_exhibit_vouchers_adjust] 
                                @start_dt = Null, 
                                @exhibit_name = 'Body Worlds',
                                @return_message = @rtn OUTPUT

                        IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
                        IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_special_exhibit_adjustments'
                        
                    END

    /*  *********************************************************************************************************************************** */  

    /*  Future Dates  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_future_dates] @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_future_dates'

    /*  *********************************************************************************************************************************** */  

    /*  Current School Year School Programs  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_current_school_year_programs] @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_current_school_year_programs'

    
    /*  *********************************************************************************************************************************** */  

    /*  Voucher Sales  */

    SELECT @rtn = ''
    EXECUTE [dbo].[LP_QS_vouchers] @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_vouchers'


    /*  *********************************************************************************************************************************** */  
        
    /*  City Pass  */

    EXECUTE [dbo].[LP_QS_city_pass] @return_Message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_city_pass'
    
    /*  *********************************************************************************************************************************** */  

    /*  MOS At School  */

    EXECUTE [dbo].[LP_QS_mos_at_school] @performance_dt = NULL, @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_mos_at_school'

    /*  *********************************************************************************************************************************** */  

    /*  Messages  */

    EXECUTE [dbo].[LP_QS_Messages] @force_overwrite = 'Y', @return_message = @rtn OUTPUT
    IF @rtn <> 'success' SELECT @err_count = (@err_count + 1)
    IF @display_results = 'Y' PRINT @rtn +  ':  LP_QS_Messages'
    
    /*  *********************************************************************************************************************************** */  
            
    IF @return_message = '' SELECT @return_message = 'success'

    DONE:

        IF @return_message = 'success' and @err_count > 0 SELECT @return_message = 'The task finished but with error.'

        IF @return_message = '' SELECT @return_message = 'unknown error'

    /*  *********************************************************************************************************************************** */  

END
GO

GRANT EXECUTE ON [dbo].[LP_QS_daily_button_build] TO impusers
GO