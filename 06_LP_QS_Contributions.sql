--USE [impresario]
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_Contributions]') AND type in (N'P', N'PC'))
--    DROP PROCEDURE [dbo].[LP_QS_Contributions]
--GO

ALTER PROCEDURE [dbo].[LP_QS_Contributions]
    @return_message varchar(100) = Null OUTPUT
AS BEGIN

    /*  Procedure Variables  */
    
        --DECLARE @contribution_list varchar(1000)

        DECLARE @bkColorDefault INT = 0,        @bkColorDonation INT = 0,
                @bkColorExplorer INT = 0,       @bkColorDiscoverer INT = 0,
                @bkColorInnovator INT = 0,      @bkColor INT = 0,
                @foColor INT = 0

        DECLARE @fund_contribution INT = 0,     @fund_explorer INT = 0,
                @fund_discoverer INT = 0,       @fund_innovator INT = 0

        DECLARE @layout_id INT = 0,             @layout_id_box INT = 0,
                @yMax INT = 0,                  @xMax INT = 0

        DECLARE @btn_nav INT = 30,              @btn_contr INT = 4
        
        --SELECT * FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE]

        
        DECLARE @contribution_table TABLE  ([layout_id] INT NOT NULL DEFAULT (0),
                                            [button_type_id] INT NOT NULL DEFAULT (0),
                                            [ypos] INT NOT NULL DEFAULT (0),
                                            [xpos] INT NOT NULL DEFAULT (0),
                                            [caption1] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [caption2] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [caption3] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [caption4] VARCHAR(30) NOT NULL DEFAULT (''),
                                            [background_color] INT NOT NULL DEFAULT (0),
                                            [foreground_color] INT NOT NULL DEFAULT (0),
                                            [itemamount] DECIMAL (18,2) NULL DEFAULT (0.0),
                                            [itemid] INT NULL DEFAULT (0),
                                            [itemotherid] INT NULL DEFAULT (0),
                                            [itemsubid] INT NULL DEFAULT (0),
                                            [product_start_mode] INT NULL DEFAULT (0),
                                            [product_start_dt] DATETIME NULL,
                                            [product_start_day_add] INT NULL DEFAULT (0),
                                            [product_sequence] INT NULL DEFAULT (0),
                                            [product_end_mode] INT NULL DEFAULT (0),
                                            [product_end_day_add] INT NULL DEFAULT (0),
                                            [product_end_dt] DATETIME NULL,
                                            [zone_mode] INT NULL DEFAULT (0),
                                            [zone_sequence] INT NULL DEFAULT (0),
                                            [itemparentid] INT NULL DEFAULT (0))
    
      /* Defaults  */

        SELECT @bkColorDefault = ISNULL([default_value], 14085615) FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
        SELECT @bkColorDonation = ISNULL([default_value], @bkColorDefault) FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Donation'
        SELECT @bkColorExplorer = ISNULL([default_value], @bkColorDonation) FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Explorer'
        SELECT @bkColorDiscoverer = ISNULL([default_value], @bkColorDonation) FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Discoverer'
        SELECT @bkColorInnovator = ISNULL([default_value], @bkColorDonation) FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Innovator'
        SELECT @foColor = ISNULL([default_value], 0) FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'

        SELECT @fund_contribution = ISNULL([fund_no], 0) FROM [dbo].[T_FUND] WHERE [description] = '030803 AF Front Line'  
        SELECT @fund_explorer = ISNULL([fund_no], 0) FROM [dbo].[T_FUND] WHERE [description] = '030754 AF Individual Gifts'  
        SELECT @fund_discoverer = ISNULL([fund_no], 0) FROM [dbo].[T_FUND] WHERE [description] = '030754 AF Individual Gifts'  
        SELECT @fund_innovator = ISNULL([fund_no], 0) FROM [dbo].[T_FUND] WHERE [description] = '070526 Innovator Memb - I' 

        --Primary Contributions Layout
        SELECT @layout_id = ISNULL([id], 0),
               @yMax = ISNULL([rows], 0),
               @xMax = ISNULL([columns], 0)
        FROM [dbo].[T_SALES_LAYOUT] 
        WHERE [description] = 'pre_defined_contributions'

        --Box Office Specific Layout
        SELECT @layout_id_box = ISNULL([id], 0)
        FROM [dbo].[T_SALES_LAYOUT] 
        WHERE [description] = 'pre_defined_contributions_box'

        IF @layout_id <= 0 OR @layout_id_box <= 0 OR @yMax <= 0 OR @xMax <= 0 BEGIN
            SELECT @return_message = 'unable to find contribution layout.'
            GOTO DONE
        END

           --SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE layout_id = 74 ORDER BY ypos, xpos

          

    /*  I had originally written this procedure to use default values in T_DEFAULTS TO dynamically create buttons, but the 
        first time there are a significant change in the program, I had to rewrite the procedure anyway, so this time around
        I'm just going to hard code all the buttons into the table variable.  If something changes in what levels are called or
        what the dollar amounts are, it should be easy enough to make the change to the insert statements below.  */

        --Header
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id, @btn_nav, 1, 1, 'ATTACH', 'CONSTITUENT FIRST', '', '', 11202814, 255, NULL, 68, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL) 
        
        --Front Line Donations
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id, @btn_contr, 1, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 2.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 1, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 3.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 1, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 4.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 1, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 5.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 2, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 10.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 2, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 15.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 2, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 20.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 2, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 25.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 2, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 30.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 3, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 35.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 3, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 40.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 3, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 45.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 3, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 50.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 3, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 60.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 4, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 70.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 4, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 80.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 4, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 90.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 4, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 100.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 4, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 200.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

        --Innovators
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id, @btn_contr, 5, 1, 'INNOVATOR', 'Bronze', '', '[Amount]', @bkColorInnovator, @foColor, 300.00, @fund_innovator, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
               (@layout_id, @btn_contr, 5, 2, 'INNOVATOR', 'Silver', '', '[Amount]', @bkColorInnovator, @foColor, 600.00, @fund_innovator, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 5, 3, 'INNOVATOR', 'Gold', '', '[Amount]', @bkColorInnovator, @foColor, 1500.00, @fund_innovator, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
               (@layout_id, @btn_contr, 5, 4, 'INNOVATOR', 'Platinum', '', '[Amount]', @bkColorInnovator, @foColor, 3000.00, @fund_innovator, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

        --Explorers
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id, @btn_contr, 6, 1, 'EXPLORER', 'Bronze', '', '[Amount]', @bkColorExplorer, @foColor, 300.00, @fund_explorer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
               (@layout_id, @btn_contr, 6, 2, 'EXPLORER', 'Silver', '', '[Amount]', @bkColorExplorer, @foColor, 600.00, @fund_explorer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 6, 3, 'EXPLORER', 'Gold', '', '[Amount]', @bkColorExplorer, @foColor, 1500.00, @fund_explorer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

        --Explorers
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id, @btn_contr, 7, 1, 'DISCOVERER', 'Bronze', '', '[Amount]', @bkColorDiscoverer, @foColor, 3000.00, @fund_discoverer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
               (@layout_id, @btn_contr, 7, 2, 'DISCOVERER', 'Silver', '', '[Amount]', @bkColorDiscoverer, @foColor, 6000.00, @fund_discoverer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 7, 3, 'DISCOVERER', 'Gold', '', '[Amount]', @bkColorDiscoverer, @foColor, 12000.00, @fund_discoverer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 7, 4, 'DISCOVERER', 'Platinum', '', '[Amount]', @bkColorDiscoverer, @foColor, 25000.00, @fund_discoverer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id, @btn_contr, 7, 5, 'DISCOVERER', 'Titanium', '', '[Amount]', @bkColorDiscoverer, @foColor, 50000.00, @fund_discoverer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
               (@layout_id, @btn_contr, 8, 5, 'DISCOVERER', 'Diamond', '', '[Amount]', @bkColorDiscoverer, @foColor, 100000.00, @fund_discoverer, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
    
        --Front Line Donations (Box Office Layout)
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id_box, @btn_contr, 1, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 1.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 1, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 2.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 1, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 3.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 1, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 4.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 1, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 5.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 2, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 6.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 2, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 7.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 2, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 8.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 2, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 9.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 2, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 10.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 3, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 11.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 3, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 12.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 3, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 13.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 3, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 14.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 3, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 15.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 4, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 16.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 4, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 17.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 4, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 18.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 4, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 19.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 4, 5, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 20.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 5, 1, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 21.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 5, 2, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 22.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 5, 3, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 23.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL), 
               (@layout_id_box, @btn_contr, 5, 4, 'DONATION', '', '', '[Amount]', @bkColorDonation, @foColor, 24.00, @fund_contribution, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

        --Footer (Box Office Layout)
        INSERT INTO @contribution_table ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                         [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                         [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
        VALUES (@layout_id_box, @btn_nav, 5, 5, 'MEMBER MOS', 'FOR HIGER AMOUNTS', '', '', 11202814, 255, NULL, 68, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL) 


        BEGIN TRY

            BEGIN TRANSACTION

                DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] IN (@layout_id, @layout_id_box)

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id],[ypos],[xpos],[caption1],[caption2],[caption3],[caption4],[background_color],[foreground_color],
                                                           [itemamount],[itemid],[itemotherid],[itemsubid],[product_start_mode],[product_start_dt],[product_start_day_add],[product_sequence],
                                                           [product_end_mode],[product_end_day_add],[product_end_dt],[zone_mode],[zone_sequence],[itemparentid])
                SELECT  [layout_id],
                        [button_type_id],
                        [ypos],
                        [xpos],
                        [caption1],
                        [caption2],
                        [caption3],
                        [caption4],
                        [background_color],
                        [foreground_color],
                        [itemamount],
                        [itemid],
                        [itemotherid],
                        [itemsubid],
                        [product_start_mode],
                        [product_start_dt],
                        [product_start_day_add],
                        [product_sequence],
                        [product_end_mode],
                        [product_end_day_add],
                        [product_end_dt],
                        [zone_mode],
                        [zone_sequence],
                        [itemparentid]
                FROM @contribution_table

            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH

            SELECT @return_message = left(Error_message(),100)
            IF @return_message is null SELECT @return_message = 'An error occurred while generating the pre_defined_contributions buttons.'
            WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
            GOTO DONE

        END CATCH


    IF ISNULL(@return_message,'') = '' SELECT @return_message = 'success'
    
    DONE:

        IF @@TRANCOUNT > 0 BEGIN
            IF ISNULL(@return_message,'') = '' SELECT @return_message = 'Transaction Error: Non-Committed transaction existed at the end of the procedure.'
            WHILE @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        END
      
        IF ISNULL(@return_message,'') = '' SELECT @return_message = 'unknown error'

END
GO

--    PRINT @return_message

--GRANT EXECUTE ON [dbo].[LP_QS_Contributions] TO impusers
--GO       
--SELECT * FROM [dbo].[T_DEFAULTS] WHERE [parent_table] = 'Museum of Science'
--SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] IN (68, 74) ORDER BY [layout_id], [ypos], [xpos]

