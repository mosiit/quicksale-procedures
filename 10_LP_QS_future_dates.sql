USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_future_dates]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_future_dates]
GO

/*
    Local Stored Procedure: [[LP_QS_future_dates]]


*/
    
CREATE PROCEDURE [dbo].[LP_QS_future_dates]
        @performance_dt datetime = Null,
        @number_of_days int = Null,
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    DECLARE @performance_date char(10), @day_num int, @last_dt datetime, @last_date char(10), @exh_season_name varchar(30)
    DECLARE @caption1 varchar(30), @caption2 varchar(30), @caption3 varchar(30), @caption4 varchar(30), @rtn varchar(100)
    DECLARE @layout_Id int, @future_layout varchar(30), @curY int, @curX int, @bTypeNav int, @bTypePerf int, @future_layout_no int
    DECLARE @omni_no int, @planet_no int, @4d_no int, @butterfly_no int, @err_count int, @fYear int, @exh_season_no int, @season_no int
    DECLARE @bkColorDefault int, @foColorFuture int, @bkColorOmni int, @bkColorPlanet int, @bkColor4D int, @bkColorButterfly int, @bkColorExhibitHalls int
    DECLARE @exh_perf_no int, @ZoneModeBestAvail int

    SELECT @return_message = '', @err_count = 0, @ZoneModeBestAvail = 0
        
    SELECT @performance_dt = IsNull(@performance_dt, getdate())
    SELECT @number_of_days = IsNull(@Number_of_days, 10)
    
    --FUTURE DATES START DAY AFTER TOMORROW (TODAY AND TOMORROW HAVE OWN BUTTONS)
    SELECT @performance_dt = dateadd(day,2,@performance_dt)
    SELECT @performance_date = convert(char(10), @performance_dt, 111)

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @foColorFuture = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Future'
    SELECT @foColorFuture = isnull(@foColorFuture, 0)

    SELECT @bkColorExhibitHalls = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Exhibit Halls'
    SELECT @bkColorExhibitHalls = isnull(@bkColorExhibitHalls, @bkColorDefault)
    
    /*  Button Colors: Each Venue has its own button color, stored in the content under the title records in Tessitura.
                       If a back color cannot be found, the default back color is used  */
       
    SELECT @bkColorOmni = [quick_sale_button_color], @omni_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Mugar Omni Theater'
    SELECT @bkColorOmni = Isnull(@bkColorOmni,-1)
    IF @bkColorOmni <= 0 SELECT @bkColorOmni = @bkColorDefault
    SELECT @omni_no = IsNull(@omni_no, 0)
        
    SELECT @bkColorPlanet = [quick_sale_button_color], @planet_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Hayden Planetarium'
    SELECT @bkColorPlanet = Isnull(@bkColorPlanet,-1)
    IF @bkColorPlanet <= 0 SELECT @bkColorPlanet = @bkColorDefault
    SELECT @planet_no = IsNull(@planet_no, 0)

    SELECT @bkColor4D = [quick_sale_button_color], @4d_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = '4-D Theater'
    SELECT @bkColor4D = Isnull(@bkColor4D,-1)
    IF @bkColor4D <= 0 SELECT @bkColor4D = @bkColorDefault
    SELECT @4d_no = IsNull(@4d_no, 0) 

    SELECT @bkColorButterfly = [quick_sale_button_color], @butterfly_no = [title_no] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE title_name = 'Butterfly Garden'
    SELECT @bkColorButterfly = Isnull(@bkColorButterfly,-1)
    IF @bkColorButterfly <= 0 SELECT @bkColorButterfly = @bkColorDefault
    SELECT @butterfly_no = isNull(@butterfly_no, 0)
             
    /*  Caption 1, 2, and 3 are created by the procedure, but Caption 4 is always the [Avail] token to display number of tickets left */

    SELECT @Caption1 = '', @Caption2 = '', @Caption3 = '', @Caption4 = ''
    SELECT @return_message = ''

    SELECT @layout_Id = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Science Central Future'
    SELECT @layout_Id = IsNull(@layout_id, 0)
    IF @layout_Id <= 0 BEGIN
        SELECT @return_message = 'Unable to locate the Science Central Future sales layout.'
        GOTO DONE
    END
        
     /*  The button type is a necessary value.  If it cannot be determined, the procedure cannot go on.   */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = IsNull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = IsNull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    BEGIN TRY
    
        DELETE FROM [dbo].T_SALES_LAYOUT_BUTTON WHERE [layout_id] = @layout_Id

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to clear the Science Central Future layout.'
        GOTO DONE

    END CATCH

    SELECT @last_dt = dateadd(day, (@number_of_days - 1), @performance_dt)
    SELECT @last_date = convert(char(10), @last_dt, 111)
    SELECT @day_num = 1
    
    WHILE @performance_date <= @last_date BEGIN

        IF datepart(month,@performance_dt) > 6 SELECT @fYear = datepart(year,@performance_dt) + 1 ELSE SELECT @fYear = datepart(year,@performance_dt)
        SELECT @curY = @day_num

        SELECT @Caption1 = left(ltrim(datename(weekday,@performance_dt)),3) + ' ' + left(ltrim(datename(month,@performance_dt)),3) + ' '
                         + ltrim(rtrim(datename(day,@performance_dt))) + ' ' + ltrim(rtrim(datename(year,@performance_dt)))


        /* Exhibit Hall Button */

        /* Need the Exhibit Hall Production Season  */
        SELECT @exh_season_name = 'Exhibit Halls FY' + convert(varchar(4),@fYear)
        SELECT @exh_season_no = [inv_no] FROM [dbo].[T_INVENTORY] WHERE [type] = 'S' and [description] = @exh_season_name
        SELECT @exh_season_no = isnull(@exh_season_no, 0)
        IF @exh_season_no <= 0 BEGIN
            SELECT @err_count = (@err_count + 1)
        END ELSE BEGIN

            /*  Need the Season number that corresponds to the Exhibit Hall Production Season  */
            SELECT @season_no = [season] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @exh_season_no
            SELECT @season_no = IsNull(@season_no, 0)
            IF @season_no <= 0 BEGIN
                SELECT @err_count = (@err_count + 1)
            END ELSE BEGIN

                /*  There should only be one performance for Public Exhibit Halls on any given day.  The performance number is retrieved by looking for a record in the
                    Exhibit Halls Production Season for the date being processed.  If there does happen to be more than one, it will take the last record created (highest performance number)  */
        
                SELECT @exh_perf_no = max([perf_no]) FROM [dbo].[T_PERF] WHERE convert(char(10),[perf_dt],111) = @performance_date and [prod_season_no] = @exh_season_no
                SELECT @exh_perf_no = isnull(@exh_perf_no, 0)
                IF @exh_perf_no <= 0 BEGIN
                    SELECT @err_count = (@err_count + 1)
                END ELSE BEGIN

                    SELECT @Caption2 = 'Public Exhibit Hall', @caption4 = '[Avail]'
                    SELECT @curX = 1

                    BEGIN TRY

                        IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_id and ypos = @curY and xpos = @curX)
                        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                                   [zone_sequence], [itemparentid])
                        VALUES (@layout_Id, @bTypePerf, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorExhibitHalls, @foColorFuture, null, @exh_perf_no, 
                                null, null, null, null, 0, null, null, null, null, @ZoneModeBestAvail, null, @season_no)
        
                    END TRY
                    BEGIN CATCH
                        SELECT @err_count = (@err_count + 1)    
                    END CATCH
                END
            END
        END

        SELECT @caption4 = ''
    
        /*  Omni Theater  */

        SELECT @rtn = ''
        SELECT @Caption2 = 'Omni Theater', @curX = 2, @future_layout = 'future_omni_' + CASE WHEN @day_num < 10 THEN '0' + convert(char(1),@day_num) ELSE convert(char(2),@day_num) END
        SELECT @future_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @future_layout
        SELECT @future_layout_no = IsNull(@future_layout_no,0)
        IF @future_layout_no > 0 and @omni_no > 0 BEGIN

            BEGIN TRY
        
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_Id and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                    VALUES (@layout_id, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorOmni, @foColorFuture, null, @future_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)


                EXECUTE [dbo].[LP_QS_single_venue] 
                        @performance_dt = @performance_dt, @title_no = @omni_no, @layout_no = @future_layout_no, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT

            END TRY
            BEGIN CATCH

                SELECT @err_count = (@err_count + 1)
        
            END CATCH

        END

        /*  Planetarium  */

        SELECT @rtn = ''
        SELECT @Caption2 = 'Planetarium', @curX = 3, @future_layout = 'future_planet_' + CASE WHEN @day_num < 10 THEN '0' + convert(char(1),@day_num) ELSE convert(char(2),@day_num) END
        SELECT @future_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @future_layout
        SELECT @future_layout_no = IsNull(@future_layout_no,0)
        IF @future_layout_no > 0 and @planet_no > 0 BEGIN

            BEGIN TRY
        
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_Id and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                    VALUES (@layout_id, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorPlanet, @foColorFuture, null, @future_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)


                EXECUTE [dbo].[LP_QS_single_venue] 
                        @performance_dt = @performance_dt, @title_no = @planet_no, @layout_no = @future_layout_no, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT

            END TRY
            BEGIN CATCH

                SELECT @err_count = (@err_count + 1)
        
            END CATCH

        END

        /*  4D-Theater  */

        SELECT @rtn = ''
        SELECT @Caption2 = '4-D Theater', @curX = 4, @future_layout = 'future_4d_' + CASE WHEN @day_num < 10 THEN '0' + convert(char(1),@day_num) ELSE convert(char(2),@day_num) END
        SELECT @future_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @future_layout
        SELECT @future_layout_no = IsNull(@future_layout_no,0)
        IF @future_layout_no > 0 and @4d_no > 0 BEGIN

            BEGIN TRY
        
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_Id and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                    VALUES (@layout_id, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColor4d, @foColorFuture, null, @future_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)


                EXECUTE [dbo].[LP_QS_single_venue] 
                        @performance_dt = @performance_dt, @title_no = @4d_no, @layout_no = @future_layout_no, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT

            END TRY
            BEGIN CATCH

                SELECT @err_count = (@err_count + 1)
        
            END CATCH

        END

        /*  Butterfly Garden  */

        SELECT @rtn = ''
        SELECT @Caption2 = 'Butterfly Garden', @curX = 5, @future_layout = 'future_btrfly_' + CASE WHEN @day_num < 10 THEN '0' + convert(char(1),@day_num) ELSE convert(char(2),@day_num) END
        SELECT @future_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @future_layout
        SELECT @future_layout_no = IsNull(@future_layout_no,0)
        IF @future_layout_no > 0 and @butterfly_no > 0 BEGIN

            BEGIN TRY
        
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_Id and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                    VALUES (@layout_id, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorbutterfly, @foColorFuture, null, @future_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)


                EXECUTE [dbo].[LP_QS_single_venue] 
                        @performance_dt = @performance_dt, @title_no = @butterfly_no, @layout_no = @future_layout_no, @sort_type = 'Time', @remove_past_performances = 'N', @return_message = @rtn OUTPUT

            END TRY
            BEGIN CATCH

                SELECT @err_count = (@err_count + 1)
        
            END CATCH

        END

        /*  School Products  */

        SELECT @rtn = ''
        SELECT @Caption2 = 'School Products', @curX = 6, @future_layout = 'future_school_' + CASE WHEN @day_num < 10 THEN '0' + convert(char(1),@day_num) ELSE convert(char(2),@day_num) END
        SELECT @future_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @future_layout
        SELECT @future_layout_no = IsNull(@future_layout_no,0)
        IF @future_layout_no > 0 and @butterfly_no > 0 and datename(weekday,@performance_dt) not in ('Saturday', 'Sunday') BEGIN

            BEGIN TRY
        
                IF not exists (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_Id and ypos = @curY and xpos = @curX)
                    INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                    VALUES (@layout_id, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorExhibitHalls, @foColorFuture, null, @future_layout_no, 
                            null, null, null, null, 0, null, null, null, null, Null, null, Null)


                EXECUTE [dbo].[LP_QS_school] @performance_dt = @performance_dt, @is_today = 'N', @is_tomorrow = 'N', @return_message = @rtn OUTPUT 
         
            END TRY
            BEGIN CATCH

                SELECT @err_count = (@err_count + 1)
        
            END CATCH

        END
                
        SELECT @performance_dt = dateadd(day, 1, @performance_dt), @day_num = (@day_num + 1)
        SELECT @performance_date = convert(char(10), @performance_dt, 111)


    END



    SELECT @layout_Id = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Science Central Launch'
    SELECT @layout_Id = IsNull(@layout_Id, 0)
    SELECT @future_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Science Central Future'
    SELECT @future_layout_no = IsNull(@future_layout_no, 0)
    IF @layout_Id > 0 and @future_layout_no > 0 BEGIN

        SELECT @curY = 5, @curX = 5
        SELECT @caption1 = 'MOVE TO', @Caption2 = 'NEXT ' + convert(varchar(2),@number_of_days) + ' DAYS'
        SELECT @caption3 = '', @Caption4 = ''

        BEGIN TRY

        
        
            DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @layout_Id and [ypos] = @curY and [xpos] = @curX
           
            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@layout_id, @bTypeNav, @curY, @curX, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorDefault, @foColorFuture, null, @future_layout_no, 
                    null, null, null, null, 0, null, null, null, null, Null, null, Null)
        
        END TRY
        BEGIN CATCH
            SELECT @err_count = (@err_count + 1)
        END CATCH

    END

    IF @err_count > 0 SELECT @return_message = 'The procedure has completed but with ' + convert(varchar(10),@err_count) + ' errors.'

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:


        IF @return_message = '' SELECT @return_message = 'unknown error'


END
GO

GRANT EXECUTE ON [dbo].[LP_QS_future_dates] TO impusers
GO










    