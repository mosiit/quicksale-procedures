USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_special_exhibit_packages]') AND type in (N'P', N'PC')) BEGIN
    EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[LP_QS_special_exhibit_packages] AS' 
    EXEC dbo.sp_executesql @statement = N'GRANT EXECUTE ON [dbo].[LP_QS_special_exhibit_packages] TO impusers' 
END
GO
    
ALTER PROCEDURE [dbo].[LP_QS_special_exhibit_packages]
        @start_dt DATETIME = NULL,
        @exhibit_name VARCHAR(30) = NULL,
        @return_message VARCHAR(1000) = NULL OUTPUT
AS BEGIN
    
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

    /*  Procedure Variables and Tables  */

        --parent_x_max and parent_y_max are hard coded but are also double-checked later
        DECLARE @parent_layout_name VARCHAR(30) = 'special_exhibit', @parent_layout_id INT = 0
        DECLARE @parent_x_pos INT = 1, @parent_y_pos INT = 1, @parent_x_max INT = 5, @parent_y_max INT = 5

        --parent_x_max and parent_y_max are hard coded and are used when creating new layouts
        DECLARE @layout_name VARCHAR(30) = 'special_exhibit_pkg_01', @layout_no INT = 2, @layout_id INT = 0
        DECLARE @x_pos INT = 1, @y_pos INT = 1, @x_max INT = 5, @y_max INT = 7

        DECLARE @pkg_button_type INT = 9    --FROM TR_SALES_LAYOUT_BUTTON_TYPE
        DECLARE @nav_button_type INT = 30   --FROM TR_SALES_LAYOUT_BUTTON_TYPE

        DECLARE @btn_bg_color_white INT = 16777215
        DECLARE @btn_bg_color INT = 11202814
        DECLARE @btn_fg_color INT = 64

        DECLARE @pkg_no INT = 0, @pkg_description VARCHAR(30) = '', @pkg_season INT, @pkg_dt DATETIME, @prev_pkg_dt DATETIME
        DECLARE @mn_name VARCHAR(30)

                
        IF OBJECT_ID('tempdb..#pkg_buttons') IS NOT NULL DROP TABLE [#pkg_buttons]
    
        CREATE TABLE [#pkg_buttons] ([id] INT NOT NULL DEFAULT (0),
                                     [layout_id] INT NOT NULL DEFAULT (0),
	                                 [button_type_id] INT NOT NULL  DEFAULT (0),
	                                 [ypos] INT NOT NULL DEFAULT ((0)),
	                                 [xpos] INT NOT NULL DEFAULT ((0)),
	                                 [caption1] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption2] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption3] VARCHAR(30) NULL DEFAULT (''),
	                                 [caption4] VARCHAR(30) NULL DEFAULT (''),
	                                 [background_color] INT NOT NULL DEFAULT (0),
	                                 [foreground_color] INT NOT NULL DEFAULT (0),
	                                 [itemamount] MONEY NULL DEFAULT (0.0),
	                                 [itemid] INT NULL DEFAULT (0),
	                                 [itemotherid] INT NULL DEFAULT (0),
	                                 [itemsubid] INT NULL DEFAULT (0),
	                                 [create_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
	                                 [created_by] VARCHAR(8) NOT NULL DEFAULT (''),
	                                 [create_loc] VARCHAR(16) NOT NULL DEFAULT (''),
	                                 [last_update_dt] DATETIME NOT NULL DEFAULT (GETDATE()),
	                                 [last_updated_by] VARCHAR(8) NOT NULL DEFAULT (''),
	                                 [product_start_mode] INT NULL DEFAULT (0),
	                                 [product_start_dt] DATETIME NULL,
	                                 [product_start_day_add] INT NULL DEFAULT (0),
	                                 [product_sequence] INT NULL DEFAULT (0),
	                                 [product_end_mode] INT NULL DEFAULT (0),
	                                 [product_end_day_add] INT NULL DEFAULT (0),
	                                 [product_end_dt] DATETIME NULL,
	                                 [zone_mode] INT NULL DEFAULT (0),
	                                 [zone_sequence] INT NULL DEFAULT (0),
	                                 [itemparentid] INT NULL DEFAULT (0))
 
    /* Check Parameters  */

        IF @start_dt IS NULL SELECT @start_dt = GETDATE()

        --Always start at the first of the month
        WHILE DATEPART(DAY,@start_dt) > 1 SELECT @start_dt = DATEADD(DAY,-1,@start_dt)
       
        SELECT @start_dt = CAST(@start_dt AS DATE)

        SELECT @Exhibit_name = ISNULL(@Exhibit_name,'')

        SELECT @return_message = ''
        
    /*  Determine parent layout id number  */

        BEGIN TRY
        
            SELECT @parent_layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT]
                                                      WHERE [description] = @parent_layout_name

            




            IF ISNULL(@parent_layout_id,0) <= 0 THROW 500001,'Unable to determine sales layout id.',1;

            SELECT @parent_x_max = ISNULL([columns],5),
                   @parent_y_max = ISNULL([rows],5)  FROM [dbo].[T_SALES_LAYOUT] 
                                                     WHERE [id] = @parent_layout_id

        END TRY
        BEGIN CATCH

            --include the layout name with the error message                           
            SELECT @return_message = @parent_layout_name + ' - ' + ERROR_MESSAGE();

            THROW 500001, @return_message, 1;

        END CATCH


    /*  First Button at the top is always for TODAY  */

        SELECT @pkg_no = [pkg_no],
               @pkg_description = [description],
               @pkg_season = [season],
               @pkg_dt = [package_dt]
        FROM dbo.T_PKG 
        WHERE pkg_type = 7 
          AND DATEDIFF(DAY,package_dt,GETDATE()) = 0


        IF ISNULL(@pkg_no,0) > 0 AND ISNULL(@pkg_season,0) > 0 AND @pkg_dt IS NOT NULL
            INSERT INTO [#pkg_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount],
                                    [itemid], [itemotherid], [itemsubid], [create_dt], [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                    [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
            SELECT 0,
                   @parent_layout_id, 
                   @pkg_button_type, 
                   1, 
                   1, 
                   CASE WHEN @Exhibit_name = '' THEN @pkg_description ELSE @Exhibit_name END,
                   FORMAT(@pkg_dt,'MM/dd/yyyy'),
                   'Package',
                   'Today',
                   @btn_bg_color_white,
                   @btn_fg_color,
                   NULL,
                   @pkg_no,
                   NULL,
                   NULL,
                   GETDATE(),
                   dbo.FS_USER(),
                   [dbo].[FS_LOCATION](),
                   GETDATE(),
                   dbo.FS_USER(),
                   NULL,
                   NULL,
                   0,
                   NULL,
                   NULL,
                   NULL,
                   NULL,
                   0,
                   NULL,
                   @pkg_season

        SELECT @parent_x_pos = 2,
               @parent_y_pos = 1       



    /*    */

        --Special Exhibits will be divided by month.  Each month will have its own layout
        --The first layout name for special exhibits is special_exhibit_pkg_01, as indicated in the variable declaration above
        --Need to determine the id number of that layout (if layout does not exist, it will automatically be created later).
        SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name

        --Package buttons are created one at a time.  The package information is pulled into a cursor.
        DECLARE package_cursor CURSOR FORWARD_ONLY FOR
        SELECT [pkg_no],
               [description],
               [season],
               [package_dt]
        FROM [dbo].[T_PKG]
        WHERE [pkg_type] = 7                    --7 = Special Exhibit (from TR_PKG_TYPE)
          AND [package_dt] >= @start_dt
        ORDER BY [package_dt]
        OPEN [package_cursor]
        
        --Pull first record from the cursor
        FETCH NEXT FROM package_cursor INTO @pkg_no, @pkg_description, @pkg_season, @pkg_dt
        SELECT @prev_pkg_dt = @pkg_dt       --Set to same as pkg date for first record
                                            --After first record, this will contain date of previous record
        
        WHILE @@FETCH_STATUS <> -1 BEGIN

            BEGIN TRY

                --IF month of current record is different from month of previous record, layout needs to change
                --Set the name to the new layout then attempt to get an id number for that layout
                IF DATEDIFF(MONTH,@prev_pkg_dt, @pkg_dt) > 0 BEGIN

                    SELECT @layout_id = 0;

                    SELECT @layout_name = 'special_exhibit_pkg_' 
                                        + CASE WHEN @layout_no < 10 THEN '0' ELSE '' END 
                                        + CONVERT(VARCHAR(2),@layout_no);
                    SELECT @layout_no += 1;

                    SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name;

                    SELECT @x_pos = 1,
                           @y_pos = 1

                END;

                --If layout does not exist (layout_id = 0) then create a new layout and try again to get the id number
                IF @layout_id = 0 BEGIN
                
                    INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                    VALUES (@layout_name, 'N', @y_max, @x_max, -1, 'N');

                    SELECT @layout_id = ISNULL([id],0) FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name;

                END;
                
                --If layout still can't be found, throw an error (jumps to catch block)
                IF @layout_id <= 0 THROW 500001,'Unable to determine sales layout id.',1;            

            END TRY
            BEGIN CATCH

                --include the layout name with the error message                           
                SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                THROW 500001, @return_message, 1;
               
            END CATCH


            BEGIN TRY

                SELECT @mn_name = DATENAME(MONTH,@pkg_dt) + ', ' + DATENAME(YEAR,@pkg_dt)

                IF NOT EXISTS (SELECT * FROM [#pkg_buttons] WHERE [layout_id] = @parent_layout_id AND itemid = @layout_id) BEGIN

                    INSERT INTO [#pkg_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount],
                                            [itemid], [itemotherid], [itemsubid], [create_dt], [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                            [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
                    SELECT 0,
                       @parent_layout_id, 
                       @nav_button_type, 
                       @parent_y_pos, 
                       @parent_x_pos, 
                       @Exhibit_name,
                       @mn_name,
                       'Packages',
                       '',
                       @btn_bg_color,
                       @btn_fg_color,
                       NULL,
                       @layout_id,
                       NULL,
                       NULL,
                       GETDATE(),
                       dbo.FS_USER(),
                       [dbo].[FS_LOCATION](),
                       GETDATE(),
                       dbo.FS_USER(),
                       NULL,
                       NULL,
                       0,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL

                    SELECT @parent_x_pos += 1

                    IF @parent_x_pos > @parent_x_max
                        SELECT @parent_y_pos += 1,
                               @parent_x_pos = 1

                END

                INSERT INTO [#pkg_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount],
                                        [itemid], [itemotherid], [itemsubid], [create_dt], [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                        [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
                SELECT 0,
                       @layout_id,
                       @pkg_button_type, 
                       @y_pos, 
                       @x_pos, 
                       CASE WHEN @Exhibit_name = '' THEN @pkg_description ELSE @Exhibit_name END,
                       FORMAT(@pkg_dt,'MM/dd/yyyy'),
                       'Package',
                       '',
                       @btn_bg_color,
                       @btn_fg_color,
                       NULL,
                       @pkg_no,
                       NULL,
                       NULL,
                       GETDATE(),
                       dbo.FS_USER(),
                       [dbo].[FS_LOCATION](),
                       GETDATE(),
                       dbo.FS_USER(),
                       NULL,
                       NULL,
                       0,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       0,
                       NULL,
                       @pkg_season

                SELECT @x_pos += 1

                IF @x_pos > @x_max
                    SELECT @y_pos += 1,
                           @x_pos = 1

            END TRY
            BEGIN CATCH

                --include the layout name with the error message                           
                SELECT @return_message = @layout_name + ' - ' + ERROR_MESSAGE();

                THROW 500001, @return_message, 1;

            END CATCH

            SELECT @prev_pkg_dt = @pkg_dt
            FETCH NEXT FROM package_cursor INTO @pkg_no, @pkg_description, @pkg_season, @pkg_dt

        END
        CLOSE package_cursor
        DEALLOCATE package_cursor
        

        INSERT INTO [#pkg_buttons] ([id], [layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount],
                                [itemid], [itemotherid], [itemsubid], [create_dt], [created_by], [create_loc], [last_update_dt], [last_updated_by], [product_start_mode], [product_start_dt],
                                [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
            SELECT DISTINCT 0,
                            [layout_id], 
                            @nav_button_type, 
                            @y_max, 
                            @x_max, 
                            '',
                            '<<< GO BACK <<<',
                            '',
                            '',
                            0,
                            16777215,
                            NULL,
                            @parent_layout_id,
                            NULL,
                            NULL,
                            GETDATE(),
                            dbo.FS_USER(),
                            [dbo].[FS_LOCATION](),
                            GETDATE(),
                            dbo.FS_USER(),
                            NULL,
                            NULL,
                            0,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL,
                            NULL
            FROM [#pkg_buttons]
            WHERE [layout_id] <> @parent_layout_id
        
        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON]
        WHERE [layout_id] IN (SELECT DISTINCT layout_id FROM [#pkg_buttons] WHERE [layout_id] <> @parent_layout_id)

        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color],
                                                   [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence],
                                                   [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
        SELECT  [layout_id],
                [button_type_id],
                [ypos],
                [xpos],
                [caption1],
                [caption2],
                [caption3],
                [caption4],
                [background_color],
                [foreground_color],
                [itemamount],
                [itemid],
                [itemotherid],
                [itemsubid],
                [product_start_mode],
                [product_start_dt],
                [product_start_day_add],
                [product_sequence],
                [product_end_mode],
                [product_end_day_add],
                [product_end_dt],
                [zone_mode],
                [zone_sequence],
                [itemparentid]
        FROM [#pkg_buttons]

        IF @return_message = '' SELECT @return_message = 'success'

    FINISHED:

END
GO



--DECLARE @rtn VARCHAR(1000) = '', @today DATETIME = GETDATE()  
--EXECUTE [dbo].[LP_QS_special_exhibit_packages] @start_dt = @today, @exhibit_name = 'Body Worlds', @return_message = @rtn OUTPUT
--PRINT @rtn



    
   


    



