

ALTER PROCEDURE [dbo].[LP_QS_mos_at_school]
        @performance_dt DATETIME = NULL,
        @return_message VARCHAR(100) OUTPUT
AS BEGIN
    
    SET NOCOUNT ON;
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    /*  Procedure Variables  */

        DECLARE @performance_date char(10) = '';

        DECLARE @bkColorDefault INT = 14085615;
        DECLARE @bkColorMOSAtSchool INT  = 14085615;
        DECLARE @foColorToday INT = 0, @foColorFuture INT = 0;

        DECLARE @bTypePerf INT = 1,  --1 = Performance from TR_SALES_LAYOUT_BUTTON_TYPE
                @bTypeNav INT = 30;   --30 = Navigator from TR_SALES_LAYOUT_BUTTON_TYPE

        DECLARE @mos_at_school_layout_no INT = 0, @mos_at_school_layout_no_all INT = 0, 
                @mos_at_school_layout_no_prod INT = 0;

        DECLARE @layout_id_launch_box INT = 50, @layout_id_launch_sci INT = 72

        DECLARE @xMax INT = 0, @yMax INT = 0, @xMaxAll INT = 0, @yMaxAll INT = 0, @xMaxPrd INT = 0, @yMaxPrd INT = 0,
                @xCur INT = 0, @yCur INT = 0, @xCurAll INT = 0, @yCurAll INT = 0, @xCurPrd INT = 0, @yCurPrd INT = 0;

        DECLARE @row_no INT = 0, @perf_no INT = 0, @prod_season_no INT = 0, @prod_no INT = 0, @perf_zone_no INT = 0;
        DECLARE @layout_name VARCHAR(30) = '', @prev_layout_name VARCHAR(30) = '', @perf_time VARCHAR(10) = '', @prod_name VARCHAR(30) = '';
        DECLARE @prod_season_name VARCHAR(30) = '', @perf_name VARCHAR(30) = '', @perf_dt DATETIME;

        DECLARE @season_no INT = 0
        DECLARE @caption1 varchar(30) = '', @caption2 varchar(30) = '', @caption3 varchar(30) = '', @caption4 varchar(30) = '';

    /*  Check Parameters  */

        SELECT @performance_dt = ISNULL(@performance_dt, GETDATE())
        SELECT @performance_date = convert(char(10),@performance_dt,111)

        SELECT @return_message = ''

    /* Defaults  */

        SELECT @bkColorDefault = ISNULL([default_value], 14085615)
                                 FROM [dbo].[T_DEFAULTS] 
                                 WHERE [parent_table] = 'Museum of Science' 
                                   AND [field_name] =  'QS Button Color Default'
        
        SELECT @bkColorMOSAtSchool = ISNULL([default_value], @bkColorDefault) 
                                   FROM [dbo].[T_DEFAULTS] 
                                   WHERE [parent_table] = 'Museum of Science' 
                                     AND [field_name] =  'QS Button Color Special Events'
        
        SELECT @foColorToday = ISNULL([default_value], 0) 
                               FROM [dbo].[T_DEFAULTS] 
                               WHERE [parent_table] = 'Museum of Science' 
                                 AND [field_name] =  'QS Text Color Today'

        SELECT @foColorFuture = ISNULL([default_value], 0) 
                                FROM [dbo].[T_DEFAULTS] 
                                WHERE [parent_table] = 'Museum of Science' 
                                  AND [field_name] =  'QS Text Color Future'


    /*  Verify that the mos_at_school_main screen and mos_at_school_all screen exists and if not, create them.  */

        IF NOT EXISTS (SELECT * FROM [dbo].[T_SALES_LAYOUT] WHERE description = 'mos_at_school')
            INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
            VALUES ('mos_at_school', 'Y', 10, 5, -1, 'N')

        IF NOT EXISTS (SELECT * FROM [dbo].[T_SALES_LAYOUT] WHERE description = 'mos_at_school_all')
            INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
            VALUES ('mos_at_school_all', 'N', 25, 10, -1, 'N')

    /*  Retrieve the id number for the MOS At School and MOS At School All screens, then delete all the buttons associated with that screen  */
            
        SELECT @mos_at_school_layout_no = ISNULL([id], 0),
               @yMax = ISNULL([rows], 0), 
               @xMax = ISNULL([columns], 0) 
        FROM [dbo].[T_SALES_LAYOUT] 
        WHERE [description] = 'mos_at_school'

        SELECT @mos_at_school_layout_no_all = ISNULL([id], 0),
               @yMaxAll = ISNULL([rows], 0),
               @xMaxAll = ISNULL([columns], 0)
        FROM [dbo].[T_SALES_LAYOUT]
        WHERE [description] = 'mos_at_school_all'
               
        IF @mos_at_school_layout_no <= 0 or @yMax <= 0 or @xMax <= 0 BEGIN
            SELECT @return_message = 'unable to find the adult offering layout information'
            GOTO DONE
        END 

        IF @yMaxAll < 5 OR @xMaxAll < 5 SELECT @mos_at_school_layout_no_all = 0

        DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] 
        WHERE [layout_id] IN (SELECT [id] 
                              FROM [dbo].[T_SALES_LAYOUT] 
                              WHERE [description] LIKE 'mos_at_school%')

        SELECT @ycur = 1, @xcur = 1, @yCurAll = 1, @xCurAll = 0;

    /*  Performances are processed one at a time  using a one way read-only cursor  */

        DECLARE MOSAtSchoolCursor INSENSITIVE CURSOR FOR
        WITH [CTE_PRODS] ([prod_no], [prod_name])
        AS (SELECT DISTINCT [production_no], [production_name]
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
            WHERE [performance_date] >= convert(char(10),@performance_dt,111) 
              AND [title_name] = 'MOS at School'),
             [CTE_PERFS] ([row_no], [prod_no], [prod_name])
        AS (SELECT ROW_NUMBER() OVER(ORDER BY [prod_name] ASC), 
                   [prod_no],
                   [prod_name]
            FROM [CTE_PRODS])
        SELECT cte.[row_no],
               'mos_at_school' + CASE WHEN cte.[row_no] < 10 THEN '_0' ELSE '_' END + CAST(cte.[row_no] AS VARCHAR(2)) AS [layout_name],
               prf.[performance_no], 
               prf.[performance_dt], 
               prf.[performance_time_display], 
               prf.[production_season_no],
               prf.[production_no],
               prf.[production_name], 
               prf.[production_season_name], 
               prf.[performance_zone], 
               prf.[performance_name]
        FROM CTE_PERFS AS cte
             LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[production_no] = cte.[prod_no]
        WHERE prf.[performance_dt] >= @performance_dt
        ORDER BY [cte].[row_no], prf.[performance_dt], prf.[performance_time]
        OPEN [MOSAtSchoolCursor]
        BEGIN_MOS_AT_SCHOOL_LOOP:
        
            FETCH NEXT FROM [MOSAtSchoolCursor] INTO @row_no, @layout_name, @perf_no, @perf_dt, @perf_time, @prod_season_no,
                                                     @prod_no, @prod_name, @prod_season_name, @perf_zone_no, @perf_name
            IF @@FETCH_STATUS = -1 GOTO END_MOS_AT_SCHOOL_LOOP
            
            --Need the Season number that corresponds to the production season - Can't Go On Without it
            SELECT @season_no = ISNULL([season], 0) FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @prod_season_no
                        
            IF @season_no <= 0 BEGIN
                SELECT @return_message = 'unable to determine the season for ' + @prod_season_name
                GOTO END_MOS_AT_SCHOOL_LOOP
            END

            /*  If the layout name has changed, reset for a new layout */

            IF @layout_name <> @prev_layout_name BEGIN

                --If the new layout name does not exist, create it
                IF NOT EXISTS (SELECT * FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = @layout_name)
                    INSERT INTO [dbo].[T_SALES_LAYOUT] ([description], [primary_ind], [rows], [columns], [control_group], [inactive])
                    VALUES (@layout_name, 'N', 10, 5, -1, 'N')

                --Get the id number for the new layout
                SELECT @mos_at_school_layout_no_prod = ISNULL([id], 0) ,
                       @yMaxPrd = ISNULL([rows], 5),
                       @xMaxPrd = ISNULL([columns], 5)
                FROM [dbo].[T_SALES_LAYOUT] WHERE description = @layout_name
                
                --Reset the grid coordinates to 1,1
                SELECT @yCurPrd = 1, @xCurPrd = 1
                SELECT @yCurAll = 1, @xcurAll += 1

                --Set the current layout name to now be the previous layout name
                SELECT @prev_layout_name = @layout_name    

            END

            --There is a main layout that has buttons to the individual productions.  If a button for the current production does
            --not exist on the main layout, create it now.  This happens with the first performance of each production and is skipped
            --on additional performances for the same production.
            IF NOT EXISTS (SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @mos_at_school_layout_no AND [caption2] = @prod_name) BEGIN

                SELECT @Caption1 = 'MOS at School',         @Caption2 = @prod_name,         @caption3 = '',         @caption4 = ''

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@mos_at_school_layout_no, @bTypeNav, @ycur, @xcur, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorMOSAtSchool, @foColorFuture, null, @mos_at_school_layout_no_prod, 
                        null, null, null, null, 0, null, null, null, null, null, null, null)

                --Increment the grid coordinates for the main layout
                IF @xCur = @xMax SELECT @yCur += 1, @xCur = 1
                ELSE SELECT @xCur += 1

            END

            --Create the button for the individual performance on the current layout
            SELECT @Caption1 = left(datename(weekday,@perf_dt),3) + ' ' + left(datename(month,@perf_dt),3) + ' ' 
                             + CASE WHEN datepart(day,@perf_dt) < 10 THEN '0' + convert(char(1),datepart(day,@perf_dt)) ELSE convert(char(2),datePart(day,@perf_dt)) END
            
            SELECT @Caption2 = @prod_name,          @caption3 = @perf_time,             @caption4 = '[Avail]'

            INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                       [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                       [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                       [zone_sequence], [itemparentid])
            VALUES (@mos_at_school_layout_no_prod, @bTypePerf, @yCurPrd, @xCurPrd, @caption1, @Caption2, @Caption3, @Caption4, @bkColorMOSAtSchool, @foColorFuture, null, 
                    @Perf_no, null, @perf_zone_no, null, null, 0, null, null, null, null, 4, null, @season_no)

            --Increment the grid coordinates for the current production's layout
            IF @xCurPrd = @xMaxPrd SELECT @yCurPrd += 1, @xCurPrd = 1
            ELSE SELECT @xCurPrd += 1
            
            --If an "ALL" layout exists, add a button for this performance to that as well.
            IF @mos_at_school_layout_no_all > 0 BEGIN

                INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                           [zone_sequence], [itemparentid])
                VALUES (@mos_at_school_layout_no_all, @bTypePerf, @yCurAll, @xCurAll, @caption1, @Caption2, @Caption3, @Caption4, @bkColorMOSAtSchool, @foColorFuture, null, 
                        @Perf_no, null, @perf_zone_no, null, null, 0, null, null, null, null, 4, null, @season_no)

                SELECT @yCurAll += 1
            
            END;

            GOTO BEGIN_MOS_AT_SCHOOL_LOOP

        END_MOS_AT_SCHOOL_LOOP:
        CLOSE [MOSAtSchoolCursor];
        DEALLOCATE [MOSAtSchoolCursor];

    /*  If an "ALL" layout exists, add a button to the main layout that navigates to it.  */

        SELECT @caption1 = 'MOS AT SCHOOL',         @caption2 = 'ALL PROGRAMS',      @caption3 = '',         @caption4 = ''

        --Increment the grid coordinates for the current production's layout
        --This was already done above.  Doing it again now will put a space between the last production and the "All" button
        IF @xCur = @xMax SELECT @yCur += 1, @xCur = 1
        ELSE SELECT @xCur += 1

        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
                                                   [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
                                                   [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
                                                   [zone_sequence], [itemparentid])
        VALUES (@mos_at_school_layout_no, @bTypeNav, @yCur, @xcur, @caption1, @caption2, @caption3, @Caption4, @bkColorMOSAtSchool, @foColorFuture, null, 
                @mos_at_school_layout_no_all, null, null, null, null, 0, null, null, null, null, null, null, null)
        
    FINISHED:

        IF ISNULL(@return_message, '') = '' SELECT @return_message = 'success'

    DONE: 
END
GO
    

--BEGIN TRAN
    DECLARE @rtn VARCHAR(100) = ''     EXECUTE [dbo].[LP_QS_mos_at_school] @performance_dt = NULL, @return_message = @rtn OUTPUT       PRINT @rtn
--ROLLBACK TRAN
--COMMIT TRAN


--
--INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id],[button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color],
--                                           [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid], [product_start_mode], [product_start_dt],
--                                           [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode],
--                                           [zone_sequence], [itemparentid])
--VALUES (@layout_id_launch_box, @bTypeNav, 1, 4, @caption1, @caption2, @caption3, @Caption4, @bkColorMOSAtSchool, @foColorFuture, null, @mos_at_school_layout_no, 
--        null, null, null, null, 0, null, null, null, null, null, null, null),
--       (@layout_id_launch_sci, @bTypeNav, 1, 4, @Caption1, @Caption2, @Caption3, @Caption4, @bkColorMOSAtSchool, @foColorFuture, null, @mos_at_school_layout_no, 
--        null, null, null, null, 0, null, null, null, null, null, null, null)

--SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = 50 AND ypos = 1 ORDER BY [ypos], [xpos];
--SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = 72 AND ypos = 1 ORDER BY [ypos], [xpos];

--ROLLBACK TRAN
--COMMIT TRAN
SELECT * FROM [dbo].[T_SALES_LAYOUT] WHERE [description] LIKE 'mos_at_school%' ORDER BY [description];
SELECT * FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] IN (SELECT [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] LIKE 'mos_at_school%') ORDER BY [caption2], [layout_id], [ypos], [xpos];
        
--SELECT * FROM [dbo].[T_INVENTORY] WHERE [description] = 'MOS at School'
--SELECT * FROM [dbo].[T_SALES_LAYOUT] WHERE description LIKE 'mos_at_school%'

--SELECT DISTINCT [performance_date]
--FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
--WHERE [performance_date] >= convert(char(10),@performance_dt,111) 
--  AND [title_name] = 'MOS at School'
--ORDER BY [performance_date]

        --WITH [CTE_PRODS] ([prod_no], [prod_name])
        --AS (SELECT DISTINCT [production_no], [production_name]
        --    FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
        --    WHERE [performance_date] >= convert(char(10),@performance_dt,111) 
        --      AND [title_name] = 'MOS at School'),
        --     [CTE_PERFS] ([row_no], [prod_no], [prod_name])
        --AS (SELECT ROW_NUMBER() OVER(ORDER BY [prod_name] ASC), 
        --           [prod_no],
        --           [prod_name]
        --    FROM [CTE_PRODS])
        --SELECT cte.[row_no],
        --       prf.[production_name],
        --       COUNT(*)
        --FROM CTE_PERFS AS cte
        --     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[production_no] = cte.[prod_no]
        --WHERE prf.[performance_dt] >= @performance_dt
        --GROUP BY [cte].[row_no], [prf].[production_name]
        --ORDER BY [cte].[row_no]

--SELECT [performance_no], 
--       [performance_dt], 
--       [performance_time_display], 
--       [production_season_no], 
--       [production_name], 
--       [production_season_name], 
--       [performance_zone], 
--       [performance_name]
--FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
--WHERE [performance_date] >= convert(char(10),@performance_dt,111) 
--  AND [title_name] = 'MOS at School'
--ORDER BY [performance_date], [performance_time]

