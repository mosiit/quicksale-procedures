USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LP_QS_members_launch]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[LP_QS_members_launch]
GO

CREATE PROCEDURE [dbo].[LP_QS_members_launch]
        @performance_dt datetime = null,
        @is_today char(1) = Null,
        @is_tomorrow char(1) = Null,
        @return_message varchar(100) = Null OUTPUT
AS BEGIN

    DECLARE @members_layout_no int, @sci_central_layout_no int, @nav_layout_no int, @bTypeNav int, @bTypePerf int
    DECLARE @bkColorDefault int, @foColorToday int, @curY int, @curX int
    DECLARE @Caption1 varchar(30), @Caption2 varchar(30), @Caption3 varchar(30), @caption4 varchar(10), @performance_date char(10)

     /*  Check Parameters:  If Null is passed to the Performance Date parameter, default to today's date
                           IF anything other then 'Title' is passed to the Sort Type parameter, default to 'Time'      */
    
    SELECT @performance_dt = IsNull(@performance_dt, getdate())
    SELECT @performance_date = convert(char(10),@performance_dt,111)

     IF @is_today is null BEGIN 
        IF @performance_date = convert(char(10),getdate(),111) SELECT @is_today = 'Y' ELSE SELECT @is_today = 'N'
    END

    IF @is_tomorrow is null BEGIN
        IF @performance_date = convert(char(10),dateadd(day,1,getdate()),111) SELECT @is_tomorrow = 'Y' ELSE SELECT @is_tomorrow = 'N'
    END

    SELECT @return_message = ''

    /* Defaults  */

    SELECT @bkColorDefault = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Button Color Default'
    SELECT @bkColorDefault = isnull(@bkColorDefault, 14085615)

    SELECT @foColorToday = [default_value] FROM [dbo].[T_DEFAULTS] WHERE parent_table = 'Museum of Science' and [field_name] =  'QS Text Color Today'
    SELECT @foColorToday = isnull(@foColorToday, 0)
        
    /*  Button Types: Performance and Navigation buttons are created by this procedure.  The ID numbers for both are needed  */

    SELECT @bTypePerf = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Performance'
    SELECT @bTypePerf = isnull(@bTypePerf, 0)
    IF @bTypePerf <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for performance button type'
        GOTO DONE
    END

    SELECT @bTypeNav = [id] FROM [dbo].[TR_SALES_LAYOUT_BUTTON_TYPE] WHERE [description] = 'Navigator'
    SELECT @bTypeNav = isnull(@bTypeNav, 0)
    IF @bTypeNav <= 0 BEGIN
        SELECT @return_message = 'unable to determine id number for navigator button type'
        GOTO DONE
    END

    /*  *********************************************************************************************************************************** */  
        
    /*  Retrieve the id number for the Box Office Launch Quick Sale Screen, then delete all the buttons associated with that screen  */
            
    SELECT @members_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Members Launch'
    SELECT @members_layout_no = isnull(@members_layout_no, 0)
    IF @members_layout_no <= 0 BEGIN
        SELECT @return_message = 'unable to find the members launch layout'
        GOTO DONE
    END

    SELECT @sci_central_layout_no = [id] FROM [dbo].[T_SALES_LAYOUT] WHERE [description] = 'Science Central Launch'
    SELECT @sci_central_layout_no = isnull(@sci_central_layout_no, 0)
    IF @sci_central_layout_no <= 0 BEGIN
        SELECT @return_message = 'unable to find the science central launch layout'
        GOTO DONE
    END
    
    DELETE FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @members_layout_no

    /*  *********************************************************************************************************************************** */  

    /* Members Launch screen is the same as Science Central Launch for the time being - Simply copy that layout over   */

    BEGIN TRY

        INSERT INTO [dbo].[T_SALES_LAYOUT_BUTTON] ([layout_id], [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount], [itemid],
                                                   [itemotherid], [itemsubid], [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], 
                                                   [product_end_dt], [zone_mode], [zone_sequence], [itemparentid])
        SELECT @members_layout_no, [button_type_id], [ypos], [xpos], [caption1], [caption2], [caption3], [caption4], [background_color], [foreground_color], [itemamount], [itemid], [itemotherid], [itemsubid],
               [product_start_mode], [product_start_dt], [product_start_day_add], [product_sequence], [product_end_mode], [product_end_day_add], [product_end_dt], [zone_mode], [zone_sequence], [itemparentid]
        FROM [dbo].[T_SALES_LAYOUT_BUTTON] WHERE [layout_id] = @sci_central_layout_no

    END TRY
    BEGIN CATCH

        SELECT @return_message = left(Error_message(),100)
        IF @return_message is null SELECT @return_message = 'an error occurred while attempting to create the members launch screen.'
        GOTO DONE

    END CATCH

    IF @return_message = '' SELECT @return_message = 'success'

    DONE:
       
        IF @return_message = '' SELECT @return_message = 'unknown error'

END
GO

GRANT EXECUTE ON [LP_QS_members_launch] to impusers
GO

